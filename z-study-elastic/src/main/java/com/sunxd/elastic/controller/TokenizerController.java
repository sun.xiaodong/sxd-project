package com.sunxd.elastic.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * @author: sun.xd
 * @date: 2021-11-22 11:48
 **/
@RestController
@RequestMapping("/api/tokenizer")
public class TokenizerController {

    @RequestMapping(value = "/getHotWords")
    public void getHotWords(HttpServletResponse  httpServletResponse) throws IOException {
        File file = new File("/usr/local/Cellar/elasticsearch-7.15.1/single/test/plugins/ik/config/custom/remote-cus1.dic");
        FileInputStream fileInputStream = new FileInputStream(file);
        InputStreamReader  inputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String str = null;
        ServletOutputStream outputStream = httpServletResponse.getOutputStream();
        httpServletResponse.setHeader("Last-Modified", String.valueOf(file.length()));
        httpServletResponse.setHeader("ETag", String.valueOf(file.length()));
        httpServletResponse.setContentType("application/json; charset=UTF-8");
        while ( (str = bufferedReader.readLine()) != null){
            outputStream.write((str + "\n").getBytes());
        }
        outputStream.flush();
        bufferedReader.close();

    }

    @RequestMapping(value = "/getStopWords")
    public void getStopWords(HttpServletResponse  httpServletResponse)throws IOException{
        File file = new File("/usr/local/Cellar/elasticsearch-7.15.1/single/test/plugins/ik/config/custom/remote-cus2.dic");
        FileInputStream fileInputStream = new FileInputStream(file);
        InputStreamReader  inputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String str = null;
        ServletOutputStream outputStream = httpServletResponse.getOutputStream();
        httpServletResponse.setHeader("Last-Modified", String.valueOf(file.length()));
        httpServletResponse.setHeader("ETag", String.valueOf(file.length()));
        httpServletResponse.setContentType("application/json; charset=UTF-8");
        while ( (str = bufferedReader.readLine()) != null){
            outputStream.write((str + "\n").getBytes());
        }
        outputStream.flush();
        bufferedReader.close();


    }
}
