package com.sunxd.elastic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZStudyElasticApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZStudyElasticApplication.class, args);
    }

}
