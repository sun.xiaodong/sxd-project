package com.sunxd.disruptor.factory;

import com.lmax.disruptor.EventFactory;
import com.sunxd.disruptor.event.LongEvent;

/**
 * @author: 作者名称
 * @date: 2021-09-23 17:04
 **/
public class LongEventFactory implements EventFactory<LongEvent> {
    @Override
    public LongEvent newInstance() {
        return new LongEvent();
    }
}
