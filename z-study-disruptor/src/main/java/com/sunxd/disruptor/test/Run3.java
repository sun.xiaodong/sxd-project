package com.sunxd.disruptor.test;

/**
 * 应对java8 lambda
 * @author: 作者名称
 * @date: 2021-09-27 09:51
 **/

import com.lmax.disruptor.*;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.util.DaemonThreadFactory;
import com.sunxd.disruptor.event.LongEvent;
import com.sunxd.disruptor.factory.LongEventFactory;
import com.sunxd.disruptor.handler.LongEventHandler;

import java.util.Arrays;
import java.util.concurrent.Executors;

public class Run3 {
    public static void main(String[] args) {
        Disruptor<LongEvent> disruptor = new Disruptor<LongEvent>(LongEvent::new,8, DaemonThreadFactory.INSTANCE);
        disruptor.handleEventsWith((longEvent, l, b) -> System.out.println("Event:" + longEvent));
        disruptor.start();
        RingBuffer<LongEvent> ringBuffer = disruptor.getRingBuffer();
        ringBuffer.publishEvent(((longEvent, l) -> longEvent.setValue(10)));
        ringBuffer.publishEvent((longEvent, sequence, aLong) -> longEvent.setValue(aLong),2L);
        ringBuffer.publishEvent((longEvent, sequence, aLong,along2) -> longEvent.setValue(aLong+along2),2L,3L);

    }
}
