package com.sunxd.disruptor.test;

/**
 * 多个handler 的情况。每个handler都会去处理一次
 *
 * @author: 作者名称
 * @date: 2021-09-27 09:51
 */

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import com.lmax.disruptor.util.DaemonThreadFactory;
import com.sunxd.disruptor.event.LongEvent;
import com.sunxd.disruptor.factory.LongEventFactory;
import com.sunxd.disruptor.handler.LongEventHandler;

public class Run7_HandlerException {
    public static void main(String[] args) throws InterruptedException {
        Disruptor<LongEvent> disruptor = new Disruptor<LongEvent>(new LongEventFactory(),8, DaemonThreadFactory.INSTANCE, ProducerType.SINGLE,new BlockingWaitStrategy());
        LongEventHandler longEventHandler1 = new LongEventHandler();
        disruptor.handleExceptionsFor(longEventHandler1).with(new ExceptionHandler<LongEvent>() {
            @Override
            public void handleEventException(Throwable throwable, long l, LongEvent longEvent) {
                throwable.printStackTrace();
            }

            @Override
            public void handleOnStartException(Throwable throwable) {
                System.out.println("Exception start to handle !");
            }

            @Override
            public void handleOnShutdownException(Throwable throwable) {

            }
        });
        disruptor.handleEventsWith(longEventHandler1);
        disruptor.start();
        RingBuffer<LongEvent> ringBuffer = disruptor.getRingBuffer();
        for (int i = 0; i < 2; i++) {
            ringBuffer.publishEvent((longEvent, sequence, aLong) -> longEvent.setValue(aLong),i);
        }

    }
}
