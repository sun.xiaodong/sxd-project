package com.sunxd.disruptor.test;

/**
 * 多个handler 的情况。每个handler都会去处理一次
 *
 * @author: 作者名称
 * @date: 2021-09-27 09:51
 */

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.EventTranslator;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import com.lmax.disruptor.util.DaemonThreadFactory;
import com.sunxd.disruptor.event.LongEvent;
import com.sunxd.disruptor.factory.LongEventFactory;
import com.sunxd.disruptor.handler.LongEventHandler;
import javafx.scene.transform.Translate;

import java.util.concurrent.*;

public class Run6_MultiConsumer {
    public static void main(String[] args) throws InterruptedException {
        Disruptor<LongEvent> disruptor = new Disruptor<LongEvent>(new LongEventFactory(),8, DaemonThreadFactory.INSTANCE, ProducerType.SINGLE,new BlockingWaitStrategy());
        LongEventHandler longEventHandler1 = new LongEventHandler();
        LongEventHandler longEventHandler2 = new LongEventHandler();

        disruptor.handleEventsWith(longEventHandler1,longEventHandler2);
        disruptor.start();
        RingBuffer<LongEvent> ringBuffer = disruptor.getRingBuffer();
        for (int i = 0; i < 2; i++) {
            ringBuffer.publishEvent((longEvent, sequence, aLong) -> longEvent.setValue(aLong),i);
        }

    }
}
