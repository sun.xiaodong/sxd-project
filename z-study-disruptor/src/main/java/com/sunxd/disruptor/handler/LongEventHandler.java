package com.sunxd.disruptor.handler;

import com.lmax.disruptor.EventHandler;
import com.sunxd.disruptor.event.LongEvent;

/**
 * @author: 作者名称
 * @date: 2021-09-24 15:50
 **/
public class LongEventHandler implements EventHandler<LongEvent> {

    public static long count = 0;

    /**
     *
     * @param longEvent 事件
     * @param l 位置
     * @param b 整体消息是否结束
     * @throws Exception
     */
    @Override
    public void onEvent(LongEvent longEvent, long l, boolean b) throws Exception {
        count ++;
        System.out.println("["+Thread.currentThread().getName()+"]"+longEvent.toString()+" .seq:"+l+" b:"+b);

    }
}
