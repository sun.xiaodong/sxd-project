package com.sunxd.disruptor.event;

import lombok.Data;

/**
 * @author: 作者名称
 * @date: 2021-09-23 17:02
 **/
@Data
public class LongEvent  {

    private long value;

    @Override
    public String toString() {
        return "LongEvent{" +
                "value=" + value +
                '}';
    }
}
