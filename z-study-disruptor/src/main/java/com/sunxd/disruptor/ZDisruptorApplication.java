package com.sunxd.disruptor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * disruptor 框架的使用，及结合springboot
 */
@SpringBootApplication
public class ZDisruptorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZDisruptorApplication.class, args);
    }

}
