package com.sunxd.disruptor.config;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.sunxd.disruptor.event.LongEvent;
import com.sunxd.disruptor.factory.LongEventFactory;
import com.sunxd.disruptor.handler.LongEventHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;

/**
 * @author: 作者名称
 * @date: 2021-09-27 14:34
 **/
@Configuration
public class RingBufferConfig {

    @Bean
    public RingBuffer<LongEvent> longEventRingBuffer(){
        LongEventFactory factory = new LongEventFactory();
        Disruptor<LongEvent> disruptor = new Disruptor<LongEvent>(factory,1024, Executors.defaultThreadFactory());
        disruptor.handleEventsWith(new LongEventHandler());
        disruptor.start();
        return disruptor.getRingBuffer();
    }
}
