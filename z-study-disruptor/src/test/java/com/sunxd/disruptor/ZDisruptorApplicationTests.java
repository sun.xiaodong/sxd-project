package com.sunxd.disruptor;

import com.lmax.disruptor.EventTranslator;
import com.lmax.disruptor.RingBuffer;
import com.sunxd.disruptor.event.LongEvent;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ZDisruptorApplicationTests {

    @Autowired
    private RingBuffer<LongEvent> longEventRingBuffer;

    @Test
    void contextLoads() {

//        EventTranslator<LongEvent> translator1 = new EventTranslator<LongEvent>() {
//            @Override
//            public void translateTo(LongEvent longEvent, long l) {
//                longEvent.setValue(0L);
//            }
//        };

        for (int i = 0; i < 10; i++) {
            longEventRingBuffer.publishEvent(getEventTranslator(i));
        }
    }

    public EventTranslator<LongEvent> getEventTranslator(long i){
        return (longEvent, l) -> longEvent.setValue(i);
    }

}
