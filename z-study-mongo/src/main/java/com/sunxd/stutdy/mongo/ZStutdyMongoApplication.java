package com.sunxd.stutdy.mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZStutdyMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZStutdyMongoApplication.class, args);
    }

}
