package com.sunxd.znginxdemo1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ZNginxDemo1ApplicationTests {

    @Autowired
    private com.sunxd.znginxdemo1.test.Test test1;

    @Test
    public void ect(){
        String s = test1.get();
        System.out.println(s);
    }

    @Test
    void contextLoads() {
    }

}
