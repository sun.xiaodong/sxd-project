package com.sunxd.znginxdemo1.aspect;

import com.alibaba.fastjson.JSONObject;
import com.sunxd.znginxdemo1.test.dto.Activity;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @ClassName AddNodeInstancesAspect  <br>
 * @Description 1、添加节点实列 2、同步其他实列节点数据
 * <br>
 * @Author jiaLe.liu  <br>
 * @Date 2022/1/10 19:24 <br>
 * @Version 1.0      <br>
 **/
@Component
@Slf4j
@Aspect
public class AddNodeInstancesAspect {


    @Pointcut("execution( * com.sunxd.znginxdemo1.test.Test.*(..)) ")
    public void addNodeInstancesPointcut() {
    }

    @Before(value = "addNodeInstancesPointcut()")
    public void addNodeInstances(JoinPoint joinPoint) {
        try {
            if (joinPoint != null && joinPoint.getArgs().length != 0) {
                if (joinPoint.getArgs()[0] instanceof Activity) {
                    Activity activity = (Activity) joinPoint.getArgs()[0];
                    // 取上下文活动id

                        String activityUuid = activity.getUuid();
                            return;

                }
            }
        } catch (Exception ex) {
            log.error("addNodeInstances-ERROR:{}", JSONObject.toJSONString(joinPoint), ex);
        }
    }
}
