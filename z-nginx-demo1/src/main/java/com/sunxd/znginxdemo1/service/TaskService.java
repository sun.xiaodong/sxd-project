package com.sunxd.znginxdemo1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.IntStream;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/4/21 20:19
 */
@Component
public class TaskService {

    @Autowired
    @Qualifier("interactCommonExecutor")
    private ThreadPoolExecutor threadPoolExecutor;

    public void execute() {
        System.out.println("------------------1-----------------------------------------");
        IntStream.range(1, 10).forEach(x -> {
            MyCallable myCallable = new MyCallable(x);
            CompletableFuture<MyCallable> myCallableCompletableFuture = CompletableFuture.supplyAsync(myCallable, threadPoolExecutor);
        });
        System.out.println("------------------1----- end------------------------------------");
        System.out.println("------------------2----- ------------------------------------");
        IntStream.range(20, 30).forEach(x -> {
            MyCallable myCallable = new MyCallable(x);
            CompletableFuture<MyCallable> myCallableCompletableFuture = CompletableFuture.supplyAsync(myCallable);
        });
        System.out.println("------------------2-----end ------------------------------------");

    }


}
