package com.sunxd.znginxdemo1.service;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/4/21 20:20
 */
@Data
@Slf4j
public class MyCallable implements Supplier<MyCallable> {

    private int key;

    public MyCallable(int key) {
        this.key = key;
    }

    @Override
    public MyCallable get() {
        Long start = System.currentTimeMillis();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException ignored) {
        }
        Long end = System.currentTimeMillis();
        log.info(" key:{} ,当前线程:{} ,耗时：{} ", key,Thread.currentThread().getName(), (end - start));
        return this;
    }
}
