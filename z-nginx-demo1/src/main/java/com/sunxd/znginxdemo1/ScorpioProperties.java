package com.sunxd.znginxdemo1;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;
@Data
@ConfigurationProperties(prefix = "scorpio")
public class ScorpioProperties {

    /**
     * 模块名称
     */
    private String module;

    private String strategyRecordKey;

    private Map<String,String> updateKeyMaps;

    public void test(){
        updateKeyMaps.forEach((k ,v) -> System.out.println("k :"+k +" v :" +v));
    }



}
