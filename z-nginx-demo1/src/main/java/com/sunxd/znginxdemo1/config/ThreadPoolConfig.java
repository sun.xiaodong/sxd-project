package com.sunxd.znginxdemo1.config;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.*;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/2/11 10:32
 */
@Configuration
@Slf4j
public class ThreadPoolConfig {

    /**
     * 发奖使用线
     * 核心最大都是16
     * 阻塞队列10000
     *
     * @return ThreadPoolExecutor
     */
    @Bean(value = "interactCommonExecutor")
    public ThreadPoolExecutor interactCommonExecutor() {
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("interact-common-thread" + "-%d")
                .setPriority(2)
                .build();
        ThreadPoolExecutor executor = new ThreadPoolExecutor(8,
                8,
                30,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(10000),
                namedThreadFactory,
                new ThreadPoolExecutor.CallerRunsPolicy());
        executor.allowCoreThreadTimeOut(true);
        return executor;
    }

}
