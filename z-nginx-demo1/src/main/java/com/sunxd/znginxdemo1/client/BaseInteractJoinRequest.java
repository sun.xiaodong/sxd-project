package com.sunxd.znginxdemo1.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Description:
 */
@EqualsAndHashCode()
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseInteractJoinRequest implements Serializable {
    private static final long serialVersionUID = 4381644119037477162L;

    /**
     * 活动id
     */
    private String activityUuid;

    /**
     * 活动类型
     */
    private String interactiveType;

    /**
     * 用户id
     */
    private String passengerUuid;

    /**
     * 头像
     */
    private String curCity;

    /**
     * 只有使用积分兑换的时候，才有值
     * 是否使用支付规则
     */
    private String payRuleJson;



}