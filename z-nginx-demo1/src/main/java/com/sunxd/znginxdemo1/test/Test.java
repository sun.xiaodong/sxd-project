package com.sunxd.znginxdemo1.test;

import com.google.common.collect.Maps;
import com.sunxd.znginxdemo1.test.dto.NodeEnum;
import com.sunxd.znginxdemo1.test.dto.NodeType;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class Test {
    public static void main(String[] args) {
        me();
        Map<Long, List<NodeType>> nodeTypeMap = Maps.newHashMap();
        List<NodeEnum> nodeEnums = Arrays.asList(NodeEnum.values());
        nodeTypeMap = nodeEnums.stream().map(e -> {
            NodeType nodeType = NodeType.builder()
                    .id(Long.valueOf(e.getId()))
                    .nodeKey(e.getNodeKey())
                    .name(e.getNodeName())
                    .componentName(e.getComponentName())
                    .type(e.getType()).build();
            return nodeType;
        }).collect(Collectors.groupingBy(NodeType::getId));
        System.out.println(nodeTypeMap);
    }

    public static  void me(){
        return;
    }

    public String get(){
        System.out.println(111);
        return "222";
    }
}
