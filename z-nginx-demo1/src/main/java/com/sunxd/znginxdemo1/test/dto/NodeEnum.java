package com.sunxd.znginxdemo1.test.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zouhui
 * @description 节点枚举值
 * @date 2021/12/21
 */
@Getter
@AllArgsConstructor
public enum NodeEnum {
    /**
     * 节点类型
     */
    BEGIN_NODE(0, "开始", "begin", "", 1),
    FREQUENCY_NODE(1, "频率节点", "frequencyNode", "frequencyNodeComponent", 1),
    GROUP_NODE(2, "受众用户", "groupNode", "groupNodeComponent", 1),
    EVENT_NODE(3, "事件节点", "eventNode", "eventNodeComponent", 1),
    REWARD_NODE(4, "奖励节点", "rewardNode", "rewardNodeComponent", 1),
    NOTICE_NODE(5, "触达节点", "noticeNode", "noticeNodeComponent", 1),
    PARTICIPATE_LIMIT_NODE(6, "参与限制", "participateLimitNode", "participateLimitNodeComponent", 1),
    END_LOGIC_NODE(7, "结束逻辑节点", "endLogicNode", "endLogicNodeComponent", 2);

    /**
     * nodeId
     */
    private final Integer id;
    /**
     * 节点名称描述
     */
    private final String nodeName;
    /**
     * 节点key
     */
    private final String nodeKey;
    /**
     * 节点对应组件beanName
     */
    private final String componentName;
    /**
     * 1 业务节点  2 逻辑节点
     */
    private final Integer type;

    public static NodeEnum match(Integer id) {
        for (NodeEnum value : NodeEnum.values()) {
            if (value.getId().equals(id)) {
                return value;
            }
        }
        return null;
    }

    public static NodeEnum match(String nodeKey) {
        for (NodeEnum value : NodeEnum.values()) {
            if (value.getNodeKey().equals(nodeKey)) {
                return value;
            }
        }
        return null;
    }
}
