package com.sunxd.znginxdemo1.test.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NodeType {
    /**
     * 自增主键
     */
    private Long id;
    /**
     * 节点key
     */
    private String nodeKey;
    /**
     * 节点名称
     */
    private String name;
    /**
     * 组件名称
     */
    private String componentName;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 类型，业务node，系统node如风控
     */
    private Integer type;

}
