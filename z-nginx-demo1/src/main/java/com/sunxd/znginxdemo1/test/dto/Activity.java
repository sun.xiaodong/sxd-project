package com.sunxd.znginxdemo1.test.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @description 活动 DO
 * <br/>
 * @author biebf
 * @createDate 2021/12/23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Activity {

    /**
     * 活动ID
     */
    private Long id;
    /**
     * 活动UUID
     */
    private String uuid;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 活动编码
     */
    private String activityCode;
    /**
     * 活动开始时间
     */
    private Date beginTime;
    /**
     * 活动结束时间
     */
    private Date endTime;
    /**
     * 适配城市编码
     */
    private String cityCode;
    /**
     * 活动类型 1 事件营销 2 条件营销
     */
    private Integer actType;
    /**
     * 活动属性-指标KPI信息
     * {指标类型，指标属性，指标值}
     */
    private String planProperty;
    /**
     * 活动额度类别 1限额，2不限
     */
    private Integer activityQuotaType;
    /**
     * 活动总额度
     */
    private BigDecimal activityTotalAmount;
    /**
     * 费控属性
     * {产品线，城市}
     */
    private String feeProperty;
    /**
     * 活动状态【1：待生效，2：生效中，3：已结束，4：已失效】
     */
    private Integer activityStatus;
    /**
     * 活动状态审核状态【1：待审核，2：已通过，3：驳回】
     */
    private Integer auditStatus;

    /**
     * 活动版本
     */
    private String actVersion;
    /**
     * create
     *
     * @param activityName name
     * @param activityCode code
     * @param beginTime beginTime
     * @param endTime endTime
     * @param nodeInstances node
     * @return Activity
     */

}
