package com.sunxd.znginxdemo1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableConfigurationProperties(ScorpioProperties.class)
public class ZNginxDemo1Application {

    @Autowired
    private ScorpioProperties scorpioProperties;

    public static void main(String[] args) {
        SpringApplication.run(ZNginxDemo1Application.class, args);

    }

}
