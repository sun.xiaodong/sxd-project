package com.sunxd.znginxdemo1.util;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Map;

/**
 * rest模板工具
 *
 * @author zhuzl
 * @date 2020/10/26
 */
@Slf4j
@Component
public final class RestTemplateUtil {
    /**
     * 实际执行请求的template
     */
    @Resource
    private RestTemplate restTemplate;
    //最大连接数
    private static final int MAX_TOTAL = 500;
    public static final Integer NUMBER30 = 30;
    //路由最大连接数
    private static final int MAX_PER_ROUTE_MAX = 200;
    public static final Integer NUMBER60000 = 60000;
    public static final Integer THOUSAND = 1000;

    /**
     * 其他模板跑龙套
     */
    private RestTemplateUtil() {

    }

    /**
     * 帖子
     * json请求
     *
     * @param url    url
     * @param entity 实体
     * @param cls    cls
     * @param <T>    T
     * @return {@link T}
     */
    public <T> T post(String url, Object entity, Class<T> cls) {
        HttpEntity<Object> request = new HttpEntity<>(entity);
        return executePost(url, request, cls);
    }

    /**
     * 文章的形式
     * form 请求
     *
     * @param url    url
     * @param entity 实体
     * @param cls    cls
     * @param <T>    t
     * @return {@link T}
     */
    public <T> T postForm(String url, Map<String, Object> entity, Class<T> cls) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        for (Map.Entry<String, Object> entry : entity.entrySet()) {
            map.add(entry.getKey(), entry.getValue());
        }
        HttpEntity<Object> request = new HttpEntity<>(map, headers);
        return executePost(url, request, cls);
    }

    /**
     * 执行职务
     * post 请求
     *
     * @param url     url
     * @param request 请求
     * @param cls     cls
     * @param <T>     t
     * @return {@link T}
     */
    private <T> T executePost(String url, HttpEntity<Object> request, Class<T> cls) {
        ResponseEntity<T> exchange = restTemplate.exchange(url, HttpMethod.POST, request, cls);
        log.info("requestUri={} >>> httpStatus={}, requestHeadMap= {} requestBody= {} , responseBody={}",
                url, exchange.getStatusCode(), request.getHeaders(),
                JSON.toJSONString(request.getBody()), JSON.toJSONString(exchange.getBody()));
        return exchange.getBody();
    }

    /**
     * 调用
     *
     * @param url         地址
     * @param headers     头
     * @param map         canshu
     * @param contextType 类型
     * @param targetClazz 类
     * @param <T>         参数
     * @return Response 响应值
     */
    public <T> String postCommonMethod(String url, Map<String, String> headers, Object map, String contextType,
                                       Class<T> targetClazz) {
        String postResult = postCommon(url, headers, JSON.toJSONString(map));

        return postResult;
    }

    /**
     * 发送消息
     *
     * @param url      链接
     * @param headers  头
     * @param jsonData json数据
     * @return String
     */
    public String postCommon(String url, Map<String, String> headers, String jsonData) {

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity request = new HttpEntity<>(jsonData, header);
        return restTemplate.postForObject(url, request, String.class);
    }





}
