package com.sunxd.znginxdemo1.util;

import com.alibaba.fastjson.JSON;
import com.sunxd.znginxdemo1.controller.res.Response;


public class ApiHandler {

    public static <T> T convert(Response<T> t){
        return (null != t && null != t.getData())?t.getData():null;
    }

    public static <T> T parse(String responseStr, Class<T> targetClazz){
        T t = JSON.parseObject(responseStr, targetClazz);
        return t;
    }
}
