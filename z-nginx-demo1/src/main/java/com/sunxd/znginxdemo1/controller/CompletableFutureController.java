package com.sunxd.znginxdemo1.controller;

import com.alibaba.fastjson.JSONObject;
import com.sunxd.znginxdemo1.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/4/21 20:27
 */
@RestController
@RequestMapping("completableFuture")
public class CompletableFutureController {

    @Autowired
    private TaskService taskService;

    @RequestMapping("test1")
    public String url(){
        taskService.execute();
        return "demo1";
    }

}
