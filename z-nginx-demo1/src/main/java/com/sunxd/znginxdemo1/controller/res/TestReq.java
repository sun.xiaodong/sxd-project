package com.sunxd.znginxdemo1.controller.res;

import lombok.Data;

import java.io.Serializable;

@Data
public class TestReq implements Serializable {
    private static final long serialVersionUID = 3793891898184063317L;

    private String userId;
    private Integer bookType;
    private Integer accountType;

}
