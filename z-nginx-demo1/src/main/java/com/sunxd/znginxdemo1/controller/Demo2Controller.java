package com.sunxd.znginxdemo1.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.sunxd.znginxdemo1.ScorpioProperties;
import com.sunxd.znginxdemo1.controller.res.Response;
import com.sunxd.znginxdemo1.controller.res.WalletAccountInfoDto;
import com.sunxd.znginxdemo1.handler.AbstractUpdateExpiration;
import com.sunxd.znginxdemo1.handler.UpdateExpirationParams;
import com.sunxd.znginxdemo1.util.ApiHandler;
import com.sunxd.znginxdemo1.util.RestTemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author: 作者名称
 * @date: 2021-04-16 11:03
 **/
@RestController
@RequestMapping("/api/wallet")
public class Demo2Controller {

    @Autowired
    private  RestTemplateUtil restTemplateUtil;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ScorpioProperties scorpioProperties;

//    @Autowired
//    private MarketingInteractClient marketingInteractClient;
//
//
//    public Response<BaseInteractiveJoinDetailInfo<?>> test(@RequestBody BaseInteractJoinRequest req){
//        marketingInteractClient.go(req,"userid");
//    }

    @RequestMapping("accountInfo")
    public String zuul(){
        scorpioProperties.test();
        return "demo1";

    }
    @RequestMapping("demo2")
    public String demo2(@RequestParam String key){
        AbstractUpdateExpiration.excute(key, UpdateExpirationParams.builder().build());
        return "demo1";
    }

    @PostMapping(value = "/demo3")
    public String demo3(HttpServletRequest request ){
        Map<String ,Object> body = new HashMap<>();
        body.put("userId","userId");
        // 乘客bu 默认传1
        body.put("bookType",1);
        body.put("accountType",1);
        HttpEntity<Object> httpEntity = new HttpEntity<>(body);
         ResponseEntity<Response> resStr = restTemplate.postForEntity("http://127.0.0.1:8092" + "/api/wallet/accountInfo", httpEntity, Response.class);
        System.out.println(resStr.getBody().getData());
        Response<WalletAccountInfoDto> res = ApiHandler.parse(JSON.toJSONString(resStr.getBody()), Response.class);
        WalletAccountInfoDto data =
                ApiHandler.parse(JSON.toJSONString(resStr.getBody().getData()), WalletAccountInfoDto.class);
        if(Objects.nonNull(data) && !StringUtils.isEmpty(data.getBooks())){
            Optional<WalletAccountInfoDto.Book> first = data.getBooks().stream().filter(x -> Objects.equals(x.getBookType(), 3)).findFirst();
            if(first.isPresent()){
                System.out.println( first.get().getAvailableBalance());
            }
        }
        return "123";
    }

    public static void main(String[] args) {

//       String str = "{\"accountDesc\":\"\",\"accountType\":1,\"accountGift\":0,\"remark\":\"\",\"updateTime\":\"2022-01-13T03:18:27.000+0000\",\"userId\":\"efc1ecec2c704441a3d8ab0afd56b892\",\"accountStatus\":1,\"accountId\":\"1481465641352380504\",\"books\":[{\"bookStatus\":1,\"totalBalance\":0,\"monetaryUnit\":1,\"permission\":\"\",\"remark\":\"\",\"updateTime\":\"2022-01-13T03:18:27.000+0000\",\"availableBalance\":0,\"accountId\":1481465641352380504,\"withdrawalBalance\":0,\"createTime\":\"2022-01-13T03:18:27.000+0000\",\"id\":95769,\"optVersion\":1,\"freezeBalance\":0,\"bookType\":1}],\"createTime\":\"2022-01-13T03:18:27.000+0000\",\"accountNo\":\"\",\"accountCash\":0,\"accountNewId\":600000235420004,\"accountWithdrawalGift\":0,\"optVersion\":1,\"accountWithdrawalCash\":0}";
//        WalletAccountInfoDto data =
//                ApiHandler.parse(str, WalletAccountInfoDto.class);
//        if(Objects.nonNull(data) && !StringUtils.isEmpty(data.getBooks())){
//            Optional<WalletAccountInfoDto.Book> first = data.getBooks().stream().filter(x -> Objects.equals(x.getBookType(), 1)).findFirst();
//            if(first.isPresent()){
//                System.out.println( first.get().getAvailableBalance());
//            }
//        }



        List<Integer> list = Lists.newArrayList();
        list.add(10);
        list.add(3);
        list.add(2);
        list.add(14);
        list.add(1312);
        list.add(131);
        list.add(13);
        list.sort(Integer::compare);
        System.out.println(list);
    }

}
