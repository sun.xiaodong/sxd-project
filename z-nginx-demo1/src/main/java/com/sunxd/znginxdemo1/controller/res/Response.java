package com.sunxd.znginxdemo1.controller.res;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class Response<T> implements Serializable {


    private static final long serialVersionUID = 6280855035125657876L;
    private String msg;
    private Integer code;
    private T data;
    private Boolean success;
}
