package com.sunxd.znginxdemo1.controller.res;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @description 支付中心-钱包账户信息
 */

@Data
public class WalletAccountInfoDto implements Serializable {

    private static final long serialVersionUID = -4454951036745212950L;

    /** 现金账本余额（可用+冻结）*/
    private Integer accountCash;
    private String accountDesc;
    /** 赠送币余额 */
    private Integer accountGift;
    /** 主账户Id */
    private String accountId;
    /** 分库分表键 */
    private String accountNewId;
    /** 主账号No */
    private String accountNo;
    /** 账户状态，0:失效, 1:正常, 2:冻结出, 3:冻结入, 4:冻结出入 */
    private Integer accountStatus;
    /** 账户类型 */
    private Integer accountType;

    private List<Book> books;

    /**
     * 账本列表
     */
    @Data
    public static class Book{
        /** 主账户Id */
        private String accountId;
        /** 可用余额 */
        private Integer availableBalance;
        /** 账本状态，0:失效, 1:正常, 2:冻结出, 3:冻结入, 4:冻结出入 */
        private Integer bookStatus;
        /** 账本类型 */
        private Integer bookType;
        /** 冻结余额 */
        private Integer freezeBalance;
        /** 账户类型 */
        private Integer id;

    }


}
