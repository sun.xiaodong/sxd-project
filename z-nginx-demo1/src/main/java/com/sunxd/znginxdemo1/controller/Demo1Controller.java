package com.sunxd.znginxdemo1.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSONObject;
import com.sunxd.znginxdemo1.DemoData;
import com.sunxd.znginxdemo1.ScorpioProperties;
import com.sunxd.znginxdemo1.controller.res.Response;
import com.sunxd.znginxdemo1.controller.res.TestReq;
import com.sunxd.znginxdemo1.handler.AbstractUpdateExpiration;
import com.sunxd.znginxdemo1.handler.UpdateExpirationParams;
import com.sunxd.znginxdemo1.util.RestTemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: 作者名称
 * @date: 2021-04-16 11:03
 **/
@RestController
@RequestMapping("z-nginx")
public class Demo1Controller {

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    @Autowired
    private ScorpioProperties scorpioProperties;

    @RequestMapping("dataUrl")
    public Response<String> dataUrl(@RequestBody TestReq req){
        Response<String> response = new Response<String>();
        response.setData(req.getUserId());
        response.setCode(200);
        response.setMsg("msg");
        return response;
    }

    @RequestMapping("url")
    public String url(){
        JSONObject queryParam = new JSONObject();
        queryParam.put("url", "h5Prefix + activityUuid");
        queryParam.put("businessLine", "营销");
        String s = restTemplateUtil.postCommon("http://10.3.64.191:8038/create", null, queryParam.toJSONString());
        System.out.println(s);
        return "demo1";
    }


    @RequestMapping("demo")
    public String zuul(){
        scorpioProperties.test();
        return "demo1";
    }
    @RequestMapping("demo2")
    public String demo2(@RequestParam String key){
        AbstractUpdateExpiration.excute(key, UpdateExpirationParams.builder().build());
        return "demo1";
    }


    @RequestMapping("demo3")
    public void export(HttpServletResponse response) throws IOException {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode("测试", "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), DemoData.class).sheet("模板").doWrite(data());
    }

    public static OutputStream getOutputStream(String fileName, HttpServletResponse response) {
        //创建本地文件
        String filePath = fileName + ".xlsx";
        try {
            response.setCharacterEncoding("UTF-8");
            response.setHeader("content-Type", "application/vnd.ms-excel");
            response.setHeader("Content-Disposition",
                    "attachment;filename*=utf-8''" + URLEncoder.encode(filePath, "UTF-8"));
            return response.getOutputStream();
        } catch (IOException e) {
            throw new RuntimeException("生成excel失败！");
        }
    }

    private List<DemoData> data() {
        List<DemoData> list = new ArrayList<DemoData>();
        for (int i = 0; i < 10; i++) {
            DemoData data = new DemoData();
            data.setString("字符串" + i);
            data.setDate(new Date());
            data.setDoubleData(0.56);
            list.add(data);
        }
        return list;
    }

}
