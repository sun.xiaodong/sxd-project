package com.sunxd.znginxdemo1.handler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: 作者名称
 * @date: 2021-04-21 17:27
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateExpirationParams implements Serializable {

    private static final long serialVersionUID = 7602151590927411846L;

    private String key;

}
