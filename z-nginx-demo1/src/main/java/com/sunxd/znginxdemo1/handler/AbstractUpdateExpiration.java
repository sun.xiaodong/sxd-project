package com.sunxd.znginxdemo1.handler;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: 作者名称
 * @date: 2021-04-21 16:56
 **/
@Slf4j
public abstract class AbstractUpdateExpiration {

    private static Map<String, AbstractUpdateExpiration> map = new HashMap<>();

    public AbstractUpdateExpiration() {
        map.put(this.unionKey(),this);
    }

    /**
     * 唯一值
     * @return String
     */
    public abstract String unionKey();

    /**
     * 逻辑处理
     * @param params p
     * @return Map<String,Object>
     */
    public abstract Map<String,Object> handle(UpdateExpirationParams params);

    public static Map<String,Object> excute(String key, UpdateExpirationParams params){
        if(map.containsKey(key)){
            return map.get(key).handle(params);
        }
        log.info(" key is error :{}" + key);
        return new HashMap<>();
    };

}
