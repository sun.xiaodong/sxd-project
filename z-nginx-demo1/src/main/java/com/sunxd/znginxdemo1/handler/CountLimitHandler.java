package com.sunxd.znginxdemo1.handler;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author: 作者名称
 * @date: 2021-04-21 17:34
 **/
@Component
public class CountLimitHandler extends AbstractUpdateExpiration{

    @Override
    public String unionKey() {
        return "def";
    }

    @Override
    public Map<String, Object> handle(UpdateExpirationParams params) {
        System.out.println("CountLimitHandler");
        return null;
    }
}
