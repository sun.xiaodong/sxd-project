package com.sunxd.znginxdemo1.handler;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author: 作者名称
 * @date: 2021-04-21 17:34
 **/
@Component
public class CycleJoinCountLimitHandler extends AbstractUpdateExpiration{


    @Override
    public String unionKey() {
        return "abc";
    }

    @Override
    public Map<String, Object> handle(UpdateExpirationParams params) {
        System.out.println("CycleJoinCountLimitHandler");
        return null;
    }
}
