//package com.sunxd.integration.platform.test;
//
//import org.mybatis.spring.mapper.ClassPathMapperScanner;
//import org.springframework.beans.factory.BeanNameAware;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.support.BeanDefinitionRegistry;
//import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.util.StringUtils;
//
//public class MapperScannerConfigurer
//    implements BeanDefinitionRegistryPostProcessor, InitializingBean, ApplicationContextAware, BeanNameAware {
//  @Override
//  public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) {
//    if (this.processPropertyPlaceHolders) {
//      processPropertyPlaceHolders();
//    }
//
//    ClassPathMapperScanner scanner = new ClassPathMapperScanner(registry);
//    scanner.setAddToConfig(this.addToConfig);
//    scanner.setAnnotationClass(this.annotationClass);
//    scanner.setMarkerInterface(this.markerInterface);
//    scanner.setSqlSessionFactory(this.sqlSessionFactory);
//    scanner.setSqlSessionTemplate(this.sqlSessionTemplate);
//    scanner.setSqlSessionFactoryBeanName(this.sqlSessionFactoryBeanName);
//    scanner.setSqlSessionTemplateBeanName(this.sqlSessionTemplateBeanName);
//    scanner.setResourceLoader(this.applicationContext);
//    scanner.setBeanNameGenerator(this.nameGenerator);
//    scanner.setMapperFactoryBeanClass(this.mapperFactoryBeanClass);
//    if (StringUtils.hasText(lazyInitialization)) {
//      scanner.setLazyInitialization(Boolean.valueOf(lazyInitialization));
//    }
//    if (StringUtils.hasText(defaultScope)) {
//      scanner.setDefaultScope(defaultScope);
//    }
//    scanner.registerFilters();
//    scanner.scan(
//        StringUtils.tokenizeToStringArray(this.basePackage, ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS));
//  }
//
//...
//
//}
