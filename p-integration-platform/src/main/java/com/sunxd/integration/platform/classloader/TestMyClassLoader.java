package com.sunxd.integration.platform.classloader;

import java.io.IOException;

import static com.sunxd.integration.platform.constants.ClassLoaderConstant.HTTP_SRC;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/9/8 16:02
 */
public class TestMyClassLoader {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        MyClassLoader myClassLoader = new MyClassLoader("HelloB", HTTP_SRC);
        Class<?> clazz = myClassLoader.dynamicLoadClass();
        System.out.println(clazz);
//        myClassLoader.dynamicLoadClass();
//        Class<?> clazz1 = Class.forName("com.sunxd.integration.platform.classloader.HelloWorld");
//        System.out.println(clazz1);
//
//        // 生成对象
//        Object obj = clazz.newInstance();
//        System.out.println(obj);
//
//        // 调用main方法
//        Method m = clazz.getMethod("main",String[].class);
//        Object invoke = m.invoke(obj, new Object[] { new String[] {} });
//        System.out.println(invoke);
//
//        //调用setName
//        Method m2 = clazz.getMethod("setName",String.class);
//        Object invoke2 = m2.invoke(obj,"zhangsan");
//        System.out.println(invoke2);
//
//        //getName
//        Method m1 = clazz.getMethod("getName");
//        Object invoke1 = m1.invoke(obj);
//        System.out.println(invoke1);
    }
}
