package com.sunxd.integration.platform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 集成平台
 * 动态注册controller
 * 动态消费dubbo服务
 */
@SpringBootApplication
public class PIntegrationPlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(PIntegrationPlatformApplication.class, args);
    }

}
