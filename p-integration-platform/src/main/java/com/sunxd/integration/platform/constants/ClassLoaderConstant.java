package com.sunxd.integration.platform.constants;

import com.sunxd.integration.platform.classloader.MyClassLoader;

import java.util.Objects;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/9/8 14:16
 */
public class ClassLoaderConstant {

    public static final String _JAVA = ".java";

    public static final String _CLASS = ".class";

    public static final String BASE_PACKAGE_NAME = MyClassLoader.class.getPackage().getName();

    public static final String CLASS_PATH;
    //回车加换行符
    public static final String RT = "\r\n";

    static {
        //获取编译后class文件所在的路径
        String str = Objects.requireNonNull(ClassLoaderConstant.class.getClassLoader().getResource("")).getPath();
        //获取出来的前面会有一个/,去掉前面的/
        CLASS_PATH = !str.startsWith("/") ? str : str.substring(1);
    }


    public static final String HTTP_SRC =
            "import com.alibaba.fastjson.JSON;" + RT +
                    "import com.google.common.base.Throwables;" + RT +
                    "import com.sunxd.integration.platform.util.RestTemplateUtil;" + RT +
                    "import java.util.Map;" + RT +
                    "import org.slf4j.Logger;" + RT +
                    "import org.slf4j.LoggerFactory;" + RT +
                    "import org.springframework.beans.factory.annotation.Autowired;" + RT +
                    "import org.springframework.web.bind.annotation.PostMapping;" + RT +
                    "import org.springframework.web.bind.annotation.RequestBody;" + RT +
                    "import org.springframework.web.bind.annotation.RestController;" + RT +
                    "import java.util.Map; " + RT +

                    "@RestController " + RT +
                    "public class #beanSimpleName#  {" + RT +
                    "   private static final Logger log = LoggerFactory.getLogger(#beanSimpleName#.class); " + RT +
                    "   public #beanSimpleName# () { " +
                    "    } " + RT +
                    "    @Autowired" + RT +
                    "    private RestTemplateUtil restTemplateUtil;" + RT +
                    "    @PostMapping(\"" + "#urlPath# " + "\")" + RT +
                    "    public String execute(@RequestBody Map<String, Object> data) {" + RT +
                    "        String urlStr = String.valueOf(data.get(\"url\"));" + RT +
                    "        String dataStr = JSON.toJSONString(data.get(\"data\"));" + RT +
                    "        log.info(\"rest start=> url: {} param: {}\", urlStr, dataStr);" + RT +
                    "        String res = null;" + RT +
                    "        try {" + RT +
                    "            res = restTemplateUtil.postCommon(urlStr, null, dataStr);" + RT +
                    "        } catch (Exception e) {" + RT +
                    "            log.info(\" res error => url ：{} ， message :  {}\", urlStr, Throwables.getStackTraceAsString(e));" + RT +
                    "        }" + RT +
                    "        log.info(\"rest end => url: {}, data : {}\", urlStr, res);" + RT +
                    "        return res;" + RT +
                    "    }" + RT +
                    "}";

    public static final String DUBBO_SRC =
            "import com.alibaba.fastjson.JSON;" + RT +
                    "import com.sunxd.base.common.entity.UserEntity;" + RT +
                    "import com.sunxd.dubbo.dubbo.api.service.user.IUserWriteService;" + RT +
                    "import org.apache.dubbo.config.annotation.DubboReference;" + RT +
                    "import org.springframework.web.bind.annotation.RequestBody;" + RT +
                    "import org.springframework.web.bind.annotation.RequestMapping;" + RT +
                    "import org.springframework.web.bind.annotation.PostMapping;" + RT +
                    "import org.springframework.web.bind.annotation.RestController;" + RT +
                    "import java.util.Map;" + RT +

                    "@RestController" + RT +
                    "public class #beanSimpleName# {" + RT +
                    "   public #beanSimpleName# () { " + RT +
                    "    } " + RT +
                    "    @DubboReference" +
                    "    private IUserWriteService service;" + RT +
                    "    @PostMapping(\"" + "#urlPath# " + "\")" + RT +
                    "    public int execute(@RequestBody Map<String, Object> data) {" + RT +
                    "        UserEntity param = JSON.parseObject(JSON.toJSONString(data), UserEntity.class);" + RT +
                    "        return service.add(param);" + RT +
                    "    }" + RT +
                    "}";
}
