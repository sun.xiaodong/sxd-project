package com.sunxd.integration.platform.controller;

import com.alibaba.fastjson.JSON;
import com.sunxd.integration.platform.util.RestTemplateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
public class TestController {

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    @PostMapping("api/test/demo2")
    public String execute(@RequestBody Map<String, Object> data) {
        log.info("test data :{}", JSON.toJSONString(data));
        return "test";
    }

}
