package com.sunxd.integration.platform.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Throwables;
import com.sunxd.integration.platform.util.RestTemplateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
public class CommonController {

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    @PostMapping("execute")
    public String execute(@RequestBody Map<String, Object> data) {
        String urlStr = String.valueOf(data.get("url"));
        String dataStr = JSON.toJSONString(data.get("data"));
        log.info("rest start=> url: {} param: {}", urlStr, dataStr);
        String res = null;
        try {
            res = restTemplateUtil.postCommon(urlStr, null, dataStr);
        } catch (Exception e) {
            log.info(" res error => url ：{} ， message :  {}", urlStr, Throwables.getStackTraceAsString(e));
        }
        log.info("rest end => url: {}, data : {}", urlStr, res);
        return res;
    }

}
