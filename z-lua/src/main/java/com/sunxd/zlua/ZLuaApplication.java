package com.sunxd.zlua;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZLuaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZLuaApplication.class, args);
    }

}
