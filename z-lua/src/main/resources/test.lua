-- print ("hello lua,my first")
-- print (b)
-- b = 5
-- print (b)
--
-- print (type("hello word"))
-- print (type("111"))
-- print (type(222))
-- print (type(true))
-- print (type(nil))
--
-- ---test table
-- tab1 = {key1 = "val1",key2 = "val2",key3 = "val3" , key4 = "val4"}
-- for k,v in pairs(tab1) do
--   print(k .. "---" .. v)
-- end
--
-- print(type(X))  -- 返回的是"nil"字符串
-- print(type(X)==nil)
-- print(type(X)=="nil")
--
--
-- ***** 逻辑判断
-- b = false
-- c = false
-- if b and c then
-- print("b c 都是 true")
-- else
-- print ("b 或 c 至少有一个false")
-- end
--
--
-- b = 1
-- c = false
-- if b==0 and c then
-- print("b c 都是 true")
-- else
-- print ("b 或 c 至少有一个false")
-- end
--
--
-- a = 5
-- local b = 60
-- function joke ()
--     c = 50
--     local d = 60
-- end
-- joke()
-- print(c,d)
--
-- do
--     local a = 6
--     b  = 11
--     print(a,b)
-- end
-- print(a,b)
--
--
-- a = "hello"  .. " word"
-- print(a)
--
-- a,b,c = 0,1,2
-- print(a,b,c)
--
-- tab1 ={}
-- tab1.["k1"]="www.baidu.com"
-- tab1.["k2"]="www.gogo.com"
-- print(tab1.k1,tab1.k2)
--
-- a = 3
-- if ( a == 3)
-- then
-- print("a ==3")
-- elseif ( a == 4)
-- then
-- print ("a ==4")
-- else
-- print ("a = ",a)
-- end
--
-- a = 3
-- b = 4
-- if(a ==3)
-- then
--     if(b ==4)
--     then
--     print ("a ==3 ,b ==4")
--     end
-- end
--
-- function add(n1,n2)
-- return n1+n2
-- end
-- print(add(1,2)
--
-- function max(n1,n2)
--     if(n1 > n2) then
--     return n1
--     else
--     return n2
--     end
-- end
-- print (max(1,2)）
--
-- -  取最大值，包含索引
-- function maxImun(a)
--     local mi = 1
--     local mv = a[mi]
--     for i,val in ipairs(a) do
--         if(val > mv) then
--             mi = i
--             mv = val
--         end
--     end
--     return mi ,mv
-- end
-- tab1 = {4,8,5,7,3,8,2}
-- print(maxImun(tab1))
-- print(tab1[3])
--
--
-- tab1 = {}
-- tab1[1]="1111"
-- tab1["a"]="aaa"
-- tab1["2"]="2222"
-- tab1["b"]="bbbb"
-- print(tab1[1])
-- tab2 = tab1
-- tab2[1]="++1111"
-- print(tab1[1])
-- print(tab2[1])
--
-- local a=3
-- local b=KEYS[1]
-- return a+b


-- local key = KESY[1]
-- local count = redis.call("incr",key)
-- return count

local url_args = ngx.req.get_uri_args()
for k,v in pairs(url_args) do
    if type(v) == "table" then
        ngx.say(k , table.concat(v , ",")
    else
        ngx.say(k,":",v)
    end
end