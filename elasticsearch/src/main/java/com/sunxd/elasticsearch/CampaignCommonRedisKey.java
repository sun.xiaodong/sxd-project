package com.sunxd.elasticsearch;

/**
 * @author Yangxiumei
 * Date: 2020-10-26 17:44
 * Description: 活动redis key
 **/
public class CampaignCommonRedisKey {

    private static final String CAMPAIGN_SEND_SMS_PREFIX = "campaign:sendSms:";

    private static final String CAMPAIGN_TICKET_PREFIX = "campaign:ticket:";

    private static final String CAMPAIGN_PRIZE_PREFIX = "campaign:prize:";

    private static final String CAMPAIGN_RANK_PREFIX = "campaignRank:";

    private static final String CAMPAIGN_CANDIDATE_PREFIX = "campaignCandidate:";

    private static final String CAMPAIGN_SIGN_UP_PREFIX = "campaignSignUp:";

    private static final String CAMPAIGN_WHITE_LIST_PREFIX = "campaign:white:list:";

    /**
     * 生成活动疲劳度限制发送短信次数key
     *
     * @param limit 限制次数
     * @param phone 手机号
     * @return redis key
     */
    public static String buildCampaignSendSmsNumberRedisKey(Integer limit, String phone) {
        return String.format(CAMPAIGN_SEND_SMS_PREFIX + "limit:%s:phone:%s:number", limit, phone);
    }

    /**
     * 生成用户活动机会数
     *
     * @param campaignId   活动ID
     * @param capabilityId 能力ID
     * @param ownerId      会员ID
     * @param ownerType    会员类型
     * @return redis key
     */
    public static String buildUserCampaignTicketRedisKey(String campaignId, String capabilityId, String ownerType, String ownerId) {
        return String.format(CAMPAIGN_TICKET_PREFIX + "campaignId:%s:capabilityId:%s:ownerType:%s:ownerId:%s:total", campaignId, capabilityId, ownerType, ownerId);
    }

    /**
     * 生成奖品库存redis key
     *
     * @param campaignId   活动ID
     * @param capabilityId 能力ID
     * @param prizeId      奖品ID
     * @return redis key
     */
    public static String buildPrizeStockRedisKey(String campaignId, String capabilityId, String prizeId) {
        return String.format(CAMPAIGN_PRIZE_PREFIX + "campaignId:%s:capabilityId:%s:prizeId:%s:stock", campaignId, capabilityId, prizeId);
    }

    /**
     * 排行redis key
     *
     * @param campaignId   活动ID
     * @param capabilityId 能力ID
     * @return redis key
     */
    public static String buildRankRedisKey(String campaignId, String capabilityId) {
        return String.format(CAMPAIGN_RANK_PREFIX + "campaignId:%s:capabilityId:%s:rank", campaignId, capabilityId);
    }

    /**
     * 投票对象redis key
     *
     * @param campaignId   活动ID
     * @param capabilityId 能力信息
     * @return redis key
     */
    public static String buildCandidateRedisKey(String campaignId, String capabilityId) {
        return String.format(CAMPAIGN_CANDIDATE_PREFIX + "campaignId:%s:capabilityId:%s:candidate", campaignId, capabilityId);
    }

    /**
     * 投票对象编号redis key
     *
     * @param campaignId   活动ID
     * @param capabilityId 能力信息
     * @return redis key
     */
    public static String buildCandidateUniqueNoRedisKey(String campaignId, String capabilityId) {
        return String.format(CAMPAIGN_CANDIDATE_PREFIX + "campaignId:%s:capabilityId:%s:uniqueNo", campaignId, capabilityId);
    }

    /**
     * 候选者编号生成redis key
     *
     * @param campaignId   活动ID
     * @param capabilityId 能力信息
     * @return redis key
     */
    public static String buildCandidateNoRedisKey(String campaignId, String capabilityId) {
        return String.format(CAMPAIGN_CANDIDATE_PREFIX + "campaignId:%s:capabilityId:%s:candidateNo", campaignId, capabilityId);
    }

    /**
     * 报名参与场次redis key
     *
     * @param campaignId   活动ID
     * @param capabilityId 能力ID
     * @param ownerType    会员类型
     * @param ownerId      用户ID
     * @param sessionIndex 场次号
     * @return redis key
     */
    public static String buildSignUpSessionJoinRedisKey(String campaignId, String capabilityId, String ownerType, String ownerId, Integer sessionIndex) {
        return String.format(CAMPAIGN_SIGN_UP_PREFIX + "campaignId:%s:capabilityId:%s:ownerType:%s:ownerId:%s:sessionIndex:%s:number",
                campaignId, capabilityId, ownerType, ownerId, sessionIndex);
    }

    /**
     * 报名场次参加总人数redis key
     *
     * @param campaignId   活动ID
     * @param capabilityId 能力ID
     * @param sessionIndex 场次号
     * @return redis key
     */
    public static String buildSignUpSessionJoinTotalRedisKey(String campaignId, String capabilityId, Integer sessionIndex) {
        return String.format(CAMPAIGN_SIGN_UP_PREFIX + "campaignId:%s:capabilityId:%s:sessionIndex:%s:total", campaignId, capabilityId, sessionIndex);
    }

    /**
     * 投票关联报名redis key
     *
     * @param campaignId   活动ID
     * @param capabilityId 能力ID
     * @return redis key
     */
    public static String buildVoteLinkSignUpRedisKey(String campaignId, String capabilityId) {
        return String.format(CAMPAIGN_CANDIDATE_PREFIX + "campaignId:%s:capabilityId:%s:voteLinkSignUp", campaignId, capabilityId);
    }


    /**
     * 生成奖品库存redis key
     *
     * @param campaignId   活动ID
     * @param capabilityId 能力ID
     * @param prizeId      奖品ID
     * @return redis key
     */
    public static String buildPrizeStockJudgeRedisKey(String campaignId, String capabilityId, String prizeId) {
        return String.format(CAMPAIGN_PRIZE_PREFIX + "campaignId:%s:capabilityId:%s:prizeId:%s:stock:judge", campaignId, capabilityId, prizeId);
    }

    public static String buildCampaignWhiteListRedisKey(String campaignId, String capabilityId) {
        return String.format(CAMPAIGN_WHITE_LIST_PREFIX + "campaignId:%s:capabilityId:%s", campaignId, capabilityId);
    }
}
