package com.sunxd.elasticsearch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sunxd.elasticsearch.CampaignCommonRedisKey.buildCampaignWhiteListRedisKey;

@SpringBootApplication
@RestController
@RequestMapping("redis/test/")
public class ElasticsearchApplication {
    private String naem;

    @Autowired
    private RedisTemplate stringRedisTemplate;

    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchApplication.class, args);
    }


    @GetMapping("add")
    public String add(@RequestParam  String key){
        Map<String,String> map = new HashMap<>();
        map.put("11","11");
        map.put("22","22");
        map.put("33","33");
        stringRedisTemplate.opsForHash().putAll(getKey(),map);
        return "1";
    }

    @GetMapping("delete")
    public String delete(@RequestParam  String key){
List<String> list = new ArrayList<>();
list.add("11");
list.add("22");
list.stream().forEach(x -> stringRedisTemplate.opsForHash().delete(getKey(),x));

        return "1";
    }
    @GetMapping("contain")
    public Boolean contain(@RequestParam String key){
        return stringRedisTemplate.opsForHash().hasKey(getKey(),key);
    }

    private String getKey(){
        return buildCampaignWhiteListRedisKey("12","34");
    }

}
