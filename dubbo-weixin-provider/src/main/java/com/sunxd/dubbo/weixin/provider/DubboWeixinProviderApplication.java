package com.sunxd.dubbo.weixin.provider;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableDubbo
public class DubboWeixinProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboWeixinProviderApplication.class, args);
    }

}
