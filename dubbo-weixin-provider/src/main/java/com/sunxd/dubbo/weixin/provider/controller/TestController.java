package com.sunxd.dubbo.weixin.provider.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: 作者名称
 * @date: 2021-05-09 23:47
 **/
@RestController
@RequestMapping("test")
public class TestController {

    @RequestMapping("path")
    public String path(){
        return "path";
    }
}
