package com.sunxd.study.sharding.jdbc;

import com.sunxd.study.sharding.jdbc.dao.UserDefaultDao;
import com.sunxd.study.sharding.jdbc.po.UserDefaultPO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserDefaultTest {


    @Autowired
    private UserDefaultDao userDefaultDao;


    /**
     * 配置默认的分库策略，数据的存储
     */
    @Test
    void test2() {
        for (int i = 1; i < 10; i++) {
            UserDefaultPO userPO = UserDefaultPO.builder()
                    .dbId(i)
                    .tbId(i)
                    .password("password")
                    .phone("password")
                    .userId(i)
                    .userName("userName").build();
            userDefaultDao.create(userPO);
            System.out.println(userPO.getId());
        }
    }

}
