package com.sunxd.study.sharding.jdbc;

import com.sunxd.study.sharding.jdbc.dao.UserDao;
import com.sunxd.study.sharding.jdbc.po.UserPO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class UserTest {

    @Autowired
    private UserDao userDao;

    @Test
    void create() {
//        根据不同的配置文件，测试分库分表的功能
        for (int i = 1; i < 10; i++) {
            UserPO userPO = UserPO.builder()
                    .dbId(i)
                    .tbId(i)
                    .password("password")
                    .phone("password")
                    .userId(i)
                    .userName("userName").build();
            userDao.create(userPO);
            System.out.println(userPO.getId());
        }
    }

    /**
     * 验证分库分表时查询执行实际sql
     * 1. userPO.setDbId(6); 设置分库建： 则表会循环查
     * 2. userPO.setDbId(6); 设置分表建： 则库会循环查
     * 3. userPO.setDbId(6);
     *    userPO.setDbId(6); 设置分库分表建： 则精准定位查
     * 4. userPO.setUserId(6); 笛卡尔积，各个库各个表循环查
     */
    @Test
    void findById() {
//        根据不同的配置文件，测试分库分表的功能
        UserPO userPO = new UserPO();
        userPO.setDbId(6);
//        userPO.setTbId(1);
        List<UserPO> list = userDao.list(userPO);
        System.out.println(list);
    }


}
