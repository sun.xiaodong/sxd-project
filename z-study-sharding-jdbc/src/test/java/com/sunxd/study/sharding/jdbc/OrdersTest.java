package com.sunxd.study.sharding.jdbc;

import com.sunxd.study.sharding.jdbc.dao.OrdersDao;
import com.sunxd.study.sharding.jdbc.po.OrdersPO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class OrdersTest {


    @Autowired
    private OrdersDao ordersDao;

    @Test
    void create() {

//        根据不同的配置文件，测试分库分表的功能
        for (int i = 1; i < 10; i++) {
            OrdersPO userPO = OrdersPO.builder()
                    .userId(i)
                    .shopId(i)
                    .build();
            ordersDao.create(userPO);
            System.out.println(userPO.getId());
        }
    }
}
