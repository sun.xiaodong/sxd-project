package com.sunxd.study.sharding.jdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZStudyShardingJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZStudyShardingJdbcApplication.class, args);
    }

}
