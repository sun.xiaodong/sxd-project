package com.sunxd.study.sharding.jdbc.po;

import com.sunxd.study.sharding.jdbc.base.BasePo;
import lombok.*;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/2/28 20:59
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrdersPO extends BasePo {
    private static final long serialVersionUID = -512564778405347714L;

    private Long id;

    private Integer shopId;

    private Integer userId;

}
