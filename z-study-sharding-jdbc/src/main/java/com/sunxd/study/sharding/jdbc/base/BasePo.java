package com.sunxd.study.sharding.jdbc.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/2/28 11:07
 */

public class BasePo implements Serializable {
    private static final long serialVersionUID = -2019315057417298681L;
    private String creatorId;
    private String creator;
    private Date createTime;
    private String updateId;
    private String updater;
    private Date updateTime;
    private Boolean deleteFlag;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateId() {
        return updateId;
    }

    public void setUpdateId(String updateId) {
        this.updateId = updateId;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}
