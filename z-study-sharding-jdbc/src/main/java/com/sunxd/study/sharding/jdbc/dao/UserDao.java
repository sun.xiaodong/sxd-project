package com.sunxd.study.sharding.jdbc.dao;

import com.sunxd.study.sharding.jdbc.base.MyBatisDao;
import com.sunxd.study.sharding.jdbc.po.UserPO;
import org.springframework.stereotype.Component;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/2/28 11:18
 */
@Component
public class UserDao extends MyBatisDao<UserPO> {
}
