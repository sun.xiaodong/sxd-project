package com.sunxd.study.sharding.jdbc.po;

import com.sunxd.study.sharding.jdbc.base.BasePo;
import lombok.*;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/2/28 11:07
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserPO extends BasePo {
    private static final long serialVersionUID = -8657950134006044880L;


    private Long id;


    private Integer dbId;
    private Integer tbId;
    private Integer userId;

    private String userName;

    private String password;

    private String phone;



}
