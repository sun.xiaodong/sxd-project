DROP TABLE IF EXISTS t_orders;
CREATE TABLE `t_orders`  (
  `id` int(11) unsigned  NOT NULL AUTO_INCREMENT COMMENT '唯一主键',
  `shopId` VARCHAR(255) NOT NULL ,
  `userId` VARCHAR(255) NOT NULL ,
  `creator` varchar(32)   NOT NULL DEFAULT '' COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updater` varchar(32)   NOT NULL DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `delete_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标识【0：未删除，1：已删除】',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4  COMMENT = '用户表' ROW_FORMAT = Dynamic;


DROP TABLE IF EXISTS t_user;
CREATE TABLE `t_user`  (
  `id` int(11) unsigned  NOT NULL AUTO_INCREMENT COMMENT '唯一主键',
  `dbId` VARCHAR(255) NOT NULL ,
  `tbId` VARCHAR(255) NOT NULL ,
  `user_id` VARCHAR(255) NOT NULL ,
  `user_name` VARCHAR(255) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `phone` VARCHAR(255) NOT NULL,
  `creator` varchar(32)   NOT NULL DEFAULT '' COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updater` varchar(32)   NOT NULL DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `delete_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标识【0：未删除，1：已删除】',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4  COMMENT = '用户表' ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS t_user_default;
CREATE TABLE `t_user_default`  (
  `id` int(11) unsigned  NOT NULL AUTO_INCREMENT COMMENT '唯一主键',
    `dbId` VARCHAR(255) NOT NULL ,
  `tbId` VARCHAR(255) NOT NULL ,
  `user_id` VARCHAR(255) NOT NULL ,
  `user_name` VARCHAR(255) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `phone` VARCHAR(255) NOT NULL,
  `creator` varchar(32)   NOT NULL DEFAULT '' COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updater` varchar(32)   NOT NULL DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `delete_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标识【0：未删除，1：已删除】',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4  COMMENT = '用户表' ROW_FORMAT = Dynamic;
DROP TABLE IF EXISTS t_user_1;
CREATE TABLE `t_user_1`  (
  `id` int(11) unsigned  NOT NULL AUTO_INCREMENT COMMENT '唯一主键',
  `dbId` VARCHAR(255) NOT NULL ,
  `tbId` VARCHAR(255) NOT NULL ,
  `user_id` VARCHAR(255) NOT NULL ,
  `user_name` VARCHAR(255) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `phone` VARCHAR(255) NOT NULL,
  `creator` varchar(32)   NOT NULL DEFAULT '' COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updater` varchar(32)   NOT NULL DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `delete_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标识【0：未删除，1：已删除】',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4  COMMENT = '用户表' ROW_FORMAT = Dynamic;
DROP TABLE IF EXISTS t_user_2;
CREATE TABLE `t_user_2`  (
  `id` int(11) unsigned  NOT NULL AUTO_INCREMENT COMMENT '唯一主键',
  `dbId` VARCHAR(255) NOT NULL ,
  `tbId` VARCHAR(255) NOT NULL ,
  `user_id` VARCHAR(255) NOT NULL ,
  `user_name` VARCHAR(255) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `phone` VARCHAR(255) NOT NULL,
  `creator` varchar(32)   NOT NULL DEFAULT '' COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updater` varchar(32)   NOT NULL DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `delete_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标识【0：未删除，1：已删除】',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4  COMMENT = '用户表' ROW_FORMAT = Dynamic;
DROP TABLE IF EXISTS t_user_3;
CREATE TABLE `t_user_3`  (
  `id` int(11) unsigned  NOT NULL AUTO_INCREMENT COMMENT '唯一主键',
  `dbId` VARCHAR(255) NOT NULL ,
  `tbId` VARCHAR(255) NOT NULL ,
  `user_id` VARCHAR(255) NOT NULL ,
  `user_name` VARCHAR(255) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `phone` VARCHAR(255) NOT NULL,
  `creator` varchar(32)   NOT NULL DEFAULT '' COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `updater` varchar(32)   NOT NULL DEFAULT '' COMMENT '更新人',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `delete_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标识【0：未删除，1：已删除】',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4  COMMENT = '用户表' ROW_FORMAT = Dynamic;

