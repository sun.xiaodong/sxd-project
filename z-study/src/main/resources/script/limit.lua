local k1 = KEYS[1]
local k2 = KEYS[2]
local k3 = KEYS[3]
-- argv 1:max_burst 2:rate 3:last_mill_second
local ratelimit_info=redis.pcall("HMGET",KEYS[1],"last_mill_second","curr_permits","max_burst","rate")
local last_mill_second=ratelimit_info[1]
local curr_permits=tonumber(ratelimit_info[2])
local max_burst=tonumber(ratelimit_info[3])
local rate=tonumber(ratelimit_info[4])

if (last_mill_second ~= nil) then
    local time_interval= math.floor((ARGV[3]-last_mill_second)/1000)
    if( time_interval >= 1 ) then
        curr_permits = curr_permits - time_interval * rate + 1
    else
        if (curr_permits >= max_burst) then
           -- 溢出，直接拒绝
        end
    end
else
    redis.pcall("HMSET",KEYS[1],"last_mill_second",ARGV[3],"curr_permits",1,"max_burst",ARGV[1],"rate",ARGV[2])
end

return true