package com.sunxd.zstudy.guava.limit;

import com.google.common.util.concurrent.RateLimiter;

/**
 * RateLimit 限流
 * @author: 作者名称
 * @date: 2021-08-24 20:38
 **/
public class RtLimit {

    private static RateLimiter rateLimiter = RateLimiter.create(2);
    private static RateLimiter rateLimiter1 = RateLimiter.create(3);

    public static void main(String[] args) {
        System.out.println(rateLimiter.acquire());
        System.out.println(rateLimiter.acquire());
        System.out.println(rateLimiter.acquire());
    }

}
