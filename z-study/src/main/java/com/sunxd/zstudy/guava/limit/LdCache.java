package com.sunxd.zstudy.guava.limit;


import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * LoadingCache 本地缓存
 * @author: 作者名称
 * @date: 2021-08-24 19:31
 **/
public class LdCache {



    public static void main(String[] args) throws ExecutionException, InterruptedException {
        LoadingCache<Integer,String> GLOBAL_CACHE = CacheBuilder.newBuilder()
                .expireAfterAccess(2L, TimeUnit.SECONDS)
                .expireAfterWrite(2L, TimeUnit.SECONDS)
                .build(new CacheLoader<Integer,String>(){
                    @Override
                    public String load(Integer key){
                        return "";
                    }
                });
        GLOBAL_CACHE.put(1,"1");
        System.out.println(GLOBAL_CACHE.get(1));
        Thread.sleep(1000);
        System.out.println(GLOBAL_CACHE.get(1));
        Thread.sleep(3000);
        System.out.println(GLOBAL_CACHE.getIfPresent(1));
    }

}
