package com.sunxd.zstudy.controller;

import com.sunxd.zstudy.designPattern.support.chainOfResponsibility.AbstractHandlerChain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: sun.xd
 * @date: 2021-10-14 19:04
 **/
@RestController
public class ChainOfResponsibility {

    @Autowired
    @Qualifier("firstHandlerChain")
    private AbstractHandlerChain firstHandlerChain;

    @RequestMapping("/check")
    public void check(String s){
        firstHandlerChain.handler(s);
    }
}
