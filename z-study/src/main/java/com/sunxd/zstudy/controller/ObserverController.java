package com.sunxd.zstudy.controller;

import com.sunxd.zstudy.designPattern.support.observer.EventConfig;
import com.sunxd.zstudy.designPattern.support.observer.EventObForSleep;
import com.sunxd.zstudy.designPattern.support.observer.EventObForWakeUp;
import com.sunxd.zstudy.designPattern.support.observer.EventPublisher;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.EventObject;

/**
 * @author: sun.xd
 * @date: 2021-10-18 10:06
 **/
@RestController
@RequiredArgsConstructor
public class ObserverController {

    private final EventPublisher eventPublisher;


    @RequestMapping("/observer1")
    public String observer1(){

        eventPublisher.publishEvent(new EventObForWakeUp("zhangsan"));
        return "su";
    }


    @RequestMapping("/observer2")
    public String observer2(){

        eventPublisher.publishEvent(new EventObForSleep(1L));
        return "sleep";
    }

}
