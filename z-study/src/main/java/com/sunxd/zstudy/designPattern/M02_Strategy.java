package com.sunxd.zstudy.designPattern;

import com.sunxd.zstudy.designPattern.support.strategy.Cat;
import com.sunxd.zstudy.designPattern.support.strategy.Dog;
import com.sunxd.zstudy.designPattern.support.strategy.Sorter;

import java.util.Arrays;

/**
 * 策略模式 和 委派模式很像
 *  体现在sort 的方法，根据人为的定义规则执行（可以有多个）
 * @author: 作者名称
 * @date: 2021-10-13 17:18
 **/
public class M02_Strategy {
    public static void main(String[] args) {
        Dog [] dogList = new Dog[6];
        dogList[0] = new Dog(1);
        dogList[1] =(new Dog(10));
        dogList[2] =(new Dog(5));
        dogList[3] =(new Dog(2));
        dogList[4] =(new Dog(8));
        dogList[5] =(new Dog(7));
        Sorter<Dog> dogSorter = new Sorter<>();
        dogSorter.sort(dogList, (o1, o2) -> Integer.compare(o1.getAge(), o2.getAge()));
        System.out.println(Arrays.toString(dogList));

        Cat[] cats = new Cat[6];
        cats[0] = new Cat(1);
        cats[1] =(new Cat(10));
        cats[2] =(new Cat(5));
        cats[3] =(new Cat(2));
        cats[4] =(new Cat(8));
        cats[5] =(new Cat(7));
        Sorter<Cat> catSorter = new Sorter<>();
        catSorter.sort(cats, (o1, o2) -> Integer.compare(o1.getAge(), o2.getAge()));
        System.out.println(Arrays.toString(dogList));


    }
}
