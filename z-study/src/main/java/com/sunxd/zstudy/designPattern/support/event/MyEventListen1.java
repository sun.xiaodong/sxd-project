package com.sunxd.zstudy.designPattern.support.event;

import com.alibaba.fastjson.JSON;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class MyEventListen1 implements ApplicationListener<TaskEvent> {


    @Override
    @Async
    public void onApplicationEvent(TaskEvent taskEvent) {
        TaskInfo taskInfo = taskEvent.getTaskInfo();
        System.out.println(Thread.currentThread().getName()+" 线程执行 event1 listen, info:" + JSON.toJSONString(taskInfo));

    }
}
