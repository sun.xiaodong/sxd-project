package com.sunxd.zstudy.designPattern.support.strategy;

import lombok.Data;

/**
 * 模仿java Comparable
 * @author: 作者名称
 * @date: 2021-10-13 17:47
 **/
@Data
public class Dog implements MyComparable<Dog> {

    private int age;

    public Dog (int age){
        this.age = age;
    }

    @Override
    public int compareTo(Dog d) {
        if (this.age < d.getAge()) return -1 ;
        else if(this.age > d.getAge()) return 1;
        return 0;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "age=" + age +
                '}';
    }




}
