package com.sunxd.zstudy.designPattern.support.event;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class BaseEvent<T> extends ApplicationEvent {

    private T taskInfo;

    public BaseEvent(Object source) {
        super(source);
    }
}
