package com.sunxd.zstudy.designPattern.support.factory;

/**
 * @author: 作者名称
 * @date: 2021-10-13 20:33
 **/
public interface MoveAble {

    void go();

}
