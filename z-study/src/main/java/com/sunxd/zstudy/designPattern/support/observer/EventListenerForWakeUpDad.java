package com.sunxd.zstudy.designPattern.support.observer;

import org.springframework.stereotype.Component;

/**
 * @author: sun.xd
 * @date: 2021-10-18 11:54
 **/
@Component
public class EventListenerForWakeUpDad implements EventListener<EventObForWakeUp> {

    @Override
    public void wakeUp(EventObForWakeUp eventOb) {
        System.out.println("dad help ...");

        System.out.println("source:" + eventOb.getSource());


    }
}
