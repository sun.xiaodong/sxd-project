package com.sunxd.zstudy.designPattern;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 组合模式
 * @author: sun.xd
 * @date: 2021-10-15 10:02
 **/
public class M09_Proxy {


    public static void main(String[] args) {

//        MoveAble m = new Tank();
//        m.move();

        // 静态代理
        TankTimeProxy tankTimeProxy = new TankTimeProxy(new Tank());
        tankTimeProxy.move();

    }


    interface MoveAble{
        void move();
    }

    static class Tank implements MoveAble{

        @Override
        public void move() {
            long l = System.currentTimeMillis();

            System.out.println(" move ....");
            try {
                TimeUnit.SECONDS.sleep(new Random().nextInt(10));
                System.out.println(" move cost: " + (System.currentTimeMillis() -l) );
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    // 静态代理
    static class TankTimeProxy implements MoveAble{

        MoveAble moveAble;

        public TankTimeProxy(MoveAble moveAble) {
            this.moveAble = moveAble;
        }

        @Override
        public void move() {
            moveAble.move();
        }
    }


}
