package com.sunxd.zstudy.designPattern.support.chainOfResponsibility;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author: sun.xd
 * @date: 2021-10-14 18:05
 **/
@Component
@Order(2)
public class DogHandler extends AbstractHandlerChain {

    @Override
    public boolean execute(String s) {
        System.out.println("dog handler");
        return true;
    }

    @Override
    public boolean canExecute(String s) {
        return Objects.equals(s, AnimalType.DOG.getName());
    }
}
