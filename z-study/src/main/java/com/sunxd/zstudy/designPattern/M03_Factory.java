package com.sunxd.zstudy.designPattern;

import com.sunxd.zstudy.designPattern.support.factory.*;

/**
 * 工厂模式 - 工厂方法、抽象工厂
 *  形容词用接口，名词用抽象类
 *
 * @author: 作者名称
 * @date: 2021-10-13 17:18
 **/
public class M03_Factory {


    public static void main(String[] args) {
        MoveAble car = new Car();
        car.go();
        MoveAble plane = new Plane();
        plane.go();

        AbstractMoveAbleFactory moveAbleFactory = new CarFactory();
        MoveAble moveAble =   moveAbleFactory.creatCar();

    }


    // 工厂方法 简单工厂 扩展性不好
    class SimpleVehicleFactory{
        public Car creatCar(){return new Car();}
        public Plane creatPlane(){return new Plane();}
    }
}
