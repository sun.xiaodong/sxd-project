package com.sunxd.zstudy.designPattern.support.strategy;

/**
 * @author: 作者名称
 * @date: 2021-10-13 19:27
 **/
public interface Comparator<T> {

    int compare(T o1,T o2);

}
