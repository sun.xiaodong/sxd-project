package com.sunxd.zstudy.designPattern.support.factory;

/**
 * @author: 作者名称
 * @date: 2021-10-13 20:32
 **/
public class Car implements MoveAble {
    @Override
    public void go() {
        System.out.println("汽车");
    }
}
