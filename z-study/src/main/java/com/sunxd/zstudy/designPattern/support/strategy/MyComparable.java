package com.sunxd.zstudy.designPattern.support.strategy;

/**
 * @author: 作者名称
 * @date: 2021-10-13 17:48
 **/
public interface MyComparable<T> {

    int compareTo(T o);

}
