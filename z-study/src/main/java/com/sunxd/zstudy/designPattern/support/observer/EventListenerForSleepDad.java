package com.sunxd.zstudy.designPattern.support.observer;

import org.springframework.stereotype.Component;

/**
 * @author: sun.xd
 * @date: 2021-10-18 11:54
 **/
@Component
public class EventListenerForSleepDad implements EventListener<EventObForSleep> {

    @Override
    public void wakeUp(EventObForSleep eventOb) {

        System.out.println("dad pai pai ...");

        System.out.println("source:" + eventOb.getSource());

    }
}
