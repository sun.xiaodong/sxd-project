package com.sunxd.zstudy.designPattern.support.event;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskInfo {

    private Long id;

    private String name;

    private boolean recieve;
}
