package com.sunxd.zstudy.designPattern.support.observer;

/**ø
 * @author: sun.xd
 * @date: 2021-10-18 11:48
 **/
public class EventObForSleep extends EventOb<Long> {


    public EventObForSleep(Long source){
        super(source);
    }

}
