package com.sunxd.zstudy.designPattern.support.event;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TaskEvent extends BaseEvent<TaskInfo> {

    private TaskInfo taskInfo;

    public TaskEvent(TaskInfo taskInfo) {
        super(taskInfo);
        this.taskInfo = taskInfo;
    }
}
