package com.sunxd.zstudy.designPattern.support.observer;

/**
 * @author: sun.xd
 * @date: 2021-10-18 11:52
 **/
public interface EventListener<E> {

    void wakeUp(E eventOb);

}
