package com.sunxd.zstudy.designPattern.support.strategy;

/**
 * @author: 作者名称
 * @date: 2021-10-13 17:29
 **/
public class Sorter<T> {

    public  void sort(T[] arr,Comparator<T> comparator){
        for (int i = 0; i < arr.length; i++) {
            int minP = i;
            for (int j = i + 1; j < arr.length; j++) {
                minP = comparator.compare(arr[minP],arr[j]) == -1 ? minP : j;
            }
            swap(arr,i,minP);
        }
    }

    void swap(T [] arr,int i,int j){
        T temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
//        System.out.println(arr[i]+" " +arr[j]);
//        System.out.println(Arrays.toString(arr));

    }



}
