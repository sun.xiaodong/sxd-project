package com.sunxd.zstudy.designPattern;

import lombok.Data;

/**
 * 装饰器  （Java 的I/O 实战）
 *  对某个功能对横向扩展，比如这里是对狗的其他功能新增。
 *  如果新增对猫的使用，则 新增类 （Cat implements Animal） -》 (ManDog extends Decorator)
 * @author: sun.xd
 * @date: 2021-10-14 11:23
 **/
public class M05_Decorator {


    public static void main(String[] args) {
        Animal animal1 = new ManDog(new Dog());
        animal1.go();

//        静态代理啊
        Animal animal2 = new WoManDog(new Dog());
        animal2.go();
    }

    interface Animal{
        void go();
    }


    static class Dog implements Animal{

        @Override
        public void go() {
            System.out.println("dog eat...");
        }
    }


    @Data
    static abstract class Decorator implements Animal{
        private Animal animal;

        public Decorator(Animal animal){
            this.animal = animal;
        }

        @Override
        public void go() {
            animal.go();
        }
    }

    // 功能拓展
    static class ManDog extends Decorator{

        public ManDog(Animal animal) {
            super(animal);
        }

        @Override
        public void go() {
            sleep();
            super.go();
        }
        //拓展的功能
        public void sleep(){
            System.out.println("高质量男狗");
        }

    }
    static class WoManDog extends Decorator{

        public WoManDog(Animal animal) {
            super(animal);
        }

        @Override
        public void go() {
            sleep();
            super.go();
        }
        //拓展的功能
        public void sleep(){
            System.out.println("高质量女狗");
        }

    }


}

