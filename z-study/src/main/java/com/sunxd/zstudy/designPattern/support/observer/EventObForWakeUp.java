package com.sunxd.zstudy.designPattern.support.observer;

/**ø
 * @author: sun.xd
 * @date: 2021-10-18 11:48
 **/
public class EventObForWakeUp extends EventOb<String> {

    public EventObForWakeUp(String source){
        super(source);
    }

}
