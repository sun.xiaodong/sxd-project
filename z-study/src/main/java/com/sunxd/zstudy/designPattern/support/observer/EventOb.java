package com.sunxd.zstudy.designPattern.support.observer;

import lombok.Data;

/**
 * @author: sun.xd
 * @date: 2021-10-18 11:48
 **/
@Data
public class EventOb<T> {

    public T source;

    protected EventOb(T source){
        this.source = source;
    }

    public T getSource() {
        return source;
    }
}
