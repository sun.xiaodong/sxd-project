package com.sunxd.zstudy.designPattern.support.observer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @author: sun.xd
 * @date: 2021-10-18 15:28
 **/
@Component
@RequiredArgsConstructor
public class EventPublisher {

    private final EventConfig eventConfig;

    public void publishEvent(EventOb eventOb){
        eventConfig.execute(eventOb.getClass().getName(),eventOb);
    }


}
