package com.sunxd.zstudy.designPattern.support.chainOfResponsibility;

import lombok.Data;

/**
 * @author: sun.xd
 * @date: 2021-10-14 18:04
 **/
@Data
public abstract class AbstractHandlerChain {

    private AbstractHandlerChain nextHandler;

    public boolean handler(String s){
        // todo 添加找不到对应处理器的处理，或者加个默认的处理器
        if(canExecute(s)){
            execute(s);
        }else {
            nextHandler.handler(s);
        }
        return false;
    }
    public abstract boolean execute(String s);

    public abstract boolean canExecute(String s);
}
