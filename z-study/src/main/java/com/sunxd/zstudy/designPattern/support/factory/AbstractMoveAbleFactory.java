package com.sunxd.zstudy.designPattern.support.factory;

/**
 * @author: sun.xd
 * @date: 2021-10-14 10:50
 **/
public abstract class AbstractMoveAbleFactory {

   public abstract MoveAble creatCar();

}
