package com.sunxd.zstudy.designPattern.support.chainOfResponsibility;

import lombok.Getter;

/**
 * @author: sun.xd
 * @date: 2021-10-14 17:42
 **/
@Getter
public enum AnimalType {
    DOG("dog","狗"),
    CAT("cat","猫"),
    MOUSE("mouse","老鼠"),
    ;
    private String name;
    private String desc;

    AnimalType(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }


}
