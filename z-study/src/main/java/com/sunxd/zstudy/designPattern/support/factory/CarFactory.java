package com.sunxd.zstudy.designPattern.support.factory;

/**
 * @author: 作者名称
 * @date: 2021-10-13 23:22
 **/
public class CarFactory extends AbstractMoveAbleFactory{

    @Override
    public MoveAble creatCar() {
        return new Car();
    }
}
