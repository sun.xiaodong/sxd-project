package com.sunxd.zstudy.designPattern;

import com.sunxd.zstudy.designPattern.support.chainOfResponsibility.AbstractHandlerChain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 责任链 配置
 *  将处理器构造成一个执行链，返回第一个bean作为起始位置
 * @author: sun.xd
 * @date: 2021-10-14 14:26
 **/
@Configuration
public class M06_ChainOfResponsibility2 {

    @Autowired
    List<AbstractHandlerChain> abstractHandlerChains;

    @Bean(name = "firstHandlerChain")
    public AbstractHandlerChain  firstHandlerChain(){
        int size = abstractHandlerChains.size();
        for (int i=0;i<size;i++){
            if (i==size-1){
                abstractHandlerChains.get(i).setNextHandler(null);
            } else {
                abstractHandlerChains.get(i).setNextHandler(abstractHandlerChains.get(i+1));
            }
        }
        return abstractHandlerChains.get(0);
    }





}
