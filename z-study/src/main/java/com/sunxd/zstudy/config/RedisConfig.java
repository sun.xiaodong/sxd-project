package com.sunxd.zstudy.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author: 作者名称
 * @date: 2021-05-31 17:09
 **/
@Configuration
public class RedisConfig {

    @Bean
    public RedisTemplate redisTemplate(RedisConnectionFactory fc){

        RedisSerializer<String> redisSerializer = new StringRedisSerializer();
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        RedisTemplate rt = new RedisTemplate<>();
        //解决查询缓存转换异常的问题
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        rt.setConnectionFactory(fc);
        rt.setKeySerializer(redisSerializer);
        rt.setValueSerializer(jackson2JsonRedisSerializer);
        rt.setHashValueSerializer(jackson2JsonRedisSerializer);
        rt.setHashKeySerializer(redisSerializer);
        return rt;
    }

    @Bean
    public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory fc){

        RedisSerializer<String> redisSerializer = new StringRedisSerializer();
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        StringRedisTemplate rt = new StringRedisTemplate();
        //解决查询缓存转换异常的问题
        ObjectMapper om = new ObjectMapper();
        jackson2JsonRedisSerializer.setObjectMapper(om);
        rt.setConnectionFactory(fc);
        rt.setValueSerializer(jackson2JsonRedisSerializer);
        rt.setHashValueSerializer(jackson2JsonRedisSerializer);
        rt.setKeySerializer(redisSerializer);
        rt.setHashKeySerializer(redisSerializer);
        return rt;
    }


}
