package com.sunxd.zstudy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;

/**
 * 加载lua脚本文件
 * @author: 作者名称
 * @date: 2021-08-25 10:57
 **/

@Configuration
public class LuaConfig {

    @Bean
    public DefaultRedisScript<Boolean> redisLimitScript() {
        DefaultRedisScript<Boolean> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("script/limit.lua")));
        redisScript.setResultType(Boolean.class);
        return redisScript;
    }

    @Bean
    public DefaultRedisScript<Boolean> redisEqScript() {
        DefaultRedisScript<Boolean> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("script/eq.lua")));
        redisScript.setResultType(Boolean.class);
        return redisScript;
    }
    @Bean
    public DefaultRedisScript<String> redisGetsScript() {
        DefaultRedisScript<String> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("script/get.lua")));
        redisScript.setResultType(String.class);
        return redisScript;
    }
}
