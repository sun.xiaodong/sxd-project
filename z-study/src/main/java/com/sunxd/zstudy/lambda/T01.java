package com.sunxd.zstudy.lambda;

import java.util.function.Function;

/**
 * 使用lambda代替接口的实现
 * @author: 作者名称
 * @date: 2021-10-13 10:50
 **/
public class T01 {

    public static void main(String[] args) {
        Function f = new Function() {
            @Override
            public Object apply(Object o) {
                System.out.println("1");
                return null;
            }
        };
        f.apply(null);
    }

    private static int operate(int a, int b,MathOperation operation){
        return operation.operation(a,b);
    }


    interface MathOperation {
        int operation(int a , int b);
    }


}
