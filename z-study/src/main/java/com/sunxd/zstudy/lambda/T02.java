package com.sunxd.zstudy.lambda;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * :: 引用方法
 * @author: 作者名称
 * @date: 2021-10-13 11:09
 **/
public class T02 {

    public static void main(String[] args) {
        List<Integer> lists = Lists.newArrayList();
        for (int i = 0; i < 10; i++) {
            lists.add(i);
        }
        lists.forEach(T02::print);
    }

    private static void print(Integer a){
        System.out.println(a);
    }
}
