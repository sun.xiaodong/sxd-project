package com.sunxd.zstudy.proxy.javassist;

public class Student {

    private String name;

    public Student() {
    }

    public Student(String name) {
        this.name = name;
    }

    public void wakeup() {
        System.out.println(String.format("学生%s早晨醒来啦",name));
    }

    public void sleep() {
        System.out.println(String.format("学生%s]晚上睡觉啦",name));
    }
}
