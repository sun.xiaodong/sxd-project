package com.sunxd.zstudy.proxy.cglib;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author: cglib 代理器
 * @date: 2021-04-10 23:42
 **/
@EqualsAndHashCode
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CglibProxy implements MethodInterceptor {

        private Object source;

    public Object createProxy(){
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(source.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("代理前操作。。。");
        method.invoke(source,objects);
        System.out.println("代理后操作。。。");
        return null;
    }

}
