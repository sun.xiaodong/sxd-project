package com.sunxd.zstudy.proxy.asm.test1;
 
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
 
public class CreateGetMethodAdapter extends MethodVisitor implements Opcodes{
 
	public CreateGetMethodAdapter(MethodVisitor mv) {
		super(Opcodes.ASM4, mv);
	}
 
	@Override
	public void visitCode(){
		mv.visitVarInsn(ALOAD, 0);
		mv.visitFieldInsn(GETFIELD, "com/sunxd/asm/Person", "name", "Ljava/lang/String;");
		mv.visitInsn(ARETURN);
		mv.visitMaxs(1, 1);
		mv.visitEnd();
	}
}