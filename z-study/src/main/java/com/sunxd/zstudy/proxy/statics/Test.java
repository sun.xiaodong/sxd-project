package com.sunxd.zstudy.proxy.statics;

/**
 * @author: 静态代理 继承、实现 某一个类
 * 自己写一个类，和原始类一样， 继承、实现 某一个类以后，复写原始类，并添加自己的处理逻辑。
 * 且代理类要有其实现、继承 的父类，并通过构造方法初始化进来，然后调用代理类的方法（方法体里面调用原始类的方法）
 * @date: 2021-04-10 15:36
 **/
public class Test {

    public static void main(String[] args) {

        PersonProxy personProxy = new PersonProxy(new Girl());
        personProxy.eat();

    }

}
