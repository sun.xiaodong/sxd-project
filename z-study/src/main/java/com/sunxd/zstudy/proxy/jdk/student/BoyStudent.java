package com.sunxd.zstudy.proxy.jdk.student;

import com.sunxd.zstudy.proxy.jdk.student.Student;

/**
 * @author: 作者名称
 * @date: 2021-04-10 16:00
 **/
public class BoyStudent implements Student {

    @Override
    public String study() {
        System.out.println("原始类执行： BoyStudent  study`");
        return "123";
    }

}
