package com.sunxd.zstudy.proxy.asm.test1;
 
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
 
public class CreateClassAdapter extends ClassVisitor implements Opcodes{
 
	public CreateClassAdapter(ClassVisitor cv) {
		super(Opcodes.ASM4, cv);
	}
 
	@Override
	public void visit(int version, int access, String name, String signature,
            String superName, String[] interfaces) {
        if (cv != null) {
            cv.visit(version, access, name, signature, superName, interfaces);
        }
    }
	
	@Override
    public MethodVisitor visitMethod(int access, String name, String desc,
            String signature, String[] exceptions) {
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
		if(name.equals("<init>")){
			return new CreateInitMethodAdapter(mv);
		}else if(name.equals("getName")){
			return new CreateGetMethodAdapter(mv);
		}else if(name.equals("setName")){
			return new CreateSetMethodAdapter(mv);
		}else{
			return super.visitMethod(access, name, desc, signature, exceptions);
		}
	}
}