package com.sunxd.zstudy.proxy.jdk.student;

/**
 * @author: 作者名称
 * @date: 2021-04-10 15:59
 **/
public interface Student {

    public String study();

}
