package com.sunxd.zstudy.proxy.asm.test1;
 
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
 
public class CreateSetMethodAdapter extends MethodVisitor implements Opcodes{
 
	public CreateSetMethodAdapter(MethodVisitor mv) {
		super(Opcodes.ASM4, mv);
	}
 
	@Override
	public void visitCode(){
		mv.visitVarInsn(ALOAD, 0);
		mv.visitVarInsn(ALOAD, 1);
		mv.visitFieldInsn(PUTFIELD, "com/sunxd/asm/Person", "name", "Ljava/lang/String;");
		mv.visitInsn(RETURN);
		mv.visitMaxs(2, 2);
		mv.visitEnd();
	}
}