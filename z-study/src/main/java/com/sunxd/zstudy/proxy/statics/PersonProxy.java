package com.sunxd.zstudy.proxy.statics;

/**
 * @author: 作者名称
 * @date: 2021-04-10 15:33
 **/
public class PersonProxy implements Person{

    private Person person;

    public PersonProxy(Person person) {
        this.person = person;
    }

    @Override
    public void eat(){
        System.out.println("PersonProxy start ....");
        person.eat();
        System.out.println("PersonProxy end ....");
    }

}
