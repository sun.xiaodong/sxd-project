package com.sunxd.zstudy.proxy.javassist;

/**
 * @author sun.xd
 * @description: description
 * @date 2023/3/8 16:54
 */
public class Test {
    public static void main(String[] args) throws Exception{
        JavassitProxy proxy = new JavassitProxy(new Student("张三"));
        Student student = (Student) proxy.getProxy();
        student.wakeup();
        student.sleep();
    }
}
