package com.sunxd.zstudy.proxy.asm.test2;

public interface ISayHello
{
    public void MethodA();
    
    public void MethodB();
    
    public void Abs();
}