package com.sunxd.zstudy.proxy.statics;

/**
 * @author: 作者名称
 * @date: 2021-04-10 15:30
 **/
public class Girl implements Person {
    @Override
    public void eat() {
        System.out.println("girl is eating");
    }
}
