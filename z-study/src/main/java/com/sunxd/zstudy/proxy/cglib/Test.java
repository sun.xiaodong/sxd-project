package com.sunxd.zstudy.proxy.cglib;

import org.springframework.cglib.core.DebuggingClassWriter;

/**
 * @author: 作者名称
 * @date: 2021-04-10 23:52
 **/
public class Test {

    public static void main(String[] args) {
        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "C:\\ProgramData\\cglib");
        Father1 father = (Father1) new CglibProxy(new Father1()).createProxy();
        father.piao();
//        Father2 father2 = (Father2) new CglibProxy(new Father2()).createProxy();
//        father2.piao();
    }

}
