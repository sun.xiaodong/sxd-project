package com.sunxd.zstudy.proxy.asm.test1;
 
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
 
public class CreateInitMethodAdapter extends MethodVisitor implements Opcodes{
 
	public CreateInitMethodAdapter(MethodVisitor mv) {
		super(Opcodes.ASM4, mv);
	}
 
	@Override
	public void visitCode(){
		mv.visitVarInsn(ALOAD, 0);
		mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V");
		mv.visitVarInsn(ALOAD, 0);
		mv.visitLdcInsn("zhangzhuo");
		mv.visitFieldInsn(PUTFIELD, "com/sunxd/asm/Person", "name", "Ljava/lang/String;");
		mv.visitInsn(RETURN);
		mv.visitMaxs(2, 1);
		mv.visitEnd();
	}
}