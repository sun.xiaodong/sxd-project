package com.sunxd.zstudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZStudyApplication.class, args);
    }

}
