package com.sunxd.zstudy.juc.T19_container.concurrent;

import java.util.*;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.stream.Stream;

/**
 *
 * @author: 作者名称
 * @date: 2021-09-10 11:47
 **/
public class T03_ConcurrentQueue {
    public static void main(String[] args) {
        Queue<String> queue  = new ConcurrentLinkedQueue<>();
        for (int i = 0; i < 10; i++) {
            queue.offer("a"+i);
            // 都是添加。区别，如果有容量限制，add 抛异常，queue 返回false ,add抛异常更加耗时
//            queue.add("a"+i);
        }
        // dayin
        System.out.println(queue);
        // 查询第一个，并不溢出
        System.out.println(queue.peek());
        //取出第一个，并从连表里溢出
        System.out.println(queue.poll());
        System.out.println(queue.poll());


    }
}
