package com.sunxd.zstudy.juc.T19_container.fromHashTableToConcurrentHashMap.addEfficiency;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: 作者名称
 * @date: 2021-09-09 15:51
 **/
public class AddConcurrentHashMap {

    static ConcurrentHashMap<Integer, Integer> hashtable = new ConcurrentHashMap<>();

    static class MyThread extends Thread {

        int start;
        int gap = EfficiencyConstants.COUNT / EfficiencyConstants.THREAD_COUNT;

        public MyThread(int start) {
            this.start = start;
        }

        @Override
        public void run() {
            for (int i = start; i < start + gap; i++) {
                hashtable.put(i, i);
            }
        }

    }

    public static void main(String[] args) throws InterruptedException {
        long start = System.currentTimeMillis();
        Thread[] threads = new Thread[EfficiencyConstants.THREAD_COUNT];
        System.out.println(threads.length);
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new MyThread(i * EfficiencyConstants.COUNT / EfficiencyConstants.THREAD_COUNT);
        }
        for (Thread t : threads){
            t.start();
        }
        for (Thread t : threads){
            t.join();
        }
        System.out.println(System.currentTimeMillis() - start);
        System.out.println(hashtable.size());
    }


}
