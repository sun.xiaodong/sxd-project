package com.sunxd.zstudy.juc.T19_container.specialQueue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * 装进去
 * 作用就是线程交换数据
 * @author: 作者名称
 * @date: 2021-09-13 19:31
 **/
public class T4TransferQueue {

    public static void main(String[] args) throws InterruptedException {
        LinkedTransferQueue<String> q = new LinkedTransferQueue<>();
        new Thread(()->{
            try {

                TimeUnit.SECONDS.sleep(10);
                System.out.println(q.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        System.out.println("aaa");
        q.transfer("111"); // 灵魂所在 ,当take完以后，才会执行下面当代码
        System.out.println("bbb");

//        new Thread(()->{
//            try {
//                System.out.println(q.take());
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }).start();
    }
}
