package com.sunxd.zstudy.juc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author: 作者名称
 * @date: 2021-03-30 00:00
 **/
public class T07_AtomicInteger {

    AtomicInteger atomicInteger = new AtomicInteger(0);

    void m(){
        for (int i = 0; i < 10000; i++) {
            atomicInteger.incrementAndGet();
//            System.out.println(Thread.currentThread().getName());
        }
    }

    public static void main(String[] args) {
        T07_AtomicInteger t07_cas = new T07_AtomicInteger();
        List<Thread> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(new Thread(t07_cas::m,"thread:"+i));
        }
        list.forEach(Thread::start);
        // 必须要加，否则线程没跑完，直接打印后面的输出，导致数据不对
        for (Thread thread : list) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(t07_cas.atomicInteger.get());
    }
}
