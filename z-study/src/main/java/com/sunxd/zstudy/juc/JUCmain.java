package com.sunxd.zstudy.juc;

import sun.misc.Unsafe;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author: 作者名称
 * @date: 2021-09-03 16:08
 **/
public class JUCmain {

    private static final Unsafe unsafe = Unsafe.getUnsafe();

    private volatile static int state;
    private static final long stateOffset;

    static {
        try {
            stateOffset = unsafe.objectFieldOffset
                    (JUCmain.class.getDeclaredField("state"));

        } catch (Exception ex) { throw new Error(ex); }
    }

    public static void main(String[] args) {
        unsafe.compareAndSwapLong(state,stateOffset,0,1);
        System.out.println(state);
    }
}
