package com.sunxd.zstudy.juc.T18_reference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 强软弱虚引用
 * @author: 作者名称
 * @date: 2021-09-08 15:00
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class M {

    private String name;
    private Integer age;

    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize");
        super.finalize();
    }

    public static void main(String[] args) {
        M m = new M();
    }
}
