package com.sunxd.zstudy.juc.T20_ThreadPool.threadPool;

import java.util.concurrent.*;

/**
 * FutureTask 继承runable 和 future 接口
 *  和 之前future区别：  new Thread(futureTask).start(); -》 获取返回值 futureTask.get()
 *
 * @author: 作者名称
 * @date: 2021-09-14 19:08
 **/
public class T3_FutureTask {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        FutureTask<String> futureTask = new FutureTask<>(()->{
            TimeUnit.SECONDS.sleep(1);
            System.out.println(1000);
            return "1000";
        });

        // 第一种写法
//        new Thread(futureTask).start();
//        System.out.println(futureTask.get());
        // 第二种写法
        // 重写父类
//        futureTask.run();
//        System.out.println(futureTask.get());
        // 第三种写法 即使提交2次，也只执行一次
        ExecutorService e = Executors.newCachedThreadPool();
//        e.submit(futureTask);
//        e.submit(futureTask);
//        System.out.println(futureTask.get());
    }
}
