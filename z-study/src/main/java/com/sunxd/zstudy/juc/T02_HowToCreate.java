package com.sunxd.zstudy.juc;

/**
 * @author: 如何创建线程
 * new Thread().start();
 * new Thread(new MyRunable).start();
 * 或者java8的lambda
 * 或者从线程池中启动
 * @date: 2021-03-28 22:35
 **/
public class T02_HowToCreate {

    public static void main(String[] args) {
        new MyThread().start();
        new Thread(new MyRunnable()).start();
        new Thread(() -> {
            System.out.println("start lambda ");
        }).start();
    }

    public static class MyThread extends Thread{
        @Override
        public void run() {
            System.out.println("start extends Thread");
        }
    }

    public static class MyRunnable implements Runnable{
        @Override
        public void run() {
            System.out.println("start implements Runnable");
        }
    }


}
