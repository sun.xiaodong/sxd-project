package com.sunxd.zstudy.juc.T18_reference;

import java.io.IOException;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * 强饮用
 * 当引用指向对象时，gc永远不会回收该对象。当m=null时，gc会回收
 * @author: 作者名称
 * @date: 2021-09-08 15:00
 **/
public class T18_1NormalReference {

    private static   volatile M m1;

    public static void main(String[] args) throws IOException {
        setM1();
        M m2 = m1;
        m2.setAge(123);
        System.out.println(m1.getAge());
    }

    public static void setM1(){
        m1 = new M();
        m1.setName("m1");
    }

}
