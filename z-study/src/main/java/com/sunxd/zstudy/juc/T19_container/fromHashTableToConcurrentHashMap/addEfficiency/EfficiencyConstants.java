package com.sunxd.zstudy.juc.T19_container.fromHashTableToConcurrentHashMap.addEfficiency;

/**
 * @author: 作者名称
 * @date: 2021-09-09 15:53
 **/
public class EfficiencyConstants {
    public static final int COUNT  = 1000000;
    public static final int THREAD_COUNT  = 100;
}
