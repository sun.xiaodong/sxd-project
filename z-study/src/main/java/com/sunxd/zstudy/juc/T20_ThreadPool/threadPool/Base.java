package com.sunxd.zstudy.juc.T20_ThreadPool.threadPool;

/**
 *
 * Runnable(void)  无返回值
 * Callable(V)    有返回值
 *
 * Future -> 存储执行的将来产生的结果
 * FutureTask -> Future + Runnable
 * CompletableFuture -> 管理多个future结果
 *
 * 线程池
 *
 * Executor
 * ExecutorService      |
 * ThreadPoolExecutor   |
 *
 * ThreadPoolExecutor
 * ForkJoinPoll (分解汇总的任务，用很少的线程执行很多的任务，TPE（ThreadPoolExecutor） 做不到先执行子任务，CPU密集型)
 *
 *
 * @author: 作者名称
 * @date: 2021-09-14 10:50
 **/
public class Base {
    public static void main(String[] args) {
//        Callable
        int a = 1 << 1;
        System.out.println(a);
        a = 1 << 2;
        System.out.println(a);
        a = 1 << 3;
        System.out.println(a);
        a = 2 << 1;
        System.out.println(a);
        a = 2 << 2;
        System.out.println(a);
    }
}
