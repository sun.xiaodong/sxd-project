package com.sunxd.zstudy.juc;

/**
 * @author: 线程集中状态
 *  new ->      ready ->     running -> terminated (结束 不能回转)
 *      (sleep,waiting)  TimedWaiting
 *      (sleep,waiting)  Waiting   o.notify o.notifyAll
 *       同步代码快  Blocked
 *
 * @date: 2021-03-28 23:21
 **/
public class T04_ThreadState {

    public static void main(String[] args) {
        Thread t1 = new Thread( () -> {
            System.out.println("1");
        });

        System.out.println(t1.getState());

        t1.start();

        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(t1.getState());
    }
}
