package com.sunxd.zstudy.juc.T18_reference;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.concurrent.TimeUnit;

/**
 * 软引用
 *  m 指向 new SoftReference 这个对象的 new byte[1024 * 1024 * 10] 这个数组
 * 当堆内存不够用当时候，就会把他清理掉
 * 用途：缓存
 *
 * @author: 作者名称
 * @date: 2021-09-08 15:00
 **/
public class T18_2SoftReference {

    // 执行前，先把堆内存改为20M
    // vm operation  -Xms20m -Xmx20m
    public static void main(String[] args) throws IOException, InterruptedException {
        //占用10M内存
        SoftReference<byte[]> m = new SoftReference<>(new byte[1024 * 1024 * 10]);
        System.out.println(m.get());
        System.gc();
        TimeUnit.SECONDS.sleep(3);
        System.out.println(m.get());
        byte[] b = new byte[1024 * 1024 * 15];
        TimeUnit.SECONDS.sleep(3);
        System.out.println(m.get());
    }

}
