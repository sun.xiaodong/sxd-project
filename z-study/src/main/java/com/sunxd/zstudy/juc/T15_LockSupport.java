package com.sunxd.zstudy.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 控制线程 停止、继续
 * @author: 作者名称
 * @date: 2021-09-02 15:34
 **/
public class T15_LockSupport {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() ->{
            for (int i = 0; i < 10; i++) {
                System.out.println(i);
                if(i ==3){
                    // 当它=3的时候，让线程阻塞
                    LockSupport.park();
                }
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        // 必须加，如果不加， thread.start()执行完后就直接执行LockSupport.unpark ，即通知线程不阻塞，导致不出现阻塞的情况
        TimeUnit.SECONDS.sleep(5);
        // 结束线程的阻塞状态，继续执行
        LockSupport.unpark(thread);
    }
}
