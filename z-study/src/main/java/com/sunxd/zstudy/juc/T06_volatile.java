package com.sunxd.zstudy.juc;

/**
 * @author: 作者名称
 * 保证线程可见行 mesi 缓存一致性
 * 禁止指令重排序 dcl double check lock
 * @date: 2021-03-29 23:23
 **/
public class T06_volatile {

//    Boolean flag =true;
   volatile Boolean flag =true;

   void m(){
       System.out.println("start");
       while (flag){
       }
       System.out.println("end");
   }


    public static void main(String[] args) {
        T06_volatile t06_volatile = new T06_volatile();
        new Thread(t06_volatile::m,"thread-1").start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t06_volatile.flag = false;

    }

}
