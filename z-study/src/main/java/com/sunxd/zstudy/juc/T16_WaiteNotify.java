package com.sunxd.zstudy.juc;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * 使用wait notify 实现2个线程工作的切换
 * 如果设计多线程，wait notify都会成对出现，如果只用wait（**并不会释放当前所），下一个线程不会马上执行，他要等当前线程执行完毕。
 *
 * 为什么说
 *
 * @author: 作者名称
 * @date: 2021-09-02 19:32
 **/
public class T16_WaiteNotify {
    volatile String id = "1";
   static volatile List list = new ArrayList();

    public static void main(String[] args) {


        Object o = new Object();
        new Thread(() ->{
            System.out.println("T2 启动");
            synchronized (o){
                if(list.size() != 5){
                    try {
                        System.out.println("t2 wait");
                        o.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("T2 end");
                // 如果不加，则t1 会一直卡住
                o.notify();
            }
        },"T2").start();

        new Thread(() ->{
            System.out.println("T1 启动");
            synchronized (o){
                for (int i = 0; i < 10; i++) {
                    list.add(new Object());
                    if(i == 5){
                        o.notify();
                        try {
                            o.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(i);

                }
                System.out.println("T1 end");
            }
        },"T1").start();
    }
}
