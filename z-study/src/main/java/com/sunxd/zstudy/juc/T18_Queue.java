package com.sunxd.zstudy.juc;

/**
 * queue 常用方法解释
 *
 *  add、offer、put这3个方法都是往队列尾部添加元素，区别如下：
 *  add：不会阻塞，添加成功时返回true，不响应中断，当队列已满导致添加失败时抛出IllegalStateException。
 *  offer：不会阻塞，添加成功时返回true，因队列已满导致添加失败时返回false，不响应中断。
 *  put：会阻塞会响应中断。
 *
 *  take、poll方法能获取队列头部第1个元素，区别如下：
 *  take：会响应中断，会一直阻塞直到取得元素或当前线程中断。
 *  poll：会响应中断，会阻塞，阻塞时间参照方法里参数timeout.timeUnit，当阻塞时间到了还没取得元素会返回null
 **/
public class T18_Queue {

}
