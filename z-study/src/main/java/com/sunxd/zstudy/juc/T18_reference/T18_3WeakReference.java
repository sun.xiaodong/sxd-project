package com.sunxd.zstudy.juc.T18_reference;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

/**
 * 弱引用-还能get到
 *  只要是垃圾回收，就会直接回收
 *
 * 一般用在容器里
 *  用途：一般有一个强引用指向他，如果强引用消失了，他就应该消失
 * @author: 作者名称
 * @date: 2021-09-08 15:00
 **/
public class T18_3WeakReference {

    public static void main(String[] args) throws IOException, InterruptedException {
        WeakReference<M> m = new WeakReference<>(new M());
        System.out.println(m.get());
        System.gc();
        System.out.println(m.get());


    }

}
