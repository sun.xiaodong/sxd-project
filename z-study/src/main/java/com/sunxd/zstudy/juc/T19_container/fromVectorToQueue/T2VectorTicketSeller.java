package com.sunxd.zstudy.juc.T19_container.fromVectorToQueue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

/**
 * 无界的队列
 *
 * @author: 作者名称
 * @date: 2021-09-09 17:20
 **/
public class T2VectorTicketSeller {

    static Vector<String> list = new Vector<>();

    static {
        for (int i = 0; i <10000; i++) {
            list.add("票号 "+i);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                while(list.size()>0){
                    try {
                        // 即使枷锁了，还是出现问题。中间睡了10ms，中间依然会出现超卖
                        TimeUnit.MICROSECONDS.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("销售了 ："+list.remove(0));
                }
            }).start();
        }
        TimeUnit.SECONDS.sleep(5);
        System.out.println(list.size());
    }



}
