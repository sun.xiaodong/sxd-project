package com.sunxd.zstudy.juc.T19_container.specialQueue;

import java.util.PriorityQueue;

/**
 * 排序
 * @author: 作者名称
 * @date: 2021-09-13 19:23
 **/
public class T2PriorityQueue {

    public static void main(String[] args) {
        PriorityQueue<String> queue = new PriorityQueue<>();
        queue.add("b");
        queue.add("f");
        queue.add("d");
        queue.add("a");
        queue.add("g");
        while (queue.size() > 0){
            System.out.println(queue.poll());
        }
    }
}
