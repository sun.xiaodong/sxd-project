package com.sunxd.zstudy.juc;

/**
 * @author: 什么是线程
 *
 * 应用程序-双击启动
 * 进程分配资源的基本单位
 * 程序执行的最小单位是线程
 *
 * @date: 2021-03-28 22:23
 **/
public class T01_WhatIsThread {

    private static class T1 extends  Thread{
        @Override
        public void run() {
            for (int i =0 ;i< 10;i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("T1");
            }
        }

    }

    public static void main(String[] args) {
        new T1().run(); // 当作一个普通方法执行，并没有启动多线程
        new T1().start();
        for (int i =0;i<10;i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("main");
        }
    }
}
