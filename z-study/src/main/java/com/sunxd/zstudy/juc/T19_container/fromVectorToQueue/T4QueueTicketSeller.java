package com.sunxd.zstudy.juc.T19_container.fromVectorToQueue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * @author: 作者名称
 * @date: 2021-09-09 17:20
 **/
public class T4QueueTicketSeller {

    static Queue<String> list = new ConcurrentLinkedDeque<>();

    static {
        for (int i = 0; i <10000; i++) {
            list.add("票号 "+i);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                while(true){
                    String s = list.poll();
                    if (s == null) {
                        break;
                    } else {
                        System.out.println("销售了 ："+s);
                    }
                }
            }).start();
        }
        TimeUnit.SECONDS.sleep(5);
        System.out.println(list.size());
    }



}
