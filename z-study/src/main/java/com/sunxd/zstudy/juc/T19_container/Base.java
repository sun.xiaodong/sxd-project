package com.sunxd.zstudy.juc.T19_container;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * 容器读写效率
 * 容器从物理结构来分：只有连续和不连续。 数组，链表
 * 容器分为2大类 ： Collection 、Map
 * Collection 主要有 List , Set , Queue  为什么还要有Queue ,主要为高并发使用,增加并发使用的api（put、take 阻塞队列）
 *  eg:售票，高并发的时候多考虑queue, 不重复的时候考虑set,set也有线程安全的set
 *
 *
 * Map: 一般都是写少，读多。
 * Hashtable,HashMap,ConcurrentHashMap（高并发使用）,Colletions.synchronizedMap  除了HashMap不是线程安全的
 * put 进入来的时候，效率相差不大
 * read 的时候 ConcurrentHashMap 效率极高
 *
 * @author: 作者名称
 * @date: 2021-09-09 11:50
 * 
 * 
 * 容器：
 * -- Collection
 * ---- List
 * -------- CopyOnWriteList
 * -------- Vector ----Stack
 * -------- ArrayList
 * -------- LinkedList
 * ---- Set
 * -------- HashSet --LinkedHashSet
 * -------- SortedSet --TreeSet
 * -------- CopyOnWriteArraySet
 * -------- ConcurrentSkipListSet
 * ---- Queue
 * -------- Deque
 * ------------ ArrayDeque
 * ------------ BlockingDeque -- LinkedBlockingDeque
 * -------- BlockingQueue
 * ------------ ArrayBlockingDeque
 * ------------ PriorityBlockingDeque
 * ------------ LinkedBlockingDeque
 * ------------ TransferBlockingDeque --LinkedTransferBlockingDeque
 * ------------ SynchronousDeque
 * -------- PriorityQueue
 * -------- ConcurrentLinkedQueue
 * -------- DelayQueue
 * 
 * -- Map 
 * ---- HashMap
 * ---- TreeMap
 * ---- WeakHashMap
 * ---- IdentityHashMap
 * 
 **/
public class Base {

    public static void main(String[] args) {
        Queue<String> queue = new ArrayDeque<>();

    }
}
