package com.sunxd.zstudy.juc.T19_container.fromHashTableToConcurrentHashMap.addEfficiency;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: 作者名称
 * @date: 2021-09-09 15:51
 **/
public class AddSynchronizedtHashMap {

    static Map<Object, Object> map = Collections.synchronizedMap(new HashMap<>());

    static class MyThread extends Thread {

        int start;
        int gap = EfficiencyConstants.COUNT / EfficiencyConstants.THREAD_COUNT;

        public MyThread(int start) {
            this.start = start;
        }

        @Override
        public void run() {
            for (int i = start; i < start + gap; i++) {
                map.put(i, i);
            }
        }

    }

    public static void main(String[] args) throws InterruptedException {
        long start = System.currentTimeMillis();
        Thread[] threads = new Thread[EfficiencyConstants.THREAD_COUNT];
        System.out.println(threads.length);
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new MyThread(i * EfficiencyConstants.COUNT / EfficiencyConstants.THREAD_COUNT);
        }
        for (Thread t : threads){
            t.start();
        }
        for (Thread t : threads){
            t.join();
        }
        System.out.println(System.currentTimeMillis() - start);
        System.out.println(map.size());
    }


}
