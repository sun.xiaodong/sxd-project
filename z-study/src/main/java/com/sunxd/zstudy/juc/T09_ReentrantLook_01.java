package com.sunxd.zstudy.juc;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author: 可重入锁
 * 定义： 同一个线程里，一个synchronize方法调用另外一个synchronize方法
 * @date: 2021-03-30 23:15
 **/
public class T09_ReentrantLook_01 {

    public static void main(String[] args) {
        // synchronized 在方法上，锁的是 a 这个对象
//        T09_ReentrantLook_01 a = new T09_ReentrantLook_01();
//        Thread thread = new Thread(a::test1);
//        thread.start();
        ReentrantLock reentrantLook = new ReentrantLock();
        reentrantLook.lock();
        try {
            System.out.println(1);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            reentrantLook.unlock();
        }
    }

    synchronized void test1(){
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
                System.out.println("test1");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(i == 3){
                test2();
            }

        }
    }
    synchronized void test2(){
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
                System.out.println("test2");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }




}
