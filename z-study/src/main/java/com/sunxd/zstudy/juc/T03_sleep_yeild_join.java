package com.sunxd.zstudy.juc;


/**
 * @author: 作者名称
 *
 * sleep :强制睡眠
 * yield ：暂时让出执行，重新回到继续状态，不能保证下一次执行的不是本地线程
 * join: 执行到着到时候，让其他线程执行完在接着本次线程继续执行。 案例保证 t1->t2->t3 这个顺序执行
 *
 * @date: 2021-03-28 22:52
 **/
public class T03_sleep_yeild_join {

    private static volatile  int a = 0;

    public static void main(String[] args) {
//        testSleep();
//        testYield();
        testJoin();
    }

    static void testSleep(){
        new  Thread(() ->{
            try {
                Thread.sleep(500);
                System.out.println("sleep");
//                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    static void testYield(){
        new  Thread(() ->{
            Thread.yield();
            System.out.println("T1");
        }).start();
        new  Thread(() ->{
            System.out.println("T2");
        }).start();
    }

    static void testJoin(){
        Thread t1 = new  Thread(() ->{
                System.out.println("T1 ");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        });
        Thread t2 = new  Thread(() ->{
            try {
                System.out.println("T2 start");
                t1.join();
//                try {
////                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                System.out.println("T2 end");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        });
        Thread t3 = new  Thread(() ->{
            try {
                System.out.println("T3 start ");
                t2.join();
                System.out.println("T3 end");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        });
        t1.start();
        t2.start();
        t3.start();
    }



}
