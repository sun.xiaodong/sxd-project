package com.sunxd.zstudy.juc;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author: 作者名称
 * @date: 2021-09-06 16:15
 **/
public class Aqs {
    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        lock.lock();
        lock.unlock();
    }
}
