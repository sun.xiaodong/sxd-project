package com.sunxd.zstudy.juc.T19_container.specialQueue;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class MyTask1 implements Delayed,Runnable {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getRunningTime() {
        return runningTime;
    }

    public void setRunningTime(long runningTime) {
        this.runningTime = runningTime;
    }

    String name;
    long runningTime;

    public MyTask1(String name, long rt) {
        this.name = name;
        this.runningTime = rt;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return unit.convert(runningTime - System.currentTimeMillis(), TimeUnit.MICROSECONDS);
    }

    @Override
    public int compareTo(Delayed o) {
        System.out.println("self"+this.getDelay(TimeUnit.MICROSECONDS));
        System.out.println("other"+o.getDelay(TimeUnit.MICROSECONDS));
        if (this.getDelay(TimeUnit.MICROSECONDS) == o.getDelay(TimeUnit.MICROSECONDS)) {
            return 0;
        } else {
            return this.getDelay(TimeUnit.MICROSECONDS) > o.getDelay(TimeUnit.MICROSECONDS) ? 1 : -1;
        }
    }

    @Override
    public void run() {

    }
}
