package com.sunxd.zstudy.juc.T19_container.specialQueue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * SynchronousQueue 是一个容量为0 的容器，不能往里面装东西，只能先take后，才能使用put。 (add,epoll,offer,都获取不到数据，)
 * 作用就是线程交换数据
 * 和Transfer区别
 * Synchronous： 先去获取，才能put
 * Transfer: 先装到里面，等人拿走后，才能继续执行
 * @author: 作者名称
 * @date: 2021-09-13 19:31
 **/
public class T3SynchronousQueue {

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<String>  q= new SynchronousQueue<>();
        new Thread(() -> {
            try {
                System.out.println(q.take());
//                System.out.println(q.poll());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        q.put("add");
        System.out.println(q.size());
//        q.add("add"); 报错
        System.out.println(q.size());
    }
}
