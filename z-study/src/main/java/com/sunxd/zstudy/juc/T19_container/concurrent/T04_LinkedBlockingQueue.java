package com.sunxd.zstudy.juc.T19_container.concurrent;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Stream;

/**
 * 无界队列
 * Blocking 阻塞体现在queue. put、take  源码注视 会一直阻塞
 *
 * @author: 作者名称
 * @date: 2021-09-10 11:47
 **/
public class T04_LinkedBlockingQueue {
    public static void main(String[] args) {
        BlockingQueue<String> queue  = new LinkedBlockingQueue<>();

        new Thread(() -> {
            for (int i = 0; i < 50; i++) {
                queue.add("a"+i);
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }).start();
        for (int i = 0; i <10 ; i++) {
            new Thread( () -> {
                while(true){
                    try {
                        System.out.println(Thread.currentThread().getName()+ " 获取："+queue.take());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            },"thread"+i).start();

        }

    }
}
