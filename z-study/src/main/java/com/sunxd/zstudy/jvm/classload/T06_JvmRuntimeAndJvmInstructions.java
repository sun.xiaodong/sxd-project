package com.sunxd.zstudy.jvm.classload;

/**
 * java 运行时数据区和常用命令
 *  PC (Program Counter) 存放下一条指令位置，虚拟机的运行类似于这样的循环
 *  JVM stacks （每个线程都自己独有都一个栈，装都是栈针，每个方法都独有一个栈针，即下面4种，都有各自的）
 *      -- local variables 局部变量
 *      -- Operand stacks 操作数栈     bipush压栈 istore出战 iload加载
 *      -- dynamic linking 指到常量池的符号链接
 *      -- return address 方法返回值
 *  method area （方法区，线程共享） -》>1.8 metaspace 启动已物理内存大小 具体实现 <=1.7 persubm  启动指定大小，不能更改，之前出现内存溢出
 *  native method stacks （C C++ 方法栈）
 *  direct memory （直接内存 NIO),用户可以直接访问内核空间内存
 *  run time constant pool （常量池）
 *  heap (堆 ，堆在线程之间共享)
 *
 *  常用命令
 *  ipush 出战
 *  store 出战
 *  load 压栈
 *  mul
 *  sub
 *  invoke
 *  invokeStatic invokeVirtual invokeInterface invokeSpecial invokeDynamic
 * @author: sun.xd
 * @date: 2021-10-20 16:59
 **/
public class T06_JvmRuntimeAndJvmInstructions {

    public static void main(String[] args) {
        new T06_JvmRuntimeAndJvmInstructions();
    }


}

