package com.sunxd.zstudy.jvm.classload;

/**
 * 类加载过程
 *  -- Loading 加载到内存 （双亲委派，处于安全考虑 【覆盖String，自己写个类库，每次输入密码时，发封邮件给自己，可以拿到密码】）
 *  -- Linking
 *  ---- verification 校验
 *  ---- preparation  准备，变量赋默认值，eg： int a= 0;
 *  ---- resolution   将变量转换为可直接使用到的内存地址
 *  -- Initializing   将 int 值赋为定义的值（给静态成员赋值）
 *
 * 对象创建过程
 *  -- Loading 加载到内存 （双亲委派，处于安全考虑 【覆盖String，自己写个类库，每次输入密码时，发封邮件给自己，可以拿到密码】）
 *  -- Linking
 *  ---- verification 校验
 *  ---- preparation  准备，变量赋默认值，eg： int a= 0;
 *  ---- resolution   将变量转换为可直接使用到的内存地址
 *  -- Initializing   将 int 值赋为定义的值（给静态成员赋值）
 *  ---- 申请内存
 *  ---- 成员变量赋默认值
 *  ---- 调用构造方法《init》
 *  -------- 成员变量顺序付初始值
 *  -------- 执行构造方法
 *
 * 对象占用内存布局：
 *  普通对象
 *      1. 对象头： markword 8字节
 *      2. ClassPoint指针 -XX:+UseCompressedClassPointers为4字节，不开始为8字节
 *      3. 实例数据：
 *      4. padding对齐，8的倍数
 *  数组对象
 *      1. 对象头： markword 8字节
 *      2. ClassPoint指针 -XX:+UseCompressedClassPointers为4字节，不开始为8字节
 *      3. 数组长度：4字节
 *      4. 实例数据：
 *      5. padding对齐，8的倍数
 *
 **/
public class A {
}
