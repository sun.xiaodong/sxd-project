package com.sunxd.zstudy.jvm.classload;

import sun.net.spi.nameservice.dns.DNSNameService;

/**
 * 类加载器的加载内容 -》 按需动态加载内容 双亲委派模式  父加载器 并不是继承，只是子加载器有上层加载器的引用
 *  Bootstrap     --> 加载lib/rt.jar ,charset.jar 等核心类，c++实现
 *  Extension     --> 加载jre/lib/ext/*.jar 或由-Djava.ext.dirs指定
 *  App           --> 加载classpath指定的内容
 *  Custom Classloader  --> 自定义classloader
 *
 *
 * @author: sun.xd
 * @date: 2021-10-20 11:17
 **/
public class T01_ClassLoaderLevel {

    public static void main(String[] args) {
        System.out.println(String.class.getClassLoader());
        System.out.println(DNSNameService.class.getClassLoader());
        System.out.println(T01_ClassLoaderLevel.class.getClassLoader());
    }

}
