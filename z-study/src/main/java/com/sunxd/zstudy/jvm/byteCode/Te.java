package com.sunxd.zstudy.jvm.byteCode;

/**
 * 查看类的加载信息 -》 打开class文件使用sublime text 查看，或者使用 view -> show bytecode with jclasslib
 * @author: 作者名称
 * @date: 2021-10-12 10:28
 **/
public class Te {
    int i;
    volatile int j;

    synchronized void m(){

    }

    void n(){
        synchronized (this){

        }
    }

    public static void main(String[] args) {

    }
}
