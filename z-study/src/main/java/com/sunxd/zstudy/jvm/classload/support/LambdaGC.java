package com.sunxd.zstudy.jvm.classload.support;

/**
 * @author: sun.xd
 * @date: 2021-10-26 09:55
 **/
public class LambdaGC {

    public static void main(String[] args) {
        I i = C::n;
    }

    public static interface I {
        void m();
    }

    class Abc implements I {

        public void m() {
            System.out.println("m");
        }
    }

    static class C {
        static void n(){
            System.out.println("n");
        }
    }
}
