package com.sunxd.zstudy.jvm.classload;

/**
 * 打开Launcher源码，查看对应的加载路径
 *
 * @author: sun.xd
 * @date: 2021-10-20 11:17
 **/
public class T02_Launcher {

    public static void main(String[] args) {
        String pathBoot = System.getProperty("sun.boot.class.path");
        System.out.println(pathBoot.replaceAll(":",System.lineSeparator()));
        System.out.println("--------------------------------------------------------------");
        String pathExt = System.getProperty("java.ext.dirs");
        System.out.println(pathExt.replaceAll(":", System.lineSeparator()));
        System.out.println("--------------------------------------------------------------");
        String pathApp = System.getProperty("java.class.path");
        System.out.println(pathApp.replaceAll(":", System.lineSeparator()));
        System.out.println("--------------------------------------------------------------");

    }

}
