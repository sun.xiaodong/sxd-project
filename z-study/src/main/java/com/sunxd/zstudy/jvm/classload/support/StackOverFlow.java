package com.sunxd.zstudy.jvm.classload.support;

/**
 *
 * 栈溢出
 * -Xss 设置栈的大小
 *
 * @author: sun.xd
 * @date: 2021-10-26 09:51
 **/
public class StackOverFlow {

    public static void main(String[] args) {
        m();
    }

    static void m(){
        m();
    }
}
