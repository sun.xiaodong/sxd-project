package com.sunxd.zstudy.jvm.classload;

/**
 *
 * 自定义加载器-》 继承  ClassLoader 实现 findClass方法 调用load方法
 *
 * @author: sun.xd
 * @date: 2021-10-20 11:17
 **/
public class T03_CustomClassLoader  {

    public static void main(String[] args) throws ClassNotFoundException {

        T03_MyClassLoader myClassLoader = new T03_MyClassLoader("C:/Users/admin/IdeaProjects/sxd-project/z-study/target/classes");
        Class<?> aClass = myClassLoader.loadClass("com.sunxd.zstudy.controller.ChainOfResponsibility");
        System.out.println(aClass.getClassLoader());
//        aClass.newInstance();


    }




}
