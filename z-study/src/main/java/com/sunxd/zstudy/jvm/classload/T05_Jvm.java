package com.sunxd.zstudy.jvm.classload;

/**
 * java 内存模型
 *
 * L0 寄存器
 * L1 、L2 cpu 独有内存
 * L3 cpu共享内存
 * main cache 主存
 *
 *  缓存一致性的硬件层实现： intel MESI 缓存一致性 缓存的四种状态的处理 一般是缓存锁+总线锁
 *
 *  缓存行 ： cache line 一般读取64个字节
 *  伪共享： 位于同一缓存行的2个不同数据，被不同cpu锁定，产生互相影响的伪共享问题
 *
 *  乱序问题： CPU 为了提高指令执行效率，会在一条指令执行过程中（比如去内存读取数据(慢100倍)），可以执行其他的命令，前提是2指令之间没有依赖关系
 *  乱序解决：
 *      CPU内存屏障：intel  sfence(写),lfence（读）, mfence(读写) 所有的读写操作完成后才会执行其他的操作.lock也可以实现
 *      JVM 实现： JSR133（loadsstore,storestore,loadstore,storeload ）读写前后都加了屏障。lock也可以实现
 *  volatitle 实现：                        CPU
 *      字节码:    acc_flag :0x0040 [volatile] ----- 查看字节码就可以知道
 *      JVM:      StoreStoreBarrier -> volatile写操作 -> StoreLoadBarrier    LoadLoadBarrier -> volatile读操作 -> LoadStoreBarrier
 *      硬件层面：  windows使用Lock指令
 *  synchronized 实现：
 *      字节码： 0x0020 [synchronized] monitorenter,monitorexit,monitorexit 2条monitorexit，一个是正常推出另外一个是异常时退出
 *      JVM：C C++使用操作系统的锁方法
 *      硬件层面  ： Lock
 *
 *
 * @author: sun.xd 
 * @date: 2021-10-20 16:59
 **/
public class T05_Jvm {

    public static void main(String[] args) {
        new T05_Jvm();
    }


}

