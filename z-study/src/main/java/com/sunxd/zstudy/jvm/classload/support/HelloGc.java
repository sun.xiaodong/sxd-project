package com.sunxd.zstudy.jvm.classload.support;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author: sun.xd
 * @date: 2021-10-26 15:53
 **/
public class HelloGc {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("HelloGc");
        List<byte[]> list = Lists.newArrayList();
        for (;;){
            byte[] b = new byte[1024*1024];
            list.add(b);
            TimeUnit.MICROSECONDS.sleep(100);
        }
    }
}
