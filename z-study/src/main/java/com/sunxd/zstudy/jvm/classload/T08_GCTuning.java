package com.sunxd.zstudy.jvm.classload;

/**
 * 优化JVM运行问题
 *
 *  常用的jvmgc日志设置
 *  -Xloggc:/opt/xxx/logs/xxxx-gc-%t.log -XX:+UseGCLogFileRotation
 *  -XX:NumberOfGCLogFiles=5-XX:GCLogFileSize=20M -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCCause
 *
 *  CPU 标高
 *      一定有线程占用系统资源。
 *      找出哪个进程cpu高（top）、该进程中的哪个线程cpu高 top -Hp、导出该线程的堆栈(jstack)、查找哪个发方法（栈帧）(jstack)
 *  内存飙高
 *      导出堆内存（jmap）、分析（jhat jvisuallvm mat jprofiler）
 *自带的：
 *  jps: 只查找Java的线程
 *  top: 观察top  pid（进程号）
 *  top -Hp (pid) ：查看进程里里面的线程使用情况 此时pid是具体的线程号
 *  jstack pid  : 具体定位线程里的信息
 *      重点关注WATING BLOCKED
 *      waiting on<0x000000088ca331o>(a java.lang.Object)
 *      加入有一个进程中100个线程，有很多线程都在waiting on<>,一定要找到是哪个线程持有这把锁
 *      怎么找？搜索jstack dump 信息，找<xx>,看哪个线程是runnable
 *  jinfo 查看进程里的信息
 *  jstat -gc pid time : eg: jstat -gc 1 打印进程为1的一次堆栈信息，jstat -gc 1 1000 一秒打印一次
 *  jmap -histo 1 | head -20  只打印前20个，内存中创建对象最多的。
 *使用远程操控：
 *  启动的时候，打开远程监控（JMS）在启动
 *  使用jdk 自带 jconsole 链接，使用jvisualVM
 *使用arthas
 *  heapdump 下载dump文件
 *  jhat 可以分析。 jhat -J-mx512M
 *
 *
 * 指定垃圾回收器：
 *      -XX:+UseSerialGC = Serial New (DefNew) + Serial Old
 *      -XX:+UseParalleIGC = Parallel Scavenge + Parallel Old (1 .8#tiÀ) [PS + SerialOld]
 *      -XX:+UseConc(urrent)MarkSweepGC = ParNew + CMS + Serial Old
 *      -XX:+UseParallelOldGC = Parallel Scavenge + Parallel Old
 *      -XX:+UseG1GC= G1
 *
 *GC 常用参数
 *      -Xms20M 最小堆
 *      -Xmx20M 最大堆
 *      -Xmn 年轻代
 *      -Xss 栈空间
 *      -XX:+UseParallelGc
 *      -XX:+HeapDumpOnOutOfMemoryError
 *      -XX:+PrintGC
 *
 *
 * -XX:+PrintFlagsFinal -XX:+PrintFlagsInitial  eg java -XX:+PrintFlagsFinal | grep CMS 查询CMS的相关参数
 * -XX:+UseConcMarkSweepGC
 * -XX:+PrintCommandLineFlags
 * -XX:+PrintGC 打印GC
 * -XX:+PrintGCDetails 打印GC明细
 * -XX:+PrintGCTimeStamps  打印GC时间戳
 * -XX:+PrintGCApplicationConcurrentTime  打印GC应用程序时间
 * -XX : + PrintGCApplicationStoppedTime 打印GC暂停时间
 * -Xms20M -Xmx20M
 *
 * CMS常用参数
 *      -XX:+ UseConcMarkSweepGc
 *      -XX:ParallelCMSThreads Cms线程数量
 *      -XX:CMSInitiatingOccupancyFraction   使用多少老年代开始CMS收集，默认是68%（近似值）
 *      -XX:+UseCMsCompactAtFullCollection   在FULLGc之后进行压缩
 *      -XX:CMSFullGCsBeforeCompaction  多少次FULLGc之后进行压缩
 *      -XX:+CMSClassUnloadingEnabled
 *      -XX:CMSInitiatingPermOccupancyFraction 达到什么比例进行Perm回收
 *      -XX:G1NewSizePercent 新生代最小比例，默认5%
 *      -XX:G1MaxNewSizePercent 新生代最大比例，默认5%
 *G1常用参数
 *      -XX:+UseG1GC
 *      -XX:MaxGCPauseMillis
 *      -XX:CMSInitiatingPermOccupancyFraction 达到什么比例进行Perm回收
 * @author: sun.xd
 * @date: 2021-10-22 10:00
 **/
public class T08_GCTuning {




}
