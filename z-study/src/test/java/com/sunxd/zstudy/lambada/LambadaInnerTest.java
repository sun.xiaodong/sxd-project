package com.sunxd.zstudy.lambada;

import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.function.Consumer;

/**
 * lambada 4大内置接口
 *
 * Consumer：消费型接口，内有抽象方法—void accept(T t)
 * Supplier：生产型接口（供给型），内有抽象方法—T get();
 * Function<T, R>：函数型接口，内有抽象方法—R apply(T t)
 * Predicate：断言型接口，内有抽象方法—boolean test(T t)
 *
 * @author sun.xd
 * @description: description
 * @date 2022/2/19 15:12
 */
public class LambadaInnerTest {

    @Test
    public void test1(){
        Consumer<String> consumer = (x) -> System.out.println(x);
        consumer.accept("abc");
    }


}
