package com.sunxd.zstudy.lambada;

import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.function.Consumer;

/**
 * lambada 表达式分为2部分，左侧-右侧
 *  左侧为参数列表
 *  右侧为方法体
 *  语法格式：
 *  1. 无参，一个参数，多个参数
 *  2. 无返回值，有返回值
 *
 *  自定义接口
 *
 * @author sun.xd
 * @description: description
 * @date 2022/2/19 15:12
 */
public class LambadaBaseTest {

    /**
     * 无参 无返回值
     */
    @Test
    public void t1(){
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("hello world");
            }
        };
        Runnable r2 = () -> System.out.println("hello lambada");
        r1.run();
        r2.run();
        Thread thread = new Thread(() -> System.out.println("hello 2"));
        thread.start();
    }

    /**
     * 1参 无返回值
     */
    @Test
    public void t2(){
        Consumer<String> con = (x) -> System.out.println(x);
        con.accept("111");
    }

    /**
     * 多个参数 有返回值
     */
    @Test
    public void t3(){
        Comparator<Integer> c = (c1,c2) -> {
            return Integer.compare(c1,c2);
        };
        int compare = c.compare(2, 23);
        System.out.println(compare);
        // 简写
        Comparator<Integer> c0 = Integer::compare;
        int compare2 = c0.compare(23, 23);
        System.out.println(compare2);
    }


    /**
     * 自定义接口实现lambada
     */
    @Test
    public void t4(){
        String s = preHandle("abc",(str) -> str.toUpperCase());
        System.out.println(s);
    }


    /**
     * 自定义接口实现lambada
     */
    @Test
    public void t5(){
        String s = preHandle("abc",(str) -> str.toUpperCase());
        System.out.println(s);
    }

    public String preHandle(String str, MyFunction1 m1){
        return m1.getValue(str);
    }

    interface MyFunction1{
        String getValue(String str);
    }


    @Test
    public void t6(){
        String s = preHandle2(1L,2L, (x,y) -> x*y*y+"");
        System.out.println(s);
    }

    public String preHandle2(Long t1,Long t2, MyFunction2<Long,String> m1){
        return  m1.getValue(t1,t2);
    }

    interface MyFunction2<T,R>{
        R getValue(T t1, T t2);
    }


}
