package com.sunxd.zstudy.test.event;

import com.sunxd.zstudy.designPattern.support.event.TaskEvent;
import com.sunxd.zstudy.designPattern.support.event.TaskInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class eventTest {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    void t1() throws InterruptedException {
        for (int i = 0; i < 5; i++) {
            long start = System.currentTimeMillis();
            applicationEventPublisher.publishEvent(new TaskEvent(TaskInfo.builder().id(1L).name("zhangsan").build()));
            System.out.println("耗时：" + (System.currentTimeMillis()- start));
        }
        TimeUnit.SECONDS.sleep(2L);
    }



}
