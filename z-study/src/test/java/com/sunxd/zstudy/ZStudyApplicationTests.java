package com.sunxd.zstudy;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.sql.Array;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
class ZStudyApplicationTests {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Autowired
    private DefaultRedisScript<Boolean> redisLimitScript;

    @Autowired
    private DefaultRedisScript<Boolean> redisEqScript;

    @Autowired
    private DefaultRedisScript<String> redisGetsScript;

    @Test
    void t1() {
        List<String> keys = Arrays.asList("k1","k2","k3");
        Boolean b = stringRedisTemplate.execute(redisLimitScript,keys,"val1","val2","val3");
    }

    @Test
    void t2() {
        List<String> keys = Arrays.asList("k1");
        Boolean b = stringRedisTemplate.execute(redisEqScript,keys,"val1");
        System.out.println(b);
    }
    @Test
    void t3() {
        List<String> keys = Arrays.asList("k1");
        String b = stringRedisTemplate.execute(redisGetsScript,keys,"val1");
        System.out.println(b);
    }

    @Test
    void test(){
        String s = "1|1";
        System.out.println(s.split("\\|").length);
    }


}
