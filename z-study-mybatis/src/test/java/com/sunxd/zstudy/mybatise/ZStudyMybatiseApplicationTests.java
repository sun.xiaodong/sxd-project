package com.sunxd.zstudy.mybatise;

import com.alibaba.fastjson.JSON;
import com.sunxd.zstudy.mybatise.application.facade.InteractiveJoinWriteFacade;
import com.sunxd.zstudy.mybatise.application.facade.request.interactive.base.BaseInteractiveJoinRequest;
import com.sunxd.zstudy.mybatise.domain.contans.enums.InteractiveType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ZStudyMybatiseApplicationTests {

    @Autowired
    private InteractiveJoinWriteFacade interactiveJoinWriteFacade;

    @Test
    void contextLoads() {
        BaseInteractiveJoinRequest request = new BaseInteractiveJoinRequest();
        request.setPassengerUuid("userId");
        request.setActivityCode("activityCode");
        request.setInteractiveType(InteractiveType.LOTTERY);
        for (int i = 0; i < 20; i++) {
        System.out.println(JSON.toJSONString(interactiveJoinWriteFacade.go(request)));
        }
    }

}
