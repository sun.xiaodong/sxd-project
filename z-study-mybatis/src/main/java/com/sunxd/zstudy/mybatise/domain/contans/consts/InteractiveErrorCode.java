package com.sunxd.zstudy.mybatise.domain.contans.consts;

public interface InteractiveErrorCode {
    String INTERNAL_SERVER_ERROR = "系统异常";
    String INTERACTIVE_TYPE_IS_NULL = "活动类型为空";
    String INTERACTIVE_NOT_EXIST = "活动不存在";
    String INTERACTIVE_ID_NULL = "活动id为空";
    String INTERACTIVE_NOT_START = "活动未开始";
    String INTERACTIVE_END = "活动已结束";
    String INTERACTIVE_REWARD_NOT_EXIST = "活动奖品不存在";

}
