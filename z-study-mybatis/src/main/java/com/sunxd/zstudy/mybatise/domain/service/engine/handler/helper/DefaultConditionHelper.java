package com.sunxd.zstudy.mybatise.domain.service.engine.handler.helper;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Description: 条件处理帮助实现
 * Author: support 9
 * Date: 2018/07/02
 */
@Component
@Slf4j
public class DefaultConditionHelper extends AbstractConditionHelper {

    @FunctionalInterface
    public interface Judger {
        Boolean judge(Object left, Object right);
    }

    private Map<String, ConditionHelper> helperMap;

    @Autowired
    public DefaultConditionHelper(List<ConditionHelper> conditionHelpers) {
        helperMap = Maps.newHashMap();
        if (!CollectionUtils.isEmpty(conditionHelpers)) {
            for (ConditionHelper helper : conditionHelpers) {
                if (helper instanceof DefaultConditionHelper) {
                    continue;
                }
                helperMap.put(helper.getClassType(), helper);
            }
        }
    }

    @Override
    public Boolean greater(Object right, Object left) {
        if (log.isDebugEnabled()) {
            log.info("[GREATER] left: {}, right: {}", left, right);
        }
        Boolean result;
        ConditionHelper conditionHelper = helperMap.get(right.getClass().getSimpleName());
        if (Objects.nonNull(conditionHelper)) {
            result = conditionHelper.greater(right, left);
        } else {
            result = super.greater(right, left);
        }
        if (log.isDebugEnabled()) {
            log.info("[GREATER] left: {}, right: {}, result: {}", left, right, result);
        }
        return result;
    }

    @Override
    public Boolean equal(Object right, Object left) {
        if (log.isDebugEnabled()) {
            log.info("[EQUAL] left: {}, right: {}", left, right);
        }
        Boolean result;
        ConditionHelper conditionHelper = helperMap.get(right.getClass().getSimpleName());
        if (Objects.nonNull(conditionHelper)) {
            result = conditionHelper.equal(right, left);
        } else {
            result = super.equal(right, left);
        }
        if (log.isDebugEnabled()) {
            log.info("[EQUAL] left: {}, right: {}, result: {}", left, right, result);
        }
        return result;
    }

    @Override
    public Boolean in(Object right, Object left) {
        if (log.isDebugEnabled()) {
            log.info("[IN] left: {}, right: {}", left, right);
        }
        Boolean result;
        ConditionHelper conditionHelper = helperMap.get(right.getClass().getSimpleName());
        if (Objects.nonNull(conditionHelper)) {
            result = conditionHelper.in(right, left);
        } else {
            result = super.in(right, left);
        }
        if (log.isDebugEnabled()) {
            log.info("[IN] left: {}, right: {}, result: {}", left, right, result);
        }
        return result;
    }


    @Override
    public String getClassType() {
        return "Default";
    }

}
