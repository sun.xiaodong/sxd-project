//package com.sunxd.zstudy.mybatise.infrastructure.utils;
//
//import com.sunxd.zstudy.mybatise.out.bean.response.Response;
//import io.terminus.ce.exception.InternalServerException;
//import io.terminus.common.model.Response;
//
//import java.util.function.Function;
//
///**
// */
//public class ResponseHelper {
//    private ResponseHelper() {
//    }
//
//    /**
//     * 包装Response转换
//     *
//     * @param origin 原Response
//     * @param mapper 转换方法
//     * @param <T>    原Response包装类型
//     * @param <R>    转换后Response包装类型
//     * @return 转换后Response
//     */
//    public static <T, R> Response<R> map(Response<T> origin, Function<? super T, ? extends R> mapper) {
//        if (!origin.isSuccess()) {
//            return Response.fail(origin.getError());
//        }
//        return Response.ok(mapper.apply(origin.getResult()));
//    }
//
//    public static <T> T or500(Response<T> resp) {
//        if (resp.isSuccess()) {
//            return resp.getResult();
//        } else {
//            throw new InternalServerException(resp.getCode() , resp.getError(), resp.getArgs());
//        }
//    }
//
//    public static void checkOr500(Response resp) {
//        if (!resp.isSuccess()) {
//            throw new InternalServerException(resp.getCode() , resp.getError(), resp.getArgs());
//        }
//    }
//}
