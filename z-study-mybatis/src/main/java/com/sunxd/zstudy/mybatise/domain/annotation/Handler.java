package com.sunxd.zstudy.mybatise.domain.annotation;

import com.sunxd.zstudy.mybatise.domain.contans.enums.ConditionType;
import com.sunxd.zstudy.mybatise.domain.contans.enums.OperatorType;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description: 条件操作类标识
 * User: support 9
 * Date: 2018/7/16
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface Handler {

    /**
     * 条件类型
     * @return ConditionType[]
     */
    ConditionType conditionType();

    /**
     * 操作类型
     * @return OperatorType[]
     */
    OperatorType operatorType();
}
