package com.sunxd.zstudy.mybatise.domain.contans.enums;

import com.sunxd.zstudy.mybatise.infrastructure.utils.StringUtils;
import lombok.Getter;

/**
 **/
@Getter
public enum LimitTypeEnum {

    CITY_LIMIT("city","参与城市限制"),
    GROUP_LIMIT("group","群组限制"),
    TOTAL_PERSON("totalPerson","总人数"),



    ;
    private final String name;
    private final String desc;

    LimitTypeEnum(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public static LimitTypeEnum fromName(String name) {
        if (StringUtils.isNotBlank(name)) {
            for (LimitTypeEnum value : LimitTypeEnum.values()) {
                if (value.name().equalsIgnoreCase(name)) {
                    return value;
                }
            }
        }
        return null;
    }


}
