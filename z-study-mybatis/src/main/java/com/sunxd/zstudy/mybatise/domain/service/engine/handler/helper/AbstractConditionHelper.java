package com.sunxd.zstudy.mybatise.domain.service.engine.handler.helper;

import java.util.Collection;
import java.util.Iterator;

/**
 * Description:
 * Author xie
 * Date 2019/1/14 下午7:36
 */
public abstract class AbstractConditionHelper implements ConditionHelper {

    @Override
    public Boolean greater(Object right, Object left) {

        Comparable comparable = left.toString();
        if (comparable.compareTo(right) > 0) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    @Override
    public Boolean equal(Object right, Object left) {
        Comparable comparable = left.toString();
        if (comparable.compareTo(right) == 0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public Boolean in(Object right, Object left) {
        Boolean result = Boolean.FALSE;
        if (right instanceof Collection) {
            for (Iterator iterator = ((Collection) right).iterator(); iterator.hasNext(); ) {
                Object next = iterator.next();
                if (equal(next, left)) {
                    result = Boolean.TRUE;
                    break;
                }
            }

        }
        return result;
    }

}
