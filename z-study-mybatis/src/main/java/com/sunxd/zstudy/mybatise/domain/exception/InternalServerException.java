package com.sunxd.zstudy.mybatise.domain.exception;

import org.springframework.util.StringUtils;

import java.util.Arrays;

public class InternalServerException extends RuntimeException {

    private static final long serialVersionUID = -6524634004016285879L;
    private String code;
    private String message;
    private Object[] args;

    public InternalServerException(String code) {
        this.code = code;
    }

    public InternalServerException(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public InternalServerException(String code, String message, Object... args) {
        this.code = code;
        this.message = message;
        this.args = args;
    }

    public int getStatus() {
        return 500;
    }

    public Object[] getArgs() {
        return this.args;
    }

    public String getMessage() {
        return !StringUtils.hasText(this.message) ? this.code : this.message;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public String toString() {
        return "InternalServerException(code=" + this.getCode() + ", message=" + this.getMessage() + ", args=" + Arrays.deepToString(this.getArgs()) + ")";
    }

    public InternalServerException() {
    }

}
