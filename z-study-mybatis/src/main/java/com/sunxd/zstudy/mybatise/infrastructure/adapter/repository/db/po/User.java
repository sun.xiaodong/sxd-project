package com.sunxd.zstudy.mybatise.infrastructure.adapter.repository.db.po;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author: 作者名称
 * @date: 2021-04-12 00:24
 **/
@Data
@EqualsAndHashCode
public class User implements Serializable {
    private static final long serialVersionUID = -8737387239978682957L;

    private Long id;
    private String name;
    private String address;
}
