package com.sunxd.zstudy.mybatise.domain.contans.consts;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 返回动作类型
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PrizeActionTypeDict {

    /**
     * 奖品
     */
    public final static String PRIZE = "PRIZE";



}
