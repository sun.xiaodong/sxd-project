package com.sunxd.zstudy.mybatise.out.bean.request;

import lombok.Data;

/**
 * @author: 作者名称
 * @date: 2021-06-01 22:42
 **/
@Data
public class PointAdd {

    private String name;
    private Integer num;
}
