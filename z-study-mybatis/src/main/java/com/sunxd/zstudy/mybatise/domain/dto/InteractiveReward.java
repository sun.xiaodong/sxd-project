package com.sunxd.zstudy.mybatise.domain.dto;

import com.sunxd.zstudy.mybatise.domain.contans.enums.PrizeIssueType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InteractiveReward implements Serializable {
    private static final long serialVersionUID = -5193516288460256233L;

    private PrizeIssueType prizeIssueType;

    private List<InteractiveRewardDetail> interactiveRewardDetails;

    private List<LimitCondition> limitConditions;
}
