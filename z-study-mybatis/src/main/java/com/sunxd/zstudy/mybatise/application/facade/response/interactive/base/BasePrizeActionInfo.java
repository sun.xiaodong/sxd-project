package com.sunxd.zstudy.mybatise.application.facade.response.interactive.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 */
@EqualsAndHashCode()
@Data
public abstract class BasePrizeActionInfo implements Serializable {
    private static final long serialVersionUID = 7788741133650993294L;

    /**
     * 奖品类型 {@link PrizeActionTypeDict}
     *
     * @return 奖品类型
     */
    public abstract String getActionType();

    /**
     * 获取奖品列表
     *
     * @return 奖品列表集合
     */
    public abstract List<? extends BasePrizeRecordInfo> getResultList();
}
