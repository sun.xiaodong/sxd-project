package com.sunxd.zstudy.mybatise.application.facade;

import com.sunxd.zstudy.mybatise.application.facade.request.interactive.base.BaseInteractiveJoinRequest;
import com.sunxd.zstudy.mybatise.application.facade.response.interactive.base.BaseInteractiveJoinDetailInfo;
import com.sunxd.zstudy.mybatise.out.bean.response.Response;

/**
 * Description:
 */
public interface InteractiveJoinWriteFacade {

    /**
     * 参与能力
     *
     * @param request 参与请求
     * @return 参加能力
     */
    Response<BaseInteractiveJoinDetailInfo<?>> go(BaseInteractiveJoinRequest request);

}
