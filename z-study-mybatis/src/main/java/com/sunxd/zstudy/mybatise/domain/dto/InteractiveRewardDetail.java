package com.sunxd.zstudy.mybatise.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InteractiveRewardDetail implements Serializable {
    private static final long serialVersionUID = -5193516288465256233L;

    private Integer id;
    private String rewardName;
    private String rewardType;
    private Integer rewardTotal;
    /**
     * 奖品图片
     */
    private String rewardImage;

    /**
     * 奖品配置总数量
     */
    private Integer totalQty;

    /**
     * 奖品可用数量
     */
    private Integer availableQty;

    /**
     * 概率范围
     */
    private Double rightNum;

    private List<LimitCondition> limitConditions;
}
