package com.sunxd.zstudy.mybatise.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LimitCondition  implements Serializable {
    private static final long serialVersionUID = -7914391215937843204L;

    private String name;
    private Boolean isOpen;
    private String operateType;
    private String value;
}
