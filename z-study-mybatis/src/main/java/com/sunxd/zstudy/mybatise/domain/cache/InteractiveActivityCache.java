package com.sunxd.zstudy.mybatise.domain.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.sunxd.zstudy.mybatise.domain.contans.enums.InteractiveStatusEnum;
import com.sunxd.zstudy.mybatise.domain.contans.enums.InteractiveType;
import com.sunxd.zstudy.mybatise.domain.contans.enums.PrizeIssueType;
import com.sunxd.zstudy.mybatise.domain.dto.InteractiveActivity;
import com.sunxd.zstudy.mybatise.domain.dto.InteractiveReward;
import com.sunxd.zstudy.mybatise.domain.dto.InteractiveRewardDetail;
import com.sunxd.zstudy.mybatise.infrastructure.utils.DateUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @description 活动PO
 * <br/>
 * @author biebf
 * @createDate 2021-12-23
 */
public class InteractiveActivityCache  {



    static int i = 0;
    public static LoadingCache<String, InteractiveActivity> cache = CacheBuilder.newBuilder()
            //设置过期策略
            .expireAfterWrite(3, TimeUnit.SECONDS)
            //设置刷新策略
            .refreshAfterWrite(3, TimeUnit.SECONDS)
            .build(new CacheLoader<String, InteractiveActivity>() {
                @Override
                public InteractiveActivity load(String actCode) throws Exception {
                    System.out.println("第"+i+"次加载");
                    i++;
                    return getInteractiveActivity();
                }
            });

    public static InteractiveActivity getInteractiveActivity(){
        List<InteractiveRewardDetail> interactiveRewardDetails = Lists.newArrayList();
        for (int j = 0; j < 5; j++) {
            String rewardType;
            Double rightNumb;
            switch (j){
                case 1:
                    rewardType = "ITEM";
                    rightNumb = 0.7;
                    break;
                case 2:
                    rewardType = "COUPON";
                    rightNumb = 0.3;
                    break;
                case 3:
                    rewardType = "POINT";
                    rightNumb = 0.9;
                    break;
                case 4:
                    rewardType = "BENEFIT";
                    rightNumb = 0.1;
                    break;
                default:
                    rewardType = "THANKS";
                    rightNumb = 1.0;
            }
            interactiveRewardDetails.add(InteractiveRewardDetail.builder()
                    .id(j)
                    .rewardName("奖品"+i)
                    .rewardType(rewardType)
                    .rightNum(rightNumb)
                    .build());
        }
        return InteractiveActivity.builder()
                .activityCode("activityCode")
                .activityName("activityName")
                .activityType(InteractiveType.LOTTERY.name())
                .activityStatus(InteractiveStatusEnum.ENABLE_STATUS.getValue())
                .beginTime(DateUtils.toDate(DateUtils.getLocalDateTime()))
                .endTime(DateUtils.toDate(DateUtils.addDays(DateUtils.getLocalDateTime(),1)))
                .interactiveReward(InteractiveReward.builder()
                        .interactiveRewardDetails(interactiveRewardDetails)
                        .prizeIssueType(PrizeIssueType.PROBABILITY)
                        .build())
                .build();
    }


    public static void main(String[] args) throws ExecutionException, InterruptedException {
        LocalDateTime localDateTime = LocalDateTime.now();

        System.out.println("当前时间:" + localDateTime);

        LocalDateTime plusDay = localDateTime.plusDays(1);

        System.out.println("当前日期一天后的日期:" + plusDay);

        System.out.println(DateUtils.toDate(DateUtils.addDays(DateUtils.getLocalDateTime(),1)));
//        for (int j = 0; j < 20; j++) {
//            new Thread(()->{
//                while (true){
//                    try {
//                        System.out.println(cache.get("activityCode"));
//                        TimeUnit.SECONDS.sleep(1);
//                    } catch (ExecutionException | InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            }).start();
//        }


    }


}
