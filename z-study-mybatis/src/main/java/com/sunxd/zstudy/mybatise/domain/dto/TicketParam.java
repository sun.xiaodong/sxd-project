package com.sunxd.zstudy.mybatise.domain.dto;

import com.sunxd.zstudy.mybatise.domain.contans.enums.TicketBusinessType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author Yangxiumei
 * Date: 2020-11-28 14:16
 * Description: 新增活动机会
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TicketParam {


    /**
     * 会员ID
     */
    private String ownerId;

    /**
     * 活动机会业务类型
     */
    private TicketBusinessType businessType;

    /**
     * 业务ID（业务ID+业务类型需要保证唯一）
     */
    private String businessId;

    /**
     * 数量
     */
    private Integer quantity;

}
