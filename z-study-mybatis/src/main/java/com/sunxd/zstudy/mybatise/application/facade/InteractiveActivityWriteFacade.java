package com.sunxd.zstudy.mybatise.application.facade;

import com.sunxd.zstudy.mybatise.out.bean.request.InteractiveActivityAddRequest;
import com.sunxd.zstudy.mybatise.out.bean.response.Response;


public interface InteractiveActivityWriteFacade {
    Response<Boolean> create(InteractiveActivityAddRequest request);
}
