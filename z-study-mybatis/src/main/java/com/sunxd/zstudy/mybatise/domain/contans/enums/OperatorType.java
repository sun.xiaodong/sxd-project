package com.sunxd.zstudy.mybatise.domain.contans.enums;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Description: 操作类型
 * Author: support 9
 * Date: 2018/07/02
 */
public enum OperatorType {

    AND,
    OR,
    GT,
    GTE,
    LT,
    LTE,
    IN,//left属于集合right
    NIN,//left不属于集合right
    EQ,
    NEQ,
    CONTAIN,//包含,两个集合,要求right是left的子集
    NOT_CONTAIN,//不包含,两个集合,right不是left的子集
    ALL_EQ,//集合完全相同，内的所有元素完全相等
    NOT_ALL_EQ,//两个集合不同
    BE_IN,//right属于集合left
    BE_NOT_IN,//right不属于集合left
    BE_CONTAIN,//被包含,两个集合,要求left是right的子集
    BE_NOT_CONTAIN,//不被包含,两个集合,要求left不是right的子集
    HAVE_INTERSECTION,//有交集
    NOT_HAVE_INTERSECTION,//没有交集
    DEFAULT,
    GTE_LTE,
    GTE_LT,
    GT_LTE,
    GT_LT,

    LEVEL_EQ,
    LEVEL_GT,
    LEVEL_GTE,
    LEVEL_LT,
    LEVEL_LTE,

    // SKU金额判断,特殊处理
    SKU_AMOUNT_GT,
    SKU_AMOUNT_GTE,
    SKU_AMOUNT_EQ,
    SKU_AMOUNT_LT,
    SKU_AMOUNT_LTE,
    SKU_AMOUNT_GT_LT,
    SKU_AMOUNT_GT_LTE,
    SKU_AMOUNT_GTE_LT,
    SKU_AMOUNT_GTE_LTE;

    static Map<String, OperatorType> leftMap = new ConcurrentHashMap<>();
    static Map<String, OperatorType> rightMap = new ConcurrentHashMap<>();
    static {
        leftMap.put(OperatorType.GTE_LTE.name(), OperatorType.GTE);
        rightMap.put(OperatorType.GTE_LTE.name(), OperatorType.LTE);

        leftMap.put(OperatorType.GTE_LT.name(), OperatorType.GTE);
        rightMap.put(OperatorType.GTE_LT.name(), OperatorType.LT);

        leftMap.put(OperatorType.GT_LT.name(), OperatorType.GT);
        rightMap.put(OperatorType.GT_LT.name(), OperatorType.LT);

        leftMap.put(OperatorType.GT_LTE.name(), OperatorType.GT);
        rightMap.put(OperatorType.GT_LTE.name(), OperatorType.LTE);
    }

    public static OperatorType getRangeLeft(OperatorType operatorType){
        return leftMap.get(operatorType.name());
    }

    public static OperatorType getRangeRight(OperatorType operatorType){
        return rightMap.get(operatorType.name());
    }
}
