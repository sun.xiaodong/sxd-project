package com.sunxd.zstudy.mybatise.infrastructure.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateUtils {

    static ZoneId zoneId = ZoneId.systemDefault();

    public static LocalDateTime getLocalDateTime(){
        return LocalDateTime.now();
    }

    public static LocalDateTime addDays(LocalDateTime  localDateTime,long days){
        return localDateTime.plusDays(days);
    }

    public static Date toDate(LocalDateTime localDateTime){

        return Date.from(localDateTime.atZone(zoneId).toInstant());
    }





}
