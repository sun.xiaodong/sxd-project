package com.sunxd.zstudy.mybatise.application.facade.response.interactive.prize;

import com.sunxd.zstudy.mybatise.application.facade.response.interactive.base.BasePrizeActionInfo;
import com.sunxd.zstudy.mybatise.application.facade.response.interactive.base.BasePrizeRecordInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

import static com.sunxd.zstudy.mybatise.domain.contans.consts.InteractiveProcessDict.PRIZE;

/**
 * @author wangxinhua
 * Date: 2020/12/7 9:39 下午
 * Description:
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PrizeActionInfo extends BasePrizeActionInfo {
    private static final long serialVersionUID = 7788741133650993294L;

    private final List<PrizeRecordInfo> resultList;

    @Override
    public String getActionType() {
        return PRIZE;
    }

    @Override
    public List<? extends BasePrizeRecordInfo> getResultList() {
        return resultList;
    }
}
