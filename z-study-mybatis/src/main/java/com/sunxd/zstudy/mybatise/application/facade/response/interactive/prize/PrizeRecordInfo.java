package com.sunxd.zstudy.mybatise.application.facade.response.interactive.prize;

import com.sunxd.zstudy.mybatise.application.facade.response.interactive.base.BasePrizeRecordInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class PrizeRecordInfo extends BasePrizeRecordInfo {

    private static final long serialVersionUID = 4279211002743429763L;


    /**
     * 奖品ID
     */
    private Integer rewardId;

    /**
     * 奖品类型
     */
    private String rewardType;

    /**
     * 外部奖品名称
     */
    private String rewardName;


    /**
     * 奖品图片
     */
    private String rewardMainImage;

    /**
     * 发放数量
     */
    private Integer prizeQty;

    /**
     * 奖品扩展信息
     */
    private Map<String, Object> prizeInfo;
}
