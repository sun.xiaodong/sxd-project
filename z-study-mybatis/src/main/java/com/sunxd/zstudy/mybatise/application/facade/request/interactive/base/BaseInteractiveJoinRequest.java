package com.sunxd.zstudy.mybatise.application.facade.request.interactive.base;

import com.sunxd.zstudy.mybatise.domain.contans.consts.InteractiveErrorCode;
import com.sunxd.zstudy.mybatise.domain.contans.enums.InteractiveType;
import com.sunxd.zstudy.mybatise.infrastructure.utils.ParamUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Description:
 */
@EqualsAndHashCode()
@Data
@NoArgsConstructor
@AllArgsConstructor
@Api("活动参与表单")
public class BaseInteractiveJoinRequest implements Serializable {
    private static final long serialVersionUID = 4381644119037477162L;

    /**
     * 活动id
     */
    @ApiModelProperty(name = "活动编码")
    private String activityCode;

    private String activityUuid;

    private String curCity;

    /**
     * 活动类型
     */
    @ApiModelProperty(name = "活动类型")
    private InteractiveType interactiveType;

    /**
     * 用户id
     */
    @ApiModelProperty(name = "用户id")
    private String passengerUuid;

    /**
     * 头像
     */
    @ApiModelProperty(name = "头像URL", notes = "头像url")
    private String avatar;

    /**
     * 昵称
     */
    @ApiModelProperty(name = "昵称", notes = "昵称")
    private String nickName;

    private Integer flag;

    /**
     * 只有使用积分兑换的时候，才有值
     * 是否使用支付规则
     */
    private String payRuleJson;


    public void checkParam() {
        ParamUtil.nonNull(activityCode, InteractiveErrorCode.INTERACTIVE_ID_NULL);
        ParamUtil.nonNull(interactiveType, InteractiveErrorCode.INTERACTIVE_TYPE_IS_NULL);
    }
}