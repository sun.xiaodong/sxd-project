package com.sunxd.zstudy.mybatise.domain.contans.enums;

import com.sunxd.zstudy.mybatise.infrastructure.utils.StringUtils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 */
@RequiredArgsConstructor
public enum InteractiveType {

    /**
     * 签到
     */
    SIGN_IN("签到"),

    /**
     * 线下打卡
     */
    CLOCK_IN("线下打卡"),

    /**
     * 裂变
     */
    FISSION("裂变"),

    /**
     * 报名
     */
    SIGN_UP("报名"),

    /**
     * 抽奖
     */
    LOTTERY("抽奖"),

    /**
     * 答题
     */
    ANSWER("答题"),

    /**
     * 问卷
     */
    QUESTIONNAIRE("问卷"),

    /**
     * 投票
     */
    VOTE("投票"),

    /**
     * 报名-投票
     */
    SIGN_VOTE("报名_投票");

    private final static Set<String> NAME_SET
            = Stream.of(InteractiveType.values())
            .map(InteractiveType::name)
            .collect(Collectors.toSet());

    /**
     * 是否是互动营销类型
     *
     * @param target 目标参数
     * @return 判断是否为互动营销类型
     */
    public static boolean isCapability(String target) {
        if (StringUtils.isEmpty(target)) {
            return false;
        }
        return NAME_SET.contains(target);
    }

    @Getter
    private final String desc;
}
