package com.sunxd.zstudy.mybatise.out.bean.enums;

public interface BaseEnumInterface {
    Integer getCode();

    String getMsg();

}
