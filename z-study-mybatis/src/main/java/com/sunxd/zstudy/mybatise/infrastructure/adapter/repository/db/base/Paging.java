package com.sunxd.zstudy.mybatise.infrastructure.adapter.repository.db.base;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Paging<T> implements Serializable {
    private static final long serialVersionUID = 1658189396722391985L;

    private Long total;
    private List<T> data;
    private Long pageCount;
    private Integer pageSize;

    public Paging() {
    }

    public Paging(Long total, List<T> data, Integer pageSize) {
        this.data = data;
        this.total = total;
        this.pageSize = pageSize;
        if (total > 0L && pageSize > 0) {
            long l = total % (long)pageSize;
            if (l == 0L) {
                this.pageCount = total / (long)pageSize;
            } else {
                this.pageCount = total / (long)pageSize + 1L;
            }
        } else {
            this.pageCount = 0L;
        }

    }

    public Paging(Long total, List<T> data) {
        this.data = data;
        this.total = total;
    }

    public List<T> getData() {
        return this.data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public Long getTotal() {
        return this.total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Boolean isEmpty() {
        return Objects.equals(0L, this.total) || this.data == null || this.data.isEmpty();
    }

    public Long getPageCount() {
        return this.pageCount;
    }

    public Paging pageCount(Long pageCount) {
        this.pageCount = pageCount;
        return this;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public Paging pageSize(Integer pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public static <T> Paging<T> empty(Class<T> clazz) {
        List<T> emptyList = Collections.emptyList();
        return new Paging(0L, emptyList);
    }

    public static <T> Paging<T> empty() {
        return new Paging(0L, Collections.emptyList());
    }

    protected boolean canEqual(Object other) {
        return other instanceof Paging;
    }


}
