package com.sunxd.zstudy.mybatise.infrastructure.utils;

import com.google.common.base.Throwables;
import com.sunxd.zstudy.mybatise.out.bean.response.Response;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Supplier;

@Slf4j
public class ResponseUtils {
    public static <T> Response<T> catchException(Supplier<T> supplier, String errPrefix, String errorResponse) {
        try {
            return Response.createSuccess(supplier.get());
        } catch (IllegalArgumentException var4) {
            return Response.createError(var4.getMessage());
        } catch (Exception var6) {
            log.info("errormsg:{}", Throwables.getStackTraceAsString(var6));
            return Response.createError(errorResponse);
        }
    }
}
