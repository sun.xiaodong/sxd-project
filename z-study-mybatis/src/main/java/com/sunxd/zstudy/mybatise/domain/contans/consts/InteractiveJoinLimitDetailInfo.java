package com.sunxd.zstudy.mybatise.domain.contans.consts;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 *  参与活动限制类型，不同的类型走不同的限制
 */
public class InteractiveJoinLimitDetailInfo {

    /**
     * 风控-门槛
     */
    public final static String JOIN_CITY = "joinCity";

    public final static String JOIN_GROUP = "joinGroup";

    public final static String TOTAL_PERSON = "totalPerson";



}
