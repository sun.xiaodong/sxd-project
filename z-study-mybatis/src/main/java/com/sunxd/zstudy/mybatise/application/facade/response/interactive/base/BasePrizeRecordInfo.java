package com.sunxd.zstudy.mybatise.application.facade.response.interactive.base;


import java.io.Serializable;

/**
 * Description: 基础奖品信息
 */
public abstract class BasePrizeRecordInfo implements Serializable {
    private static final long serialVersionUID = 2278432805510558996L;
}
