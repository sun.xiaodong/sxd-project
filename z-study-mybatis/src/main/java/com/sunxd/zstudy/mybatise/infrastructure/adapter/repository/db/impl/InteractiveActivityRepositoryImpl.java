package com.sunxd.zstudy.mybatise.infrastructure.adapter.repository.db.impl;

import com.sunxd.zstudy.mybatise.domain.dto.InteractiveActivityAddDto;
import com.sunxd.zstudy.mybatise.domain.repository.InteractiveActivityRepository;
import com.sunxd.zstudy.mybatise.infrastructure.adapter.repository.db.convert.InteractiveActivityPoConvert;
import com.sunxd.zstudy.mybatise.infrastructure.adapter.repository.db.dao.InteractiveActivityDao;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class InteractiveActivityRepositoryImpl implements InteractiveActivityRepository {

   private final InteractiveActivityDao interactiveActivityDao;

    @Override
    public boolean save(InteractiveActivityAddDto addDto) {
        return interactiveActivityDao.create(InteractiveActivityPoConvert.addDtoToAddPo(addDto));
    }
}
