package com.sunxd.zstudy.mybatise.domain.dto;

import com.sunxd.zstudy.mybatise.domain.contans.enums.ConditionType;
import com.sunxd.zstudy.mybatise.domain.contans.enums.OperatorType;
import lombok.*;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Condition implements Serializable {
    private static final long serialVersionUID = 6787834377679192109L;
   /**
    * 值类型
    */
    private ConditionType type;

    /**
     * 字段名
     */
    private Object left;

    /**
     * 条件类型
     */
    private OperatorType operator = OperatorType.DEFAULT;

    /**
     * 被比较的对象
     */
    private Object right;
}
