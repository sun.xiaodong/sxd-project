package com.sunxd.zstudy.mybatise.domain.service.interactivity;

import com.sunxd.zstudy.mybatise.domain.convert.activity.InteractiveActivityDtoConvert;
import com.sunxd.zstudy.mybatise.domain.dto.InteractiveActivityAddDto;
import com.sunxd.zstudy.mybatise.domain.repository.InteractiveActivityRepository;
import com.sunxd.zstudy.mybatise.out.bean.request.InteractiveActivityAddRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class InteractiveActivityWriteService {

    private final InteractiveActivityRepository interactiveActivityRepository;

    public Boolean create(InteractiveActivityAddRequest request) {
        InteractiveActivityAddDto interactiveActivityAddDto = InteractiveActivityDtoConvert.addReqToAddDto(request);
        return interactiveActivityRepository.save(interactiveActivityAddDto);
    }
}
