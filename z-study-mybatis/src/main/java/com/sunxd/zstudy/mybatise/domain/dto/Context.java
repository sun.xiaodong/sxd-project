package com.sunxd.zstudy.mybatise.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class Context implements Serializable {
    private static final long serialVersionUID = 4810479487682033586L;

    protected Map<String, Object> data;


    public <T> T get(String name){
        return (T) data.get(name);
    }

}
