package com.sunxd.zstudy.mybatise.domain.contans.consts;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 *  参与活动限制类型，不同的类型走不同的限制
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InteractiveJoinLimitType {

    /**
     * 风控-门槛
     */
    public final static String THRESHOLD = "threshold";
    public final static String SAFE = "safe";



}
