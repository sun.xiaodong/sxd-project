package com.sunxd.zstudy.mybatise.infrastructure.adapter.repository.db.dao;

import com.sunxd.zstudy.mybatise.infrastructure.adapter.repository.db.base.MyBatisDao;
import com.sunxd.zstudy.mybatise.infrastructure.adapter.repository.db.po.InteractiveActivityPO;
import org.springframework.stereotype.Component;

@Component
public class InteractiveActivityDao extends MyBatisDao<InteractiveActivityPO> {
}
