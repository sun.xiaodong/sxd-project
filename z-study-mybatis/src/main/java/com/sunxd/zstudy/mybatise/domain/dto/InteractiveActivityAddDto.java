package com.sunxd.zstudy.mybatise.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @description 活动PO
 * <br/>
 * @author biebf
 * @createDate 2021-12-23
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InteractiveActivityAddDto implements Serializable {
    private static final long serialVersionUID = 3321699875308963034L;

    /**
     * uuid
     */
    private String uuid;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 活动编码
     */
    private String activityCode;
    /**
     * 开始时间
     */
    private Date beginTime;
    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 活动类型 1 抽奖 2签到
     */
    private Integer activityType;

    /**
     * 活动状态【1:草稿，2：待生效，3：生效中，4：已结束，5：已停用】
     */
    private Integer activityStatus;
    /**
     * 审核状态【1：待审核，2：已通过，3：驳回】
     */
    private Integer auditStatus;
    /**
     * 活动内容
     */
    private String content;
    /**
     * 页面配置
     */
    private String pageResource;

    /**
     * 创建人
     */
    private String creator;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新人
     */
    private String updater;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 删除标识 0: 默认未删除 1: 已删除
     */
    private Integer deleteFlag;
}
