package com.sunxd.zstudy.mybatise.application.facade.response.interactive.base;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * Description:
 */
@EqualsAndHashCode()
@Data
@ApiModel("基础参与信息返回")
public class BaseInteractiveJoinDetailInfo<T extends BasePrizeActionInfo>  implements Serializable {
    private static final long serialVersionUID = 8034893995026289560L;

    /**
     * 奖品信息
     */
    private final List<T> actionList;
}
