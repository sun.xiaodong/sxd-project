package com.sunxd.zstudy.mybatise.domain.service.engine.handler.join;

import com.sunxd.zstudy.mybatise.application.facade.request.interactive.base.BaseInteractiveJoinRequest;
import lombok.NonNull;

import java.util.concurrent.ExecutionException;

/**
 * Description: 能力执行处理器
 */
public interface InteractiveJoinHandler<T extends BaseInteractiveJoinRequest, R> {

    /**
     * 参与能力执行接口
     *
     * @param request 参与活动请求
     * @return 参与活动信息, R - 能力额外信息
     */
    R join(@NonNull T request) throws ExecutionException;

}
