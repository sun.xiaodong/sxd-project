package com.sunxd.zstudy.mybatise.domain.contans.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @description 数据删除标识枚举
 * <br/>
 * @author biebf
 * @date 2021/12/28
 */
@Getter
@AllArgsConstructor
public enum DeleteFlagEnum {
    /**
     * 删除标识
     */
    NOT_DELETE(0, "未删除"),
    DELETE(1, "已删除"),
    ;

    /**
     * 事件类型
     */
    private final Integer type;
    /**
     * 事件名称
     */
    private final String name;

    public static DeleteFlagEnum match(Integer type) {
        for (DeleteFlagEnum instance : values()) {
            if (instance.getType().equals(type)) {
                return instance;
            }
        }
        throw new IllegalArgumentException("unexpected DeleteFlagEnum value: " + type);
    }
}
