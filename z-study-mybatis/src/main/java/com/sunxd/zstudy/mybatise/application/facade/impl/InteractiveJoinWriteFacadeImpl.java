package com.sunxd.zstudy.mybatise.application.facade.impl;

import com.sunxd.zstudy.mybatise.application.facade.InteractiveJoinWriteFacade;
import com.sunxd.zstudy.mybatise.application.facade.request.interactive.base.BaseInteractiveJoinRequest;
import com.sunxd.zstudy.mybatise.application.facade.response.interactive.base.BaseInteractiveJoinDetailInfo;
import com.sunxd.zstudy.mybatise.domain.service.engine.dispatcher.interactive.InteractiveJoinHandlerDispatcher;
import com.sunxd.zstudy.mybatise.out.bean.response.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Description:
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class InteractiveJoinWriteFacadeImpl implements InteractiveJoinWriteFacade {

    private final InteractiveJoinHandlerDispatcher interactiveJoinHandlerDispatcher;

    @Override
    public Response<BaseInteractiveJoinDetailInfo<?>> go(BaseInteractiveJoinRequest request) {
         BaseInteractiveJoinDetailInfo<?> res = interactiveJoinHandlerDispatcher.dispatch(request);
         return Response.createSuccess(res);
    }
}
