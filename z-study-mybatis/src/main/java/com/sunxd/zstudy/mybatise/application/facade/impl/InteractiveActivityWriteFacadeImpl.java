package com.sunxd.zstudy.mybatise.application.facade.impl;

import com.sunxd.zstudy.mybatise.application.facade.InteractiveActivityWriteFacade;
import com.sunxd.zstudy.mybatise.domain.service.interactivity.InteractiveActivityWriteService;
import com.sunxd.zstudy.mybatise.infrastructure.utils.ResponseUtils;
import com.sunxd.zstudy.mybatise.out.bean.request.InteractiveActivityAddRequest;
import com.sunxd.zstudy.mybatise.out.bean.response.Response;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@Slf4j
@AllArgsConstructor
public class InteractiveActivityWriteFacadeImpl implements InteractiveActivityWriteFacade {


    private final InteractiveActivityWriteService interactiveActivityWriteService;

    @Override
    public Response<Boolean> create(InteractiveActivityAddRequest request) {
        return ResponseUtils.catchException(() -> interactiveActivityWriteService.create(request), "ActivityCommandFacadeImpl.create", "activity.create.failure");
    }
}
