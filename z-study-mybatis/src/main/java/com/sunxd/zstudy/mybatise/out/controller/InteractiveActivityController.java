package com.sunxd.zstudy.mybatise.out.controller;

import com.sunxd.zstudy.mybatise.application.facade.InteractiveActivityWriteFacade;
import com.sunxd.zstudy.mybatise.out.bean.request.InteractiveActivityAddRequest;
import com.sunxd.zstudy.mybatise.out.bean.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/api/admin/interactive/activity")
public class InteractiveActivityController {


    @Autowired
    private InteractiveActivityWriteFacade interactiveActivityWriteFacade;

    @RequestMapping("/create")
    public Response<Boolean> create(@RequestBody InteractiveActivityAddRequest request){
        return interactiveActivityWriteFacade.create(request);
    }


}
