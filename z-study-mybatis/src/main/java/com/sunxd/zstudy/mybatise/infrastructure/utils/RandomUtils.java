package com.sunxd.zstudy.mybatise.infrastructure.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Random;

@Component
public class RandomUtils {

    private Random random;

    @Autowired
    public RandomUtils() {
        random = new Random();
    }

    /**
     * 获取(0-1之间随机数)
     *
     * @return 随机数
     */
    public BigDecimal random() {
        return BigDecimal.valueOf(random.nextDouble());
    }
}
