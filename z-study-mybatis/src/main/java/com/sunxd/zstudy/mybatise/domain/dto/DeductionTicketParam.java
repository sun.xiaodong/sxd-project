package com.sunxd.zstudy.mybatise.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fenglili
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeductionTicketParam {
    /**
     * 是否使用免费次数
     */
    private Boolean useFree;

    /**
     * 活动使用机会
     */
    private TicketParam ticketParam;
}
