package com.sunxd.zstudy.mybatise.domain.convert.activity;

import com.sunxd.zstudy.mybatise.out.bean.request.InteractiveActivityAddRequest;
import com.sunxd.zstudy.mybatise.domain.dto.InteractiveActivityAddDto;

public class InteractiveActivityDtoConvert {

    public static InteractiveActivityAddDto addReqToAddDto(InteractiveActivityAddRequest request){
        return InteractiveActivityAddDto.builder()
                .activityCode(request.getActivityCode())
                .activityName(request.getActivityName())
                .activityType(request.getActivityType())
                .beginTime(request.getBeginTime())
                .endTime(request.getEndTime())
                .content(request.getContent())
                .pageResource(request.getPageResource())
                .creator(request.getCreator())
                .updater(request.getUpdater())
                .build();
    }

}
