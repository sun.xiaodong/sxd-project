package com.sunxd.zstudy.mybatise.domain.contans.enums;

import com.sunxd.zstudy.mybatise.infrastructure.utils.StringUtils;
import lombok.Getter;

/**
 **/
@Getter
public enum RewardType {

    /**
     * 优惠券
     */
    COUPON("优惠券",true),

    /**
     * 积分
     */
    POINT("积分",true),

    /**
     * 活动机会
     */
    CAMPAIGN_TICKET("活动机会",true),

    /**
     * 权益
     */
    OFFLINE_BENEFIT("线下权益",true),


    /**
     * 实物商品
     */
    ITEM("实物商品",true),

    /**
     * 谢谢惠顾
     */
    THANKS("谢谢惠顾",false),


    ;
    private final String desc;
    private final Boolean prize;

    RewardType(String desc, Boolean prize) {
        this.desc= desc;
        this.prize = prize;
    }

    public static RewardType fromName(String name) {
        if (StringUtils.isNotBlank(name)) {
            for (RewardType value : RewardType.values()) {
                if (value.name().equalsIgnoreCase(name)) {
                    return value;
                }
            }
        }
        return null;
    }


}
