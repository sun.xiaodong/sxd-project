package com.sunxd.zstudy.mybatise.domain.contans.enums;

/**
 * Description: 条件类型
 * User: support 9
 * Date: 2018/7/16
 */
public enum ConditionType {
    VALUE,
    LOGIC,
    INVOKE
}
