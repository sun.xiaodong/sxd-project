//package com.sunxd.zstudy.mybatise.infrastructure.serialize;
//
//import com.alibaba.fastjson.JSON;
//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.databind.DeserializationContext;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
//import com.google.common.collect.Maps;
//import com.sunxd.zstudy.mybatise.application.facade.request.interactive.base.BaseInteractiveJoinRequest;
//import com.sunxd.zstudy.mybatise.domain.annotation.Capability;
//import com.sunxd.zstudy.mybatise.domain.exception.InternalServerException;
//import lombok.extern.slf4j.Slf4j;
//
//import java.io.IOException;
//import java.util.Map;
//import java.util.Set;
//import org.reflections.Reflections;
//
//import static com.sunxd.zstudy.mybatise.domain.contans.consts.InteractiveErrorCode.INTERNAL_SERVER_ERROR;
//import static java.util.Objects.isNull;
//
///**
// * @author wangxinhua
// * Date: 2020/11/30 5:11 下午
// * Description: 互动营销参与 参数多态解析器
// */
//@Slf4j
//public class InteractiveJoinRequestDeserializer
//        extends StdDeserializer<BaseInteractiveJoinRequest> {
//    private static final long serialVersionUID = -4127253164499946694L;
//    private final static String CAPABILITY_TYPE = "capabilityType";
//
//    private Map<String, BaseInteractiveJoinRequest> map = Maps.newHashMap();
//
//    public InteractiveJoinRequestDeserializer() {
//        this(null);
//    }
//
//    protected InteractiveJoinRequestDeserializer(Class<BaseInteractiveJoinRequest> vc) {
//        super(vc);
//        scan("io.terminus.sagittarius.api.bean.facade.request.write.interactive.join");
//    }
//
//    private void scan(String... paths) {
//        try {
//            for (String path : paths) {
//                // TODO path需要考虑二开扩展
//                Reflections reflections = new Reflections(path);
//                Set<Class<? extends BaseInteractiveJoinRequest>> classSet =
//                        reflections.getSubTypesOf(BaseInteractiveJoinRequest.class);
//                for (Class<? extends BaseInteractiveJoinRequest> clazz : classSet) {
//                    BaseInteractiveJoinRequest req = clazz.newInstance();
//                    Capability annotation = req.getClass().getAnnotation(Capability.class);
//                    if (annotation != null) {
//                        map.put(annotation.value(), req);
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public BaseInteractiveJoinRequest deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException {
//        JsonNode node = jp.getCodec().readTree(jp);
//        if (isNull(node.get(CAPABILITY_TYPE))) {
//            log.error("capability param missing node type");
//            throw new InternalServerException( INTERNAL_SERVER_ERROR);
//        }
//        String type = node.get(CAPABILITY_TYPE).asText();
//        BaseInteractiveJoinRequest instance = map.get(type);
//
//        if (instance == null) {
//            log.error("capability param type : {} not implemented instance", type);
//            throw new InternalServerException(INTERNAL_SERVER_ERROR);
//        }
//        return JSON.parseObject(node.toString(), instance.getClass());
//    }
//
//    public static void main(String[] args) {
//        Reflections reflections = new Reflections();
//        Set<Class<? extends BaseInteractiveJoinRequest>> subTypesOf = reflections.getSubTypesOf(BaseInteractiveJoinRequest.class);
//        System.out.println(subTypesOf);
//    }
//}
