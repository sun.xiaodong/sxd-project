package com.sunxd.zstudy.mybatise.infrastructure.adapter.repository.db.convert;

import com.sunxd.zstudy.mybatise.domain.contans.enums.InteractiveStatusEnum;
import com.sunxd.zstudy.mybatise.domain.contans.enums.AuditStatusEnum;
import com.sunxd.zstudy.mybatise.domain.contans.enums.DeleteFlagEnum;
import com.sunxd.zstudy.mybatise.domain.dto.InteractiveActivityAddDto;
import com.sunxd.zstudy.mybatise.infrastructure.adapter.repository.db.po.InteractiveActivityPO;
import com.sunxd.zstudy.mybatise.infrastructure.utils.StringUtils;

import java.util.Date;

public class InteractiveActivityPoConvert {

    public static InteractiveActivityPO addDtoToAddPo(InteractiveActivityAddDto request){
        Date date = new Date();
        return InteractiveActivityPO.builder()
                .uuid(StringUtils.buildUUID())
                .activityCode(request.getActivityCode())
                .activityName(request.getActivityName())
                .activityStatus(InteractiveStatusEnum.DRAFT.getValue())
                .activityType(request.getActivityType())
                .auditStatus(AuditStatusEnum.PASSED.getType())
                .beginTime(request.getBeginTime())
                .endTime(request.getEndTime())
                .content(request.getContent())
                .pageResource(request.getPageResource())
                .createTime(date)
                .creator(request.getCreator())
                .updateTime(date)
                .updater(request.getUpdater())
                .deleteFlag(DeleteFlagEnum.NOT_DELETE.getType())
                .build();
    }

}
