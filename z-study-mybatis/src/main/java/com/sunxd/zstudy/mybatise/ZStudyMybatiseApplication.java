package com.sunxd.zstudy.mybatise;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZStudyMybatiseApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZStudyMybatiseApplication.class, args);
    }


}
