package com.sunxd.zstudy.mybatise.out.controller;

import com.sunxd.zstudy.mybatise.application.facade.impl.InteractiveJoinWriteFacadeImpl;
import com.sunxd.zstudy.mybatise.application.facade.request.interactive.base.BaseInteractiveJoinRequest;
import com.sunxd.zstudy.mybatise.application.facade.response.interactive.base.BaseInteractiveJoinDetailInfo;
import com.sunxd.zstudy.mybatise.domain.service.MarketingInteractClient;
import com.sunxd.zstudy.mybatise.out.bean.response.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 */
@Slf4j
@Api(value = "互动营销活动api")
@RestController
@RequestMapping("/api/marketing/campaign/v1")
@AllArgsConstructor
public class InteractiveCampaignController {


    private final InteractiveJoinWriteFacadeImpl interactiveJoinWriteFacade;
    private final MarketingInteractClient marketingInteractClient;

    @PostMapping("/go")
    @ApiOperation("参与活动")
    public Response<BaseInteractiveJoinDetailInfo<?>> go(@RequestBody BaseInteractiveJoinRequest request) {
        String userId = null;
        String nickName = null;
        String avatar = null;
        return interactiveJoinWriteFacade.go(request);
    }


    @PostMapping("/go1")
    @ApiOperation("参与活动")
    public Response<BaseInteractiveJoinDetailInfo<?>> go1(@RequestBody BaseInteractiveJoinRequest request) {
        return marketingInteractClient.go(request,"userid");
    }


//    @PostMapping("/info")
//    @ApiOperation("查询活动明细")
//    public InteractiveCampaignDefDetailInfo info(@RequestBody InteractiveCampaignDefDetailRequest request) {
//        OwnerInfoContent ownerInfoContent = extraProcess.ownerInfoPerProcess();
//        request.setUserId(ownerInfoContent.getOwnerId());
//        Response<InteractiveCampaignDefDetailInfo> detail =
//                interactiveCampaignDefReadFacade.detail(request);
//        return or500(detail);
//    }


//    @PostMapping("/tickets")
//    @ApiOperation("剩余活动机会")
//    public InteractiveCampaignTicketsInfo tickets(@RequestBody InteractiveCampaignTicketsRequest request) {
//        OwnerInfoContent ownerInfoContent = extraProcess.ownerInfoPerProcess();
//        request.setUserId(ownerInfoContent.getOwnerId());
//        Response<InteractiveCampaignTicketsInfo> tickets = interactiveCampaignDefReadFacade.tickets(request);
//        return or500(tickets);
//    }



//    @PostMapping("/exchange-with-point")
//    @ApiOperation("消耗积分兑换活动机会")
//    public InteractiveCampaignExchangeTicketsInfo exchangeWithPoint(
//            @RequestBody InteractiveCampaignExchangeTicketsRequest request) {
//        OwnerInfoContent ownerInfoContent = extraProcess.ownerInfoPerProcess();
//        request.setUserId(ownerInfoContent.getOwnerId());
//        Response<InteractiveCampaignExchangeTicketsInfo> res =
//                campaignDefWriteFacade.exchangeWithPoint(request);
//        return or500(res);
//    }

//    @PostMapping("/page-config")
//    @ApiOperation("活动页面配置")
//    public InteractiveCampaignPageConfigInfo pageConfig(@RequestBody InteractiveCampaignPageConfigRequest request) {
//        Response<CampaignDefFindResultInfo> campaignOp = campaignDefReadFacade.findById(new CampaignDefFindByIdRequest(request.getCampaignId()));
//        if (!campaignOp.isSuccess()) {
//            log.error("campaignId: {} not exists", request.getCampaignId());
//            throw new InternalServerException(CommonErrorCode.INTERNAL_SERVER_ERROR);
//        }
//        CampaignDefFindResultInfo result = campaignOp.getResult();
//        return new InteractiveCampaignPageConfigInfo(request.getCampaignId(), result.getPageConfig());
//    }



}
