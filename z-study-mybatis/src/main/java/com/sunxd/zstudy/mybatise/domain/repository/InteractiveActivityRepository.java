package com.sunxd.zstudy.mybatise.domain.repository;

import com.sunxd.zstudy.mybatise.domain.dto.InteractiveActivityAddDto;

public interface InteractiveActivityRepository {

    boolean save(InteractiveActivityAddDto addDto);
}
