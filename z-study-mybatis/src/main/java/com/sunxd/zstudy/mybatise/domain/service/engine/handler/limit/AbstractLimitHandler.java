package com.sunxd.zstudy.mybatise.domain.service.engine.handler.limit;

import com.sunxd.zstudy.mybatise.domain.dto.LimitCondition;
import lombok.Data;

import java.util.Map;

/**
 * @author: sun.xd
 * @date: 2021-10-14 18:04
 **/
@Data
public abstract class AbstractLimitHandler {

    private AbstractLimitHandler nextHandler;

    public boolean handler(Map<String, LimitCondition> limit){
        // todo 添加找不到对应处理器的处理，或者加个默认的处理器
        if(canExecute(limit)){
            execute(limit);
        }else {
            nextHandler.handler(limit);
        }
        return false;
    }
    public abstract boolean execute(Map<String, LimitCondition> limit);

    public boolean canExecute(Map<String, LimitCondition> limit){
        if(limit.containsKey(getName())){
            LimitCondition limitCondition = limit.get(getName());
            return limitCondition.getIsOpen();
        }
        return Boolean.FALSE;
    }


    public abstract String getName();
}
