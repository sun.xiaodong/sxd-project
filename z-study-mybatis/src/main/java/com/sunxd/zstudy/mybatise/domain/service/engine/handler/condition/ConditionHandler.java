package com.sunxd.zstudy.mybatise.domain.service.engine.handler.condition;


import com.sunxd.zstudy.mybatise.domain.dto.Condition;
import com.sunxd.zstudy.mybatise.domain.dto.Context;

/**
 */
public interface ConditionHandler {

    /**
     * 比较表达式
     *
     * @param condition
     * @param context
     * @return
     */
    Boolean judge(Condition condition, Context context);

}
