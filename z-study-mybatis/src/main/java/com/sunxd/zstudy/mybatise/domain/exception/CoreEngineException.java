package com.sunxd.zstudy.mybatise.domain.exception;

/**
 */
public class CoreEngineException extends InternalServerException {


    private static final long serialVersionUID = 6550214592386015766L;

    public CoreEngineException(String code, String i18n, Object... args) {
        super(code, i18n);
    }
}
