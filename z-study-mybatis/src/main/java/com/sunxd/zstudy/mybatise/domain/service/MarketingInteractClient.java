package com.sunxd.zstudy.mybatise.domain.service;

import com.sunxd.zstudy.mybatise.application.facade.request.interactive.base.BaseInteractiveJoinRequest;
import com.sunxd.zstudy.mybatise.infrastructure.utils.RestTemplateUtil;
import com.sunxd.zstudy.mybatise.out.bean.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 互动营销 相关
 *
 * @author sun.xd
 */
@Slf4j
@Component
public class MarketingInteractClient {

    /**
     * 营销slb地址
     */
    private String slbMarketing = "http://localhost:20819";

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    private static final String PRE_FIX = "/api/marketing/interactive/web";
    private static final String GO = PRE_FIX + "/go";
    private static final String PAGE_CONFIG = PRE_FIX + "/page-config";
    private static final String INTERACT_CONFIG = PRE_FIX + "/interact-config";
    private static final String TICKET_SURPLUS = PRE_FIX + "/ticket/surplus";
    private static final String TICKET_ADD = PRE_FIX + "/ticket/add";


    public Response go(BaseInteractiveJoinRequest req, String userid) {
        req.setPassengerUuid(userid);
        return restTemplateUtil.post(slbMarketing + GO, req, Response.class);
    }

//    public Response pageConfig(PageConfigRequest request) {
//        return restTemplateUtil.post(slbMarketing + PAGE_CONFIG, request, Response.class);
//    }
//
//    public Response interactConfig(PageConfigRequest request) {
//        return restTemplateUtil.post(slbMarketing + INTERACT_CONFIG, request, Response.class);
//    }
//
//    public Response surplus(TicketQueryRequest request) {
//        return restTemplateUtil.post(slbMarketing + TICKET_SURPLUS, request, Response.class);
//    }
//
//    public Response addTickets(TicketCommandRequest request) {
//        return restTemplateUtil.post(slbMarketing + TICKET_ADD, request, Response.class);
//    }


}
