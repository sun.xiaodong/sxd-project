package com.sunxd.zstudy.mybatise.domain.service.context;

import com.sunxd.zstudy.mybatise.domain.dto.InteractiveActivity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InteractiveContextInfo {

    private InteractiveActivity interactiveActivity;


}
