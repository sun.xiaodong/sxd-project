package com.sunxd.zstudy.mybatise.application.facade.request.interactive.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Description:
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LotteryInteractiveJoinRequest extends BaseInteractiveJoinRequest {
    private static final long serialVersionUID = -6980497630292878840L;
}
