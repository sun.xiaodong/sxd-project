package com.sunxd.zstudy.mybatise.domain.convert.activity;

import com.google.common.collect.Lists;
import com.sunxd.zstudy.mybatise.application.facade.response.interactive.prize.PrizeActionInfo;
import com.sunxd.zstudy.mybatise.application.facade.response.interactive.prize.PrizeRecordInfo;
import com.sunxd.zstudy.mybatise.domain.dto.InteractiveRewardDetail;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
@AllArgsConstructor
public class LotteryConverter {



    public static PrizeActionInfo convertFromReward(InteractiveRewardDetail rewardDetail) {

        if (rewardDetail == null) {
            return new PrizeActionInfo(Collections.emptyList());
        }
        List<PrizeRecordInfo> prizeRecordInfos = Lists.newArrayList(PrizeRecordInfo.builder()
                .rewardId(rewardDetail.getId())
                .rewardName(rewardDetail.getRewardName())
                .rewardType(rewardDetail.getRewardType())
                .build());
        return new PrizeActionInfo(prizeRecordInfos);

    }

}
