package com.sunxd.zstudy.mybatise.domain.service.engine.handler.join.lottery;

import com.sunxd.zstudy.mybatise.domain.dto.InteractiveReward;
import com.sunxd.zstudy.mybatise.domain.dto.LotteryReward;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Component
public class LotteryProcessorDispatcher {

    private Map<String, IssueProcessor> prizeIssueProcessorMap;

    @Autowired
    public void setPrizeIssueProcessorMap(List<IssueProcessor> issueProcessors) {
        this.prizeIssueProcessorMap = issueProcessors.stream()
                .collect(Collectors.toMap(IssueProcessor::getIssueType, Function.identity()));
    }

    public LotteryReward lottery(InteractiveReward interactiveReward) {
        IssueProcessor issueProcessor = fromIssueType(interactiveReward.getPrizeIssueType().name());
        LotteryReward lotteryReward = issueProcessor.lottery(interactiveReward);
        // todo 奖品总体限制
        return lotteryReward;

    }


    private IssueProcessor fromIssueType(String issueType){
        return prizeIssueProcessorMap.get(issueType);
    }



}
