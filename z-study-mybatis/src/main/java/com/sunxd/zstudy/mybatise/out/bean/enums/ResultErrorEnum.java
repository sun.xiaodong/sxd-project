package com.sunxd.zstudy.mybatise.out.bean.enums;


/**
 * @author: caodongfeng
 * @date: 2019-09-27 10:23
 * @des: 营销活动操作记录组
 */
public enum ResultErrorEnum implements BaseEnumInterface {
    NEED_LOGIN(9999, "登录过期，请重新登录"),
    STAFF_NULL(9998, "人员信息不存在"),
    POSITION_NULL(9997, "当前用户未分配岗位信息"),
    AGENT_UUID_NULL(9996, "未设置运营商ID"),
    ACCOUNT_QUIT(9995, "账号已离职"),
    ACCOUNT_FREEZE(9994, "账号已冻结"),
    SYSTEM_ERROR(10000, "系统错误"),
    PARAM_NULL_ERROR(10001, "相关参数不能为空"),
    PARAM_FORAMT_ERROR(10002, "参数格式错误"),
    PARAM_TIME_ERROR(10003, "时间设置错误"),
    LOGIN_TIMEOUT(10004, "登录超时,请重新登录"),
    NONE_PERMISSION(10005, "无访问权限"),
    PASSWORD_IS_POOL(10006, "需修改初始密码"),
    INVALID_TOKEN(10007, "无效的认证请重新登录"),
    ACCOUNT_OR_PASSWORD_ERROR(500, "账号或者密码错误"),

    PAGE_NULL_ERROR(30002, "分页参数不能为空"),
    FACE_NULL_ERROR(30003, "头像不能为空"),
    NAME_NULL_ERROR(30004, "姓名不能为空"),
    SEX_NULL_ERROR(30005, "性别不能为空"),
    ID_CARD_NULL_ERROR(30007, "身份证号不能为空"),
    MOBILE_NULL_ERROR(30008, "手机号不能为空"),
    ENTRY_TIME_NULL_ERROR(30009, "入职时间不能为空"),
    ID_CARD_FACE_IMG_NULL_ERROR(0010, "身份证照人像面不能为空"),
    ID_CARD_BACK_IMG_NULL_ERROR(30011, "身份证照国徽面不能为空"),
    DRIVER_ID_CARD_IMG_NULL_ERROR(30012, "手持身份证照不能为空"),
    DRIVER_UUID_NULL_ERROR(30012, "司机uuid不能为空"),
    BLOCK_END_NULL_ERROR(30013, "解封时间不能为空"),
    BLOCK_TYPE_NULL_ERROR(30014, "封号类型不能为空"),
    OPERATE_REMARK_NULL_ERROR(30015, "操作备注不能为空"),

    DIMISSION_REMARK_NULL_ERROR(30016, "离职备注不能为空"),
    FILE_TYPE_ERROR(30017, "文件类型错误，请上传.jpg .png 等类型的图片文件"),
    FILE_NULL_ERROR(30018, "请选择文件"),

    LEAVA_NOTE_ID_NULL(30019, "请假ID不能为空"),
    BUSINESS_EMPTY_ERROR(30021, "至少选择一条业务线"),
    ORDER_TYPE_EMPTY_ERROR(30022, "至少选择一种订单类型"),
    ;
    private Integer code;

    private String msg;

    ResultErrorEnum(Integer errorCode, String msg) {
        this.code = errorCode;
        this.msg = msg;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

}


