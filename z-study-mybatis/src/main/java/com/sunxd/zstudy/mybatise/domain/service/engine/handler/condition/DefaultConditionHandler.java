package com.sunxd.zstudy.mybatise.domain.service.engine.handler.condition;

import com.sunxd.zstudy.mybatise.domain.annotation.Handler;
import com.sunxd.zstudy.mybatise.domain.contans.enums.ConditionType;
import com.sunxd.zstudy.mybatise.domain.contans.enums.OperatorType;
import com.sunxd.zstudy.mybatise.domain.dto.Condition;
import com.sunxd.zstudy.mybatise.domain.dto.Context;
import com.sunxd.zstudy.mybatise.domain.exception.CoreEngineException;
import com.sunxd.zstudy.mybatise.domain.service.engine.handler.helper.DefaultConditionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 */
@Component
@Handler(conditionType = ConditionType.VALUE, operatorType = OperatorType.DEFAULT)
public class DefaultConditionHandler implements ConditionHandler  {
    private final DefaultConditionHelper conditionHelper;

    @Autowired
    public DefaultConditionHandler(DefaultConditionHelper conditionHelper) {
        this.conditionHelper = conditionHelper;
    }

    @Override
    public Boolean judge(Condition condition, Context context) {
        if (!Objects.equals(condition.getOperator(), OperatorType.DEFAULT)) {
            throw new CoreEngineException("EqualsConditionHandler.typeNotMatch", "");
        }
        return conditionHelper.equal(condition.getRight(), condition.getLeft());
    }
}
