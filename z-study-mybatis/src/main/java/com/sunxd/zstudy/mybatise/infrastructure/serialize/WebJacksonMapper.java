//package com.sunxd.zstudy.mybatise.infrastructure.serialize;
//
//import com.fasterxml.jackson.databind.module.SimpleModule;
//import com.sunxd.zstudy.mybatise.application.facade.request.interactive.base.BaseInteractiveJoinRequest;
//import com.sunxd.zstudy.mybatise.infrastructure.utils.AbstractJacksonMapper;
//import lombok.AccessLevel;
//import lombok.NoArgsConstructor;
//
///**
// * @author wangxinhua
// * Date: 2020/11/30 5:08 下午
// * Description: 注册web端jackson解析器, 二开可以复写逻辑，增加自定义解析
// */
//@NoArgsConstructor(access = AccessLevel.PRIVATE)
//public class WebJacksonMapper extends AbstractJacksonMapper {
//
//    private static WebJacksonMapper INSTANCE = new WebJacksonMapper();
//
//    public static WebJacksonMapper getInstance() {
//        return INSTANCE;
//    }
//
//    @Override
//    protected void addDeserializers(SimpleModule module) {
//        module.addDeserializer(BaseInteractiveJoinRequest.class, new InteractiveJoinRequestDeserializer());
//    }
//}
