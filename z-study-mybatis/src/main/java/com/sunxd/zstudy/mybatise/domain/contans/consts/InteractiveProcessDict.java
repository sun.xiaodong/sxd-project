package com.sunxd.zstudy.mybatise.domain.contans.consts;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Description: 活动流程解析器字段，不使用枚举
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InteractiveProcessDict {

    /**
     * 活动
     */
    public static final String CAMPAIGN = "CAMPAIGN";


    /**
     * 奖品
     */
    public static final String PRIZE = "PRIZE";

    /**
     * 任务
     */
    public static final String TASK = "TASK";

    /**
     * 分享
     */
    public static final String SHARE = "SHARE";

    /**
     * 抽奖
     */
    public static final String LOTTERY = "LOTTERY";

    /**
     * 裂变
     */
    public static final String FISSION = "FISSION";

    /**
     * 投票
     */
    public static final String VOTE = "VOTE";

    /**
     * 投票
     */
    public static final String SIGN_VOTE = "SIGN_VOTE";


    /**
     * 问卷
     */
    public static final String QUESTIONNAIRE = "QUESTIONNAIRE";

    /**
     * 打卡
     */
    public static final String CLOCK_IN = "CLOCK_IN";

    /**
     * 答题
     */
    public static final String ANSWER = "ANSWER";

    /**
     * 签到
     */
    public static final String SIGN_IN = "SIGN_IN";

    /**
     * 报名
     */
    public static final String SIGN_UP = "SIGN_UP";


}
