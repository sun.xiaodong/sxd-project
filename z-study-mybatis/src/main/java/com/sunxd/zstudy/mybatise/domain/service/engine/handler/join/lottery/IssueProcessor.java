package com.sunxd.zstudy.mybatise.domain.service.engine.handler.join.lottery;

import com.sunxd.zstudy.mybatise.domain.dto.InteractiveReward;
import com.sunxd.zstudy.mybatise.domain.dto.LotteryReward;

public interface IssueProcessor {
    /**
     * 获取派奖类型
     * @return 派奖类型
     */
    String getIssueType();

    /**
     * 抽奖
     */
    LotteryReward lottery(InteractiveReward interactiveReward);

}
