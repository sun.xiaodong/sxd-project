package com.sunxd.zstudy.mybatise.domain.contans.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @ClassName ActivityStatusEnum  <br>
 * @Description 活动状态枚举值   <br>
 * @Author jiaLe.liu  <br>
 * @Date 2021/12/15 17:57 <br>
 * @Version 1.0      <br>
 **/
@Getter
@AllArgsConstructor
public enum InteractiveStatusEnum {
    /**
     * 活动状态枚举
     */
    DRAFT(1, "草稿"),
    NOT_STARTED(2, "待生效"),
    ENABLE_STATUS(3, "生效中"),
    ALREADY_END_STATUS(4, "已结束"),
    ALREADY_STOP_STATUS(5, "已停用"),
    ;

    /**
     * 值
     */
    private final Integer value;
    /**
     * 名
     */
    private final String title;

    public static InteractiveStatusEnum match(int value) {
        for (InteractiveStatusEnum item : InteractiveStatusEnum.values()) {
            if (item.getValue() == value) {
                return item;
            }
        }
        return null;
    }
}
