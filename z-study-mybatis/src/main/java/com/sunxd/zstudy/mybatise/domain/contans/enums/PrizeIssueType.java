package com.sunxd.zstudy.mybatise.domain.contans.enums;

public enum PrizeIssueType {

    /**
     * 按照概率派奖
     */
    PROBABILITY,
    /**
     * 按照人数派奖
     */
    OWNER_COUNT,
    /**
     * 按照时间派奖
     */
    TIME;
}
