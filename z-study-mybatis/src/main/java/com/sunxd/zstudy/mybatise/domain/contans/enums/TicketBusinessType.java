package com.sunxd.zstudy.mybatise.domain.contans.enums;

/**
 * @author Yangxiumei
 * Date: 2020-11-26 16:28
 * Description: 机会业务类型
 **/
public enum TicketBusinessType {

    /**
     * 奖品赠送
     */
    PRIZE_GIVE_AWAY,

    /**
     * 购买
     */
    BUY,

    /**
     * 使用
     */
    USE,


}
