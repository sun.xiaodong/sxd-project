package com.sunxd.zstudy.mybatise.domain.contans.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @description 审核状态枚举
 * <br/>
 * @author biebf
 * @date 2021/12/28
 */
@Getter
@AllArgsConstructor
public enum AuditStatusEnum {
    /**
     * 审核状态标识
     */
    NOT_AUDIT(1, "待审核"),
    PASSED(2, "已通过"),
    REJECTED(3, "已驳回"),
    ;

    /**
     * 标识值
     */
    private final Integer type;
    /**
     * 名称
     */
    private final String name;

    public static AuditStatusEnum match(Integer type) {
        for (AuditStatusEnum instance : values()) {
            if (instance.getType().equals(type)) {
                return instance;
            }
        }
        throw new IllegalArgumentException("unexpected AuditStatusEnum value: " + type);
    }
}
