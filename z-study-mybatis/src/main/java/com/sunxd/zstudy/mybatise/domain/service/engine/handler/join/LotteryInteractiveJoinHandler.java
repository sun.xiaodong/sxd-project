package com.sunxd.zstudy.mybatise.domain.service.engine.handler.join;

import com.google.common.collect.Lists;
import com.sunxd.zstudy.mybatise.application.facade.request.interactive.base.LotteryInteractiveJoinRequest;
import com.sunxd.zstudy.mybatise.application.facade.response.interactive.base.BaseInteractiveJoinDetailInfo;
import com.sunxd.zstudy.mybatise.application.facade.response.interactive.prize.PrizeActionInfo;
import com.sunxd.zstudy.mybatise.application.facade.response.interactive.prize.PrizeRecordInfo;
import com.sunxd.zstudy.mybatise.domain.annotation.Capability;
import com.sunxd.zstudy.mybatise.domain.contans.consts.InteractiveErrorCode;
import com.sunxd.zstudy.mybatise.domain.contans.consts.InteractiveProcessDict;
import com.sunxd.zstudy.mybatise.domain.contans.enums.RewardType;
import com.sunxd.zstudy.mybatise.domain.convert.activity.LotteryConverter;
import com.sunxd.zstudy.mybatise.domain.dto.InteractiveReward;
import com.sunxd.zstudy.mybatise.domain.dto.InteractiveRewardDetail;
import com.sunxd.zstudy.mybatise.domain.dto.LotteryReward;
import com.sunxd.zstudy.mybatise.domain.exception.InternalServerException;
import com.sunxd.zstudy.mybatise.domain.service.context.InteractiveContextInfo;
import com.sunxd.zstudy.mybatise.domain.service.engine.handler.join.lottery.LotteryProcessorDispatcher;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


/**
 * Description:
 */
@Component
@AllArgsConstructor
@Capability(InteractiveProcessDict.LOTTERY)
public class LotteryInteractiveJoinHandler
        extends BaseInteractiveJoinHandler<LotteryInteractiveJoinRequest, BaseInteractiveJoinDetailInfo<PrizeActionInfo>> {

    private final LotteryProcessorDispatcher lotteryProcessorDispatcher;


    @Override
    public @Nullable BaseInteractiveJoinDetailInfo<PrizeActionInfo> join(LotteryInteractiveJoinRequest request, InteractiveContextInfo contextInfo) {

        InteractiveReward interactiveReward = contextInfo.getInteractiveActivity().getInteractiveReward();
        if (Objects.isNull(interactiveReward) || CollectionUtils.isEmpty(interactiveReward.getInteractiveRewardDetails())) {
            throw new InternalServerException(InteractiveErrorCode.INTERACTIVE_REWARD_NOT_EXIST);
        }
        LotteryReward lotteryPrize = lotteryProcessorDispatcher.lottery(interactiveReward);
        List<PrizeActionInfo> prizeActionInfos = Lists.newArrayList();
        if (lotteryPrize != null ) {
            InteractiveRewardDetail reward = lotteryPrize.getReward();
            if (Objects.nonNull(lotteryPrize.getReward()) && !Objects.equals(reward.getRewardType(),RewardType.THANKS.name())) {
                prizeActionInfos = dispatchPrize(reward, contextInfo);
            }

        // 奖品发完了/没有匹配到奖品，走保底奖品
        if (CollectionUtils.isEmpty(prizeActionInfos)) {
            prizeActionInfos = dispatchPrize(lotteryPrize.getThanks(),contextInfo);
        }
        // 奖品发完了/没有匹配到奖品，走保底奖品
        if (CollectionUtils.isEmpty(prizeActionInfos)) {
            prizeActionInfos = dispatchThanks(lotteryPrize.getThanks());
        }
        }
        return new BaseInteractiveJoinDetailInfo<>(prizeActionInfos);
    }

    private List<PrizeActionInfo> dispatchPrize(InteractiveRewardDetail rewardDetail, InteractiveContextInfo contextInfo) {

        Boolean stockDeductionResult = Boolean.TRUE;
        if(!Objects.equals(rewardDetail.getRewardType(),RewardType.THANKS.name())){
            // todo 扣减redis库存 是否支持lua脚本
        }
        if (stockDeductionResult) {
            try {
                // 插入奖品记录--状态设置为发送中
                // 发奖
//                prizeInvoke.dispatchPrize(LotteryConverter.convertFromReward(rewardDetail));
                return Lists.newArrayList(LotteryConverter.convertFromReward(rewardDetail));
            } catch (Exception e) {
                // todo 出现异常，直接回滚库存
                throw new InternalServerException(e.getMessage());
            }
        }
        return Collections.emptyList();
    }


    private List<PrizeActionInfo> dispatchThanks(InteractiveRewardDetail lotteryPrize) {
        lotteryPrize.setRewardType(RewardType.THANKS.name());
        List<PrizeRecordInfo> prizeRecordInfos = Lists.newArrayList(PrizeRecordInfo.builder()
                .rewardId(lotteryPrize.getId())
                .rewardName(lotteryPrize.getRewardName())
                .rewardType(lotteryPrize.getRewardType())
                .build());
        return Lists.newArrayList(new PrizeActionInfo(prizeRecordInfos));
    }

}
