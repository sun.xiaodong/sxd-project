package com.sunxd.zstudy.mybatise.domain.service.engine.handler.limit;

import com.sunxd.zstudy.mybatise.domain.annotation.LimitHandler;
import com.sunxd.zstudy.mybatise.domain.contans.consts.InteractiveJoinLimitDetailInfo;
import com.sunxd.zstudy.mybatise.domain.contans.consts.InteractiveJoinLimitType;
import com.sunxd.zstudy.mybatise.domain.dto.LimitCondition;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Order(1)
@LimitHandler(value = InteractiveJoinLimitDetailInfo.JOIN_CITY,group = InteractiveJoinLimitType.THRESHOLD)
public class ThresholdJoinCityLimitHandler extends AbstractLimitHandler {
    @Override
    public boolean execute(Map<String, LimitCondition> limit) {
        return false;
    }

    @Override
    public String getName() {
        return InteractiveJoinLimitDetailInfo.JOIN_CITY;
    }

}
