package com.sunxd.zstudy.mybatise.domain.service.engine.handler.helper;


/**
 * Description: 条件处理帮助
 * Author: support 9
 * Date: 2018/07/02
 */
public interface ConditionHelper {

    /**
     * 大于
     */
    Boolean greater(Object right, Object left);

    /**
     * 等于
     */
    Boolean equal(Object right, Object left);

    /**
     * 在。。。中
     */
    Boolean in(Object right, Object left);

    /**
     * 大于或等于
     */
    default Boolean greaterOrEquals(Object right, Object left) {
        return greater(right, left) || equal(right, left);
    }

    /**
     * 小于或等于
     */
    default Boolean lessOrEquals(Object right, Object left) {
        return !greater(right, left);
    }

    /**
     * 小于
     */
    default Boolean less(Object right, Object left) {
        return !greater(right, left) && !equal(right, left);
    }

    String getClassType();

}
