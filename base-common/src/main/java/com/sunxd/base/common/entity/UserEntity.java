package com.sunxd.base.common.entity;

import lombok.*;

import java.io.Serializable;

/**
 * @author: 作者名称
 * @date: 2021-05-07 11:19
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity implements Serializable {
    private static final long serialVersionUID = -2199760688069950438L;

    private Long id;
    private String name;
    private Integer age;

}
