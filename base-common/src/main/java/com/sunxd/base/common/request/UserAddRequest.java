package com.sunxd.base.common.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: 作者名称
 * @date: 2021-05-07 23:16
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAddRequest  implements Serializable {

    private static final long serialVersionUID = -8587014652006241641L;
    private String name;
    private Integer age;
}
