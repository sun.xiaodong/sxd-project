package com.sunxd.batch.writer;

import com.sunxd.batch.po.BlogInfo;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author: sun.xd
 * @date: 2021-11-01 18:44
 **/
@Component
public class BlogWriter implements ItemWriter<BlogInfo> {
    @Override
    public void write(List<? extends BlogInfo> list) throws Exception {
        System.out.println("writer begin ...");
        list.forEach(x -> System.out.println(x.toString()));
        System.out.println("writer end ...");
    }
}
