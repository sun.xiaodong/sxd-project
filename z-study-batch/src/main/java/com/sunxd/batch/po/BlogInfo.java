package com.sunxd.batch.po;

import lombok.Data;

/**
 * @author: sun.xd
 * @date: 2021-11-01 17:38
 **/

@Data
public class BlogInfo {

    private Integer id;
    private String blogAuthor;
    private String blogUrl;
    private String blogTitle;
    private String blogItem;

    public BlogInfo(Integer id, String blogAuthor, String blogUrl) {
        this.id = id;
        this.blogAuthor = blogAuthor;
        this.blogUrl = blogUrl;
    }

    @Override
    public String toString() {
        return "BlogInfo{" +
                "id=" + id +
                ", blogAuthor='" + blogAuthor + '\'' +
                ", blogUrl='" + blogUrl + '\'' +
                ", blogTitle='" + blogTitle + '\'' +
                ", blogItem='" + blogItem + '\'' +
                '}';
    }
}
