package com.sunxd.batch.listenner;

import com.sunxd.batch.po.BlogInfo;

import javax.batch.api.chunk.listener.ItemProcessListener;
import javax.batch.api.chunk.listener.ItemReadListener;

/**
 * @version :1.0.0
 * @author: term
 * @time: 2018-05-07 11:56
 * @description :
 */
public class BlogReadListener implements ItemReadListener {

    @Override
    public void beforeRead() throws Exception {

    }

    @Override
    public void afterRead(Object item) throws Exception {

    }

    @Override
    public void onReadError(Exception ex) throws Exception {

    }
}
