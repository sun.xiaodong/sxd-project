package com.sunxd.batch.listenner;

import com.sunxd.batch.po.BlogInfo;
import org.springframework.batch.core.ItemWriteListener;

import javax.batch.api.chunk.ItemWriter;
import javax.batch.api.chunk.listener.ItemProcessListener;
import java.awt.event.ItemListener;
import java.util.List;

/**
 * @version :1.0.0
 * @author: term
 * @time: 2018-05-07 11:56
 * @description :
 */
public class BlogWriterListener implements ItemWriteListener<BlogInfo> {


    @Override
    public void beforeWrite(List<? extends BlogInfo> list) {

    }

    @Override
    public void afterWrite(List<? extends BlogInfo> list) {

    }

    @Override
    public void onWriteError(Exception e, List<? extends BlogInfo> list) {

    }
}
