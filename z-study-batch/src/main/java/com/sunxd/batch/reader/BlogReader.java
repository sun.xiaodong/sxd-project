package com.sunxd.batch.reader;

import com.sunxd.batch.po.BlogInfo;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.database.AbstractPagingItemReader;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * @author: sun.xd
 * @date: 2021-11-01 17:53
 **/
@Component
@Slf4j
public class BlogReader extends AbstractPagingItemReader<BlogInfo> {

    int i;

    @Override
    protected void doReadPage() {
        results = new ArrayList<>();
        if(i == 5){
            return;
        }
        i = i++;
        for (int j = 0; j < getPageSize(); j++) {
            BlogInfo blogInfo = new BlogInfo(j,String.format("author ",j),String.format("url ",j));
            results.add(blogInfo);
        }
    }

    @Override
    protected void doJumpToPage(int i) {

    }
}
