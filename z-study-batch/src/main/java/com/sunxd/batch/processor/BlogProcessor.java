package com.sunxd.batch.processor;

import com.sunxd.batch.po.BlogInfo;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

/**
 * @author: sun.xd
 * @date: 2021-11-01 18:09
 **/
@Component
public class BlogProcessor implements ItemProcessor<BlogInfo,BlogInfo> {
    @Override
    public BlogInfo process(BlogInfo blogInfo) throws Exception {
        System.out.println(blogInfo.toString());
        return blogInfo;
    }
}
