package com.sunxd.znginxdemo2.Demo;

import com.sunxd.znginxdemo2.Demo.content.BaseContent;
import com.sunxd.znginxdemo2.Demo.content.TaskContent;
import com.sunxd.znginxdemo2.Demo.limit.task.AbstractTaskLimitProcessor;
import com.sunxd.znginxdemo2.Demo.model.Activity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/9/27 19:21
 */
@Slf4j
@RequiredArgsConstructor
public abstract class TestJoin {


    // 使用
    private final LimitDelegate<TaskContent> limitDelegate;

    public void test(){
        TaskContent taskContent = new TaskContent();
        taskContent.setLimitType("TASK");
        limitDelegate.doExecute(taskContent);
    }


//    private static final Map<String, AbstractLimitHandler> checkMap = new HashMap<>();


    public static void main(String[] args) {
        Activity activity = new Activity();
        if(activity.getStartTime().getTime() > System.currentTimeMillis()
                || System.currentTimeMillis() > activity.getEndTime() .getTime()){
            log.info("活动未开始、已结束");
        }
        int count = 0;
        if(count >= activity.getDayCount()){
            log.info("超出每天最大的参与次数");
        }
        if(count >= activity.getTotalCount()){
            log.info("超出总参与次数");
        }
        sendReward("reward");

//        LimitDelegate<TaskContent> limitDelegate = new LimitDelegate<TaskContent>();
//        limitDelegate.doExecute(new TaskContent());

//        Activity activity = new Activity();
//        Iterator<Map.Entry<String, AbstractTaskLimitProcessor>> iterator = checkMap.entrySet().iterator();
//        while (iterator.hasNext()){
//            iterator.next().getValue().check();
//        }
//        // 。。。。 其他的限制条件
//        sendReward(activity.getReward());
    }
    public static void sendReward(String reward){
    }


}
