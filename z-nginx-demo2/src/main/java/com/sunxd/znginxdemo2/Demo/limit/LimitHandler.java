package com.sunxd.znginxdemo2.Demo.limit;

import com.sunxd.znginxdemo2.Demo.content.BaseContent;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/9/28 9:41
 */
public interface LimitHandler<T extends BaseContent> {

    String getKey();

    Function<T,Boolean> check();
}
