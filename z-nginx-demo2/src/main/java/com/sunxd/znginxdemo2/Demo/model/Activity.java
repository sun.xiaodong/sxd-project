package com.sunxd.znginxdemo2.Demo.model;

import lombok.Data;

import java.util.Date;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/9/27 19:21
 */
@Data
public class Activity {

    private Date startTime;

    private Date endTime;

    private Integer dayCount;
    private Integer totalCount;

    private String reward;
}
