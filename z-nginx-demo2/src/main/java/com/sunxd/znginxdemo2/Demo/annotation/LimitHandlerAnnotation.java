package com.sunxd.znginxdemo2.Demo.annotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description: 标记能力处理器, 用于能力分发
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface LimitHandlerAnnotation {

    /**
     * @return 能力域键
     */
    String value();

    String group();
}
