package com.sunxd.znginxdemo2.Demo.limit.task;

import com.sunxd.znginxdemo2.Demo.content.BaseContent;
import com.sunxd.znginxdemo2.Demo.limit.AbstractLimitStrategy;
import com.sunxd.znginxdemo2.Demo.limit.LimitHandler;
import lombok.Data;

/**
 * 自己父类，定义公共的东西
 * @author sun.xd
 * @description: description
 * @date 2022/9/27 19:53
 */
@Data
public abstract class AbstractTaskLimitProcessor<T extends BaseContent> extends AbstractLimitStrategy<T> {


}
