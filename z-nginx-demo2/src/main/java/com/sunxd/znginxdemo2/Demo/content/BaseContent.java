package com.sunxd.znginxdemo2.Demo.content;

import lombok.Data;
import org.springframework.boot.autoconfigure.web.ResourceProperties;

import java.util.List;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/9/28 9:50
 */
@Data
public class BaseContent {

    private List<Rule> ruleList;

    private String curCity;

    /**
     * 校验什么东西（活动维度，奖品维度，奖品明细维度）
     */
    private String limitType;

    @Data
    public static class Rule{
        private String key;
        private String value;
    }

}
