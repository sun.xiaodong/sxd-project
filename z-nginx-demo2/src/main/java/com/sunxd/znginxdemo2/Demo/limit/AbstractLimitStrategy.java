package com.sunxd.znginxdemo2.Demo.limit;

import com.sunxd.znginxdemo2.Demo.content.BaseContent;
import lombok.Data;

import java.util.Objects;

/**
 * 策略类，执行不同的策略
 * @param <T> t
 */
@Data
public abstract class AbstractLimitStrategy<T extends BaseContent> implements LimitHandler<T> {

    private AbstractLimitStrategy<T> nextHandler;

    /**
     *
     * @param baseContent baseContent
     */
    public Boolean doCheck(T baseContent) {
        if (!check().apply(baseContent)) {
            return Boolean.FALSE;
        }
        if (Objects.nonNull(nextHandler)) {
            if (!nextHandler.doCheck(baseContent)) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }


}
