package com.sunxd.znginxdemo2.Demo.limit.task;

import com.sunxd.znginxdemo2.Demo.annotation.LimitHandlerAnnotation;
import com.sunxd.znginxdemo2.Demo.content.TaskContent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * 领取城市校验
 */
@Slf4j
@Component
@Order(2)
//@LimitHandlerAnnotation(value = LimitConditionKeys.J_CITY_LIMIT, group = LimitConditionGroup.MATCH_TASK)
@LimitHandlerAnnotation(value = "cityLimit", group = "task")
public class CityLimitProcessor extends AbstractTaskLimitProcessor<TaskContent> {

    @Override
    public String getKey() {
//        return LimitConditionKeys.J_CITY_LIMIT;
        return "cityLimit";
    }

    @Override
    public Function<TaskContent,Boolean> check() {
        return (taskContent) -> Boolean.TRUE;
    }
//
//    @Override
//    protected Boolean execute(TaskContent baseContent) {
//        return null;
//    }
}
