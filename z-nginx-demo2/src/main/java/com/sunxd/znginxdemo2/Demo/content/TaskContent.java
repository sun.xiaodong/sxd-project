package com.sunxd.znginxdemo2.Demo.content;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/9/28 9:50
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TaskContent extends BaseContent{

    private String v1;

}
