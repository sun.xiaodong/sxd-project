package com.sunxd.znginxdemo2.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: 作者名称
 * @date: 2021-04-16 11:03
 **/
@RestController
@RequestMapping("z-nginx")
public class Demo2Controller {

    @RequestMapping("demo")
    public String zuul(){
        return "demo2";
    }

}
