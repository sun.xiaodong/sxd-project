package com.sunxd.znginxdemo2.controller;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.sunxd.znginxdemo2.controller.request.TestReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



@RestController
@RequestMapping("/api/wallet")
public class HandlerController {

    @PostMapping("accountInfo")
    public String accountInfo(HttpServletRequest request, @RequestBody TestReq testReq){
        System.out.println(request.getHeader("token"));

        List<Map<String,Object>> books = new ArrayList<>();
        Map<String,Object> book1 = new HashMap<>();
        book1.put("accountId",1);
        book1.put("availableBalance",11);
        book1.put("bookStatus",111);
        book1.put("bookType",1);
        book1.put("id",1);
        book1.put("remark","remark");
        Map<String,Object> book2 = new HashMap<>();
        book2.put("accountId",2);
        book2.put("availableBalance",22);
        book2.put("bookStatus",222);
        book2.put("bookType",2);
        book2.put("id",2);
        book2.put("remark","remark");
        books.add(book1);
        books.add(book2);

        Map<String,Object> date = new HashMap<>();
        date.put("accountCash",11);
        date.put("accountDesc","1234");
        date.put("accountGift",33);
        date.put("accountId",44);
        date.put("accountNewId",55);
        date.put("books",books);
        Map<String,Object> map = new HashMap<>();
        map.put("bizCode","bizCode");
        map.put("code",123);
        map.put("success",Boolean.TRUE);
        map.put("data",date);
        return JSON.toJSONString(map);
    }

}
