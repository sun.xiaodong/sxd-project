package com.sunxd.znginxdemo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZNginxDemo2Application {

    public static void main(String[] args) {
        SpringApplication.run(ZNginxDemo2Application.class, args);
    }

}
