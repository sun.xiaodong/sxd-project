package com.sunxd.zredis.demo1;

import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class ZRedisDemo1ApplicationTests {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    void contextLoads() throws InterruptedException {
        String key = "fff";
//        if (StringUtils.isNotBlank(stringRedisTemplate.opsForValue().getAndSet(key,"1"))) {
//            System.out.println("路径不存在");
//        }
//        long time = System.currentTimeMillis() - new Date(1627026767000L).getTime();
//        stringRedisTemplate.expire(key,time, TimeUnit.MILLISECONDS);
//        if (StringUtils.isNotBlank(stringRedisTemplate.opsForValue().getAndSet(key,"2"))) {
//            System.out.println("路径不存在");
//        }
//        System.out.println("结束");

        long time = new Date(1627026767000L).getTime() -System.currentTimeMillis() ;
        String s = stringRedisTemplate.opsForValue().getAndSet(key,"1");
        if (StringUtils.isNotBlank(s)) {
            System.out.println("不存在1");
        }
        System.out.println("111111");
        stringRedisTemplate.expire(key,time, TimeUnit.MILLISECONDS);

        Thread.sleep(2000);

        s = stringRedisTemplate.opsForValue().getAndSet(key,"2");
        if (StringUtils.isNotBlank(s)) {
            System.out.println("不存在2");
        }
        System.out.println("22222");
        stringRedisTemplate.expire(key,time, TimeUnit.MILLISECONDS);
    }

}
