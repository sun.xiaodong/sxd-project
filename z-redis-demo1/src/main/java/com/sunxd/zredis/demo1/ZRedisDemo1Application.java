package com.sunxd.zredis.demo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ZRedisDemo1Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(ZRedisDemo1Application.class, args);

    }

}
