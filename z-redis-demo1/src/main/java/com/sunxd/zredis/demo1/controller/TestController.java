package com.sunxd.zredis.demo1.controller;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sunxd.zredis.demo1.MyPeople;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.hash.Jackson2HashMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author: 作者名称
 * @date: 2021-05-31 18:47
 **/
@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestController {

    private final StringRedisTemplate stringRedisTemplate;
    private final RedisTemplate redisTemplate;
    private final ObjectMapper objectMapper;


    @RequestMapping("/stringRedisTemplate/get")
    public void stringRedisTemplateGet(@RequestParam String k1){
        System.out.println(stringRedisTemplate.opsForValue().get(k1));
    }


    @RequestMapping("redisTemplate/get")
    public void redisTemplateGet(@RequestParam String k1){
        System.out.println(redisTemplate.opsForValue().get(k1));
    }

    @RequestMapping("stringRedisTemplate/add")
    public void stringRedisTemplateAdd(@RequestParam String k1,@RequestParam String value){
        stringRedisTemplate.opsForValue().set(k1,value);
        System.out.println(stringRedisTemplate.opsForValue().get(k1));
    }

    @RequestMapping("redisTemplate/add")
    public void redisTemplateAdd(@RequestParam String k1,@RequestParam String value){
        redisTemplate.opsForValue().set(k1,value);
        System.out.println(redisTemplate.opsForValue().get(k1));
    }

    // ============================  redisTemplate 自动加了序列化  ============================




    @RequestMapping("redisTemplate/hash")
    public void redisTemplateHash(@RequestParam String key,@RequestParam String name ){
        MyPeople people = new MyPeople();
        people.setMyName(name);
//                .age(age)
        Jackson2HashMapper jm = new Jackson2HashMapper(true);
        redisTemplate.opsForHash().putAll(key, jm.toHash(people));

        Map entries = redisTemplate.opsForHash().entries(key);
        MyPeople p = objectMapper.convertValue(entries, MyPeople.class);
        System.out.println(p.getAge());

    }

    @RequestMapping("stringRedisTemplate/hash")
    public void stringRedisTemplateHash(@RequestParam String key,@RequestParam String name ) {
        MyPeople people = new MyPeople();
        people.setMyName(name);
//                .age(age)
        Jackson2HashMapper jm = new Jackson2HashMapper(true);
        stringRedisTemplate.opsForHash().putAll(key, jm.toHash(people));

        Map entries = stringRedisTemplate.opsForHash().entries(key);
        MyPeople p = objectMapper.convertValue(entries, MyPeople.class);
        System.out.println(p.getAge());

    }

    @RequestMapping("stringRedisTemplate/addSet")
    public void stringRedisTemplateSet() {
        for (int i = 0; i < 10; i++) {
            stringRedisTemplate.opsForSet().add("abc:def:ghe",i+"1",i+"2",i+"3",i+"4",i+"5",i+"6");
        }
        stringRedisTemplate.expire("abc:def:ghe",60*60L, TimeUnit.SECONDS);
    }

    @RequestMapping("stringRedisTemplate/getPipeline")
    public void stringRedisTemplateGetPipeline() {

        List<String> ab = stringRedisTemplate.opsForSet().pop("abc:def:ghe", 10);
        ab.stream().forEach(System.out::println);

    }



}
