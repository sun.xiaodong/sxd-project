package com.sunxd.zredis.demo1;

import lombok.*;
import org.springframework.beans.factory.annotation.Required;

import java.io.Serializable;

/**
 * @author: 作者名称
 * @date: 2021-05-31 19:35
 **/
@Data
public class MyPeople implements Serializable {

    private static final long serialVersionUID = 6927009276510461683L;

    private String myName;

    private Integer age;

}
