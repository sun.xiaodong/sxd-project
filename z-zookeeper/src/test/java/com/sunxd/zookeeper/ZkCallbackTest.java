package com.sunxd.zookeeper;

import com.sunxd.zookeeper.use.configCenter.SystemConfig;
import com.sunxd.zookeeper.use.callback.YewWatchCallBack;
import com.sunxd.zookeeper.use.callback.ZkCallbackConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author: 作者名称
 * @date: 2021-06-18 23:26
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@RequiredArgsConstructor
@Slf4j
public class ZkCallbackTest {

    @Autowired
    private ZkCallbackConfig zkCallbackConfig;

    @Test
    public void test1() throws InterruptedException {
        // 1 节点不存在
        SystemConfig systemConfig = new SystemConfig();
        YewWatchCallBack yewAWatchCallBack = new YewWatchCallBack();
        yewAWatchCallBack.setZooKeeper(zkCallbackConfig.zooKeeperCallback());
        yewAWatchCallBack.setSystemConfig(systemConfig);
        yewAWatchCallBack.setChildrenPath("/abc");
        yewAWatchCallBack.aWait();
        while (true){
            Thread.sleep(1000);
        }
    }
}
