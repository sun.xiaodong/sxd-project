package com.sunxd.zookeeper;

/**
 * @author: 作者名称
 * @date: 2021-06-28 02:28
 **/

import com.sunxd.zookeeper.use.ZkMyselfUtil;
import com.sunxd.zookeeper.use.configCenter.ListenUtil;
import com.sunxd.zookeeper.use.lock.ZkLockUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author: 作者名称
 * @date: 2021-06-18 23:26
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@RequiredArgsConstructor
@Slf4j
public class ZkLockTests {

    @Autowired
    private ZkLockUtil zkLockUtil;


    @Test
    public void testConnected() throws InterruptedException {
        zkLockUtil.excute("/lock","lockData");
    }


}
