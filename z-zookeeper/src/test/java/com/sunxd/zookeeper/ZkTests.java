package com.sunxd.zookeeper;

import com.alibaba.fastjson.JSON;
import com.sunxd.zookeeper.use.common.ZkData;
import com.sunxd.zookeeper.use.common.ZkUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.AsyncCallback;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.data.Stat;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@RequiredArgsConstructor
@Slf4j
class ZkTests {

    @Autowired
    private ZkUtil zkUtil;

    @Test
    void create() {
        zkUtil.createP("/abc", "abc");
        ZkData zkData = zkUtil.getData("/abc", watchedEvent -> {
        });
        log.info(" +++++++ zkData getData :{} " + JSON.toJSONString(zkData));
    }

    @Test
    void testWatcher() {
//        zkUtil.createP("/abc","abc");
        ZkData zkData = zkUtil.getData("/abc", watchedEvent -> {
            // NodeDataChanged
            log.info("+++++++ zkData watcher event:{}" + JSON.toJSONString(watchedEvent));
        });
        zkUtil.setData("/abc", "bcd");
    }

    /**
     * watch 是一次性的 ，2次set，只执行一次
     */
    @Test
    void testWatcher1() {
//        zkUtil.createP("/abc","abc");
        zkUtil.getData("/abc", watchedEvent -> {
            // NodeDataChanged
            log.info("+++++++ zkData watcher event:{}" + JSON.toJSONString(watchedEvent));
        });
        zkUtil.setData("/abc", "bcd");
        zkUtil.setData("/abc", "def");

        ZkData zkData = zkUtil.getWatchData("/abc", false);
        log.info("++++ getData /abc data:{}" + JSON.toJSONString(zkData));
    }

    /**
     * 循环监听 watch 走zkclientconfig的watcher
     */
    @Test
    void testWatcher2() {
        zkUtil.getData("/abc", watchedEvent -> {
            log.info("+++++++ zkData watcher event:{}" + JSON.toJSONString(watchedEvent));
            //如果此时的 为true,则注册的是zkClinetConfig的watch, 会看到2次 zk connected path : null 日志
            zkUtil.getWatchData("/abc", true);
        });
        zkUtil.setData("/abc", "bcd");
        zkUtil.setData("/abc", "def");

        ZkData zkData = zkUtil.getWatchData("/abc", true);

        log.info("++++ getData /abc data:{}" + JSON.toJSONString(zkData));
    }

    /**
     * 循环监听 watch 走自己的watcher
     */
    @Test
    void testWatcher3() throws InterruptedException {
        zkUtil.getData("/abc", new Watcher() {
            @Override
            public void process(WatchedEvent watchedEvent) {
                log.info("+++++++ zkData watcher event:{}" + JSON.toJSONString(watchedEvent));
                //如果此时的 为true,则注册的是zkClinetConfig的watch, 会看到2次 zk connected path : null 日志
                zkUtil.getData("/abc", this);
            }
        });
        zkUtil.setData("/abc", "bcd");
        Thread.sleep(1000);
        zkUtil.setData("/abc", "def");
        Thread.sleep(1000);
        zkUtil.setData("/abc", "defg");
        Thread.sleep(1000);

        ZkData zkData = zkUtil.getWatchData("/abc", true);
        log.info("++++ getData /abc data:{}" + JSON.toJSONString(zkData));
        Thread.sleep(10000);
    }


    /**
     * 循环监听 watch 走自己的watcher
     */
    @Test
    void getDataCallback() throws InterruptedException {
        zkUtil.createP("/abc", "abc");
        zkUtil.getDataCallback("/abc", new AsyncCallback.DataCallback() {
            // i ? path:监控的path， ctx：自己传过来的入参，bytes:数据，stat：stat
            @Override
            public void processResult(int i, String path, Object ctx, byte[] bytes, Stat stat) {
                log.info("++++getDataCallback回掉  i:{}, path:{}, ctx:{}, bytes:{}, stat:{}",i,path,JSON.toJSONString(ctx),new String(bytes),JSON.toJSONString(stat));
            }
        }, "测试入参数据");
        log.info("++++getDataCallback over");
        Thread.sleep(10000);
    }

    /**
     * 循环监听 watch 走自己的watcher
     */
    @Test
    void testForOver() throws InterruptedException {

        // 保持连接，关闭zk,看日志，session id 不变
        zkUtil.createP("/abc", "abc");
        Thread.sleep(30000);
    }


}
