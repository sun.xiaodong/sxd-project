package com.sunxd.zookeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZZookeeperApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZZookeeperApplication.class, args);
    }

}
