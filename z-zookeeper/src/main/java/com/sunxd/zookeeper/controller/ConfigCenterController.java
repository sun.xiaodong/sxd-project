package com.sunxd.zookeeper.controller;

import com.alibaba.fastjson.JSON;
import com.sunxd.zookeeper.use.ZkConf;
import com.sunxd.zookeeper.use.ZkMyselfUtil;
import com.sunxd.zookeeper.use.configCenter.ListenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.AsyncCallback;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.data.Stat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * zk 的 连接watch  业务节点的监听 业务数据的回掉测试集合
 *
 * 其中watch 和 data callback可单独放在类里面，此处简便测试
 * @author: 作者名称
 * @date: 2021-06-28 03:11
 **/
@RestController
@RequestMapping("/config/center")
@RequiredArgsConstructor
@Slf4j
public class ConfigCenterController {

    private final ListenUtil listenUtil;



    @RequestMapping("createPNode")
    public String createPNode(@RequestParam String path, @RequestParam String data) throws InterruptedException {
        listenUtil.execute("configCenter");
        return "";
    }



}
