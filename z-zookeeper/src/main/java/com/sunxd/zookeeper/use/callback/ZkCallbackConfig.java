package com.sunxd.zookeeper.use.callback;

import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.CountDownLatch;

/**
 * @author: 作者名称
 * @date: 2021-06-18 00:04
 **/
@Configuration
@Slf4j
public class ZkCallbackConfig {

    @Value("${zk.address}")
    private String zkAddress;

    @Value("${zk.timeOut}")
    private int timeOut;

    public ZooKeeper zooKeeperCallback() {
        ZooKeeper zooKeeper = null ;
        CountDownLatch countDownLatch = new CountDownLatch(1);
        DefaultWatch defaultWatch = new DefaultWatch();
        defaultWatch.setCountDownLatch(countDownLatch);
        try {
            zooKeeper = new ZooKeeper(zkAddress+"/testPath",timeOut,defaultWatch);
            countDownLatch.await();
            return zooKeeper ;
        } catch (Exception e) {
            log.info("exception e:{}", (Object) e.getStackTrace());
        }
        return zooKeeper;
    }
}
