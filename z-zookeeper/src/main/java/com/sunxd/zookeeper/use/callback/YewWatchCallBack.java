package com.sunxd.zookeeper.use.callback;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.AsyncCallback;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import com.sunxd.zookeeper.use.configCenter.SystemConfig;

import java.util.Objects;
import java.util.concurrent.CountDownLatch;

/**
 * @author: 作者名称
 * @date: 2021-06-18 23:32
 **/
@Slf4j
@Data
public class YewWatchCallBack implements Watcher , AsyncCallback.DataCallback ,AsyncCallback.StatCallback {


    private ZooKeeper zooKeeper;

    private SystemConfig systemConfig;

    private String childrenPath;

    private CountDownLatch countDownLatch = new CountDownLatch(1);

    /**
     * 事件的状态callback
     * @param watchedEvent watchedEvent
     */
    @Override
    public void process(WatchedEvent watchedEvent) {
        log.info(" [YewAWatchCallBack => process] watchedEvent:{}",JSON.toJSONString(watchedEvent));
        switch (watchedEvent.getType()){

            case None:
                break;
            case NodeCreated:
                log.info(" [YewAWatchCallBack => process => NodeCreated]");
                zooKeeper.getData(childrenPath,this,this,childrenPath);
                break;
            case NodeDeleted:
                break;
            case NodeDataChanged:
                log.info(" [YewAWatchCallBack => process => NodeDataChanged]");
                zooKeeper.getData(childrenPath,this,this,childrenPath);
                break;
            case NodeChildrenChanged:
                break;
            case DataWatchRemoved:
                break;
            case ChildWatchRemoved:
                break;
            case PersistentWatchRemoved:
                break;
        }
    }


    /**
     * DataCallback
     * @param i i
     * @param path path
     * @param ctx ctx
     * @param bytes bytes
     * @param stat stat
     */
    @Override
    public void processResult(int i, String path, Object ctx, byte[] bytes, Stat stat) {
        if(bytes != null) {
            systemConfig.setData(new String(bytes));
            countDownLatch.countDown();
            log.info(" [YewAWatchCallBack => processResult => DataCallback i:{} ,path:{} ,ctx:{} ,bytes:{}, stat:{}]" ,i,path,ctx,new String(bytes),JSON.toJSONString(stat));
        }
    }

    /**
     * StatCallback 状态callback
     * @param i i
     * @param path path
     * @param ctx ctx
     * @param stat stat
     */
    @Override
    public void processResult(int i, String path, Object ctx, Stat stat) {
        log.info(" [YewAWatchCallBack => processResult => StatCallback i:{} ,path:{} ,ctx:{}, stat:{}]" ,i,path,ctx, JSON.toJSONString(stat));
        if(Objects.nonNull(stat)){
            zooKeeper.getData((String) ctx,this,this,ctx);
        }
    }


    public void aWait() throws InterruptedException {
        zooKeeper.exists(childrenPath,this, this,childrenPath);
        countDownLatch.await();
    }

}
