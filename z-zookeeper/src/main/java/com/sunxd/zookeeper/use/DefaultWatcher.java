package com.sunxd.zookeeper.use;

import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;

import java.util.concurrent.CountDownLatch;

/**
 * @author: 作者名称
 * @date: 2021-06-30 10:12
 **/
@Slf4j
public class DefaultWatcher implements Watcher {

    private CountDownLatch countDownLatch;

    @Override
    public void process(WatchedEvent watchedEvent) {
        
//        log.info("-----【进入Watch】-------------------------------------------------------------------------------");
        switch (watchedEvent.getState()) {
            case Disconnected:
                log.info("---[ 打印当前zk连接状态 -> Disconnected  ]------------------");
                break;
            case SyncConnected:
                countDownLatch.countDown();
                log.info("---[ 打印当前zk连接状态 -> SyncConnected  ]------------------");
                break;
            case AuthFailed:
                log.info("---[ 打印当前zk连接状态 -> AuthFailed  ]------------------");
                break;
            case ConnectedReadOnly:
                log.info("---[ 打印当前zk连接状态 -> ConnectedReadOnly  ]------------------");
                break;
            case SaslAuthenticated:
                log.info("---[ 打印当前zk连接状态 -> SaslAuthenticated  ]------------------");
                break;
            case Expired:
                log.info("---[ 打印当前zk连接状态 -> Expired  ]------------------");
                break;
            case Closed:
                log.info("---[ 打印当前zk连接状态 -> Closed  ]------------------");
                break;
        }

    }

    public CountDownLatch initCountDownLatch(){
        this.countDownLatch =  new CountDownLatch(1);
        return countDownLatch;
    }
}
