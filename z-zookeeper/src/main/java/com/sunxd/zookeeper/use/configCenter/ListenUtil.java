package com.sunxd.zookeeper.use.configCenter;

import com.sunxd.zookeeper.use.ZkConf;
import com.sunxd.zookeeper.use.ZkMyselfUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.stereotype.Component;

/**
 * @author: 作者名称
 * @date: 2021-06-30 17:35
 **/

@Component
@RequiredArgsConstructor
public class ListenUtil {

    private final ZkConf zkConf;

    public void execute(String path) throws InterruptedException {
        ZooKeeper zooKeeper = zkConf.getZookeeper();
        ReactiveListen reactiveListen = new ReactiveListen();
        reactiveListen.init(zooKeeper);
        reactiveListen.await(path);

        while (true){
            Thread.sleep(1000);
            if(StringUtils.isEmpty(reactiveListen.getSystemConfig().getData())){
                System.out.println("数据丢失了。。。" );
                reactiveListen.await(path);
            }
            System.out.println("最新的数据" + reactiveListen.getSystemConfig().getData());
        }
    }




}
