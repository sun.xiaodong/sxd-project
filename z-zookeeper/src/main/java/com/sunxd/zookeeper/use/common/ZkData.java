package com.sunxd.zookeeper.use.common;

import lombok.*;
import org.apache.zookeeper.data.Stat;

import java.io.Serializable;

/**
 * @author: 作者名称
 * @date: 2021-06-08 21:07
 **/

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class ZkData implements Serializable {

    private static final long serialVersionUID = 6259604756268828081L;
    private String data;
    private Stat stat;
}
