package com.sunxd.zookeeper.use.common;

import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.CountDownLatch;

import static org.apache.zookeeper.Watcher.Event.EventType.*;
import static org.apache.zookeeper.Watcher.Event.KeeperState.*;

/**
 * @author: 作者名称
 * @date: 2021-06-07 18:03
 **/
@Configuration
@Slf4j
public class ZkClientConfig {

    @Value("${zk.address}")
    private String zkAddress;

    @Value("${zk.timeOut}")
    private int timeOut;

    public ZooKeeper getZkClient(){
        ZooKeeper zooKeeper = null;
        try {
            CountDownLatch countDownLatch = new CountDownLatch(1);
            zooKeeper = new ZooKeeper(zkAddress,timeOut, (event)->{
                Watcher.Event.KeeperState state = event.getState();
                Watcher.Event.EventType type = event.getType();
                switch (state) {
                    case Unknown:
                        break;
                    case Disconnected:
                        log.info("++++++++++++.KeeperState state: " + Disconnected.name());
                        break;
                    case NoSyncConnected:
                        break;
                    case SyncConnected:
                        log.info("++++++++++++.KeeperState state: " + SyncConnected.name());
                        break;
                    case AuthFailed:
                        log.info("++++++++++++.KeeperState state: " + AuthFailed.name());
                        break;
                    case ConnectedReadOnly:
                        log.info("++++++++++++.KeeperState state: " + ConnectedReadOnly.name());
                        break;
                    case SaslAuthenticated:
                        log.info("++++++++++++.KeeperState state: " + SaslAuthenticated.name());
                        break;
                    case Expired:
                        log.info("++++++++++++.KeeperState state: " + Expired.name());
                        break;
                    case Closed:
                        break;
                }
                switch (type) {
                    case None:
                        log.info("++++++++++++.EventType type: " + None.name());
                        break;
                    case NodeCreated:
                        log.info("++++++++++++.EventType type: " + NodeCreated.name());
                        break;
                    case NodeDeleted:
                        log.info("++++++++++++.EventType type: " + NodeDeleted.name());
                        break;
                    case NodeDataChanged:
                        log.info("++++++++++++.EventType type: " + NodeDataChanged.name());
                        break;
                    case NodeChildrenChanged:
                        log.info("++++++++++++.EventType type: " + NodeChildrenChanged.name());
                        break;
                    case DataWatchRemoved:
                        log.info("++++++++++++.EventType type: " + DataWatchRemoved.name());
                        break;
                    case ChildWatchRemoved:
                        log.info("++++++++++++.EventType type: " + ChildWatchRemoved.name());
                        break;
                    case PersistentWatchRemoved:
                        log.info("++++++++++++.EventType type: " + PersistentWatchRemoved.name());
                        break;
                }
                String path = event.getPath();
                log.info("++++++++++++ zk connected path : {}",path);
                if(event.getState() == SyncConnected){
                    log.info("++++++++++++ zk client success");
                    countDownLatch.countDown();
                }
            });
            log.info(" ++++++++++++ 初始化ZooKeeper连接状态: {}",zooKeeper.getState());
            countDownLatch.await();
        } catch (Exception e) {
            log.error(" ++++++++++++ 初始化Zookeeper连接状态异常: {}",e.getMessage());
        }
        return zooKeeper;
    }


}
