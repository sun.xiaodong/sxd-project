package com.sunxd.zookeeper.use.myself;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;

import java.util.concurrent.CountDownLatch;

/**
 * @author: 作者名称
 * @date: 2021-06-28 02:03
 **/
@Data
@Slf4j
public class ZkConnectWatcher implements Watcher {

    private CountDownLatch countDownLatch;

    @Override
    public void process(WatchedEvent watchedEvent) {
        log.info("---[ 准备连接zk  start]------------------");
        switch (watchedEvent.getState()) {
            case Disconnected:
                log.info("---[ zk 连接状态 -> Disconnected  ]------------------");
                break;
            case SyncConnected:
                log.info("---[ zk 连接状态 -> SyncConnected  ]------------------");
                countDownLatch.countDown();
                break;
            case AuthFailed:
                log.info("---[ zk 连接状态 -> AuthFailed  ]------------------");
                break;
            case ConnectedReadOnly:
                log.info("---[ zk 连接状态 -> ConnectedReadOnly  ]------------------");
                break;
            case SaslAuthenticated:
                log.info("---[ zk 连接状态 -> SaslAuthenticated  ]------------------");
                break;
            case Expired:
                log.info("---[ zk 连接状态 -> Expired  ]------------------");
                break;
            case Closed:
                log.info("---[ zk 连接状态 -> Closed  ]------------------");
                break;
        }
    }

}
