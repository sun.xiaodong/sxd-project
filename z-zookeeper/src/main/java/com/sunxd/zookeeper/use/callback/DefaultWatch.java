package com.sunxd.zookeeper.use.callback;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;

import java.util.concurrent.CountDownLatch;

/**
 * @author: 作者名称
 * @date: 2021-06-18 00:09
 **/
@Slf4j
@Data
public class DefaultWatch implements Watcher {

    private CountDownLatch countDownLatch;

    /**
     * Watcher -> process
     *  和path没有关系，和session相关
     * @param watchedEvent
     */
    @Override
    public void process(WatchedEvent watchedEvent) {
        switch (watchedEvent.getState()) {
            case Unknown:
                break;
            case Disconnected:
                break;
            case NoSyncConnected:
                break;
            case SyncConnected:
                log.info(" 【c】 zk 连接成功了 ");
                countDownLatch.countDown();
                break;
            case AuthFailed:
                break;
            case ConnectedReadOnly:
                break;
            case SaslAuthenticated:
                break;
            case Expired:
                break;
            case Closed:
                break;
        }
        log.info(" 【DefaultWatch process】 :{}",watchedEvent.toString());
    }


}
