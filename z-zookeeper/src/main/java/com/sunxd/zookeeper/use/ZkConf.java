package com.sunxd.zookeeper.use;

import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.CountDownLatch;

/**
 * @author: 作者名称
 * @date: 2021-06-28 01:56
 **/

@Configuration
@Slf4j
public class ZkConf {

    @Value("${zk.address}")
    private String zkAddress;

    @Value("${zk.timeOut}")
    private int timeOut;


    public ZooKeeper getZookeeper() {
        return getZookeeper("");
    }

    public ZooKeeper getZookeeper(String path) {
//        log.info("---连接zk--start------------------");
        DefaultWatcher defaultWatcher = new DefaultWatcher();
        CountDownLatch countDownLatch = defaultWatcher.initCountDownLatch();
        ZooKeeper zooKeeper = null;
        try {
            zooKeeper = new ZooKeeper(zkAddress+path, timeOut, defaultWatcher);
            countDownLatch.await();
        } catch (Exception e) {
            log.info("----------------zk connected error--------------");

        }
        log.info("---成功连接zk--end------------------");
        return zooKeeper;
    }

}
