package com.sunxd.dubbo.consumer.dubbo.consumer.controller;

import com.alibaba.fastjson.JSON;
import com.sunxd.base.common.entity.UserEntity;
import com.sunxd.dubbo.dubbo.api.service.user.IUserWriteService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author: 作者名称
 * @date: 2021-05-07 23:05
 **/
@RestController
@RequestMapping("user")
public class UserController {

    @DubboReference
    private IUserWriteService service;

    @RequestMapping("add")
    public int add(@RequestBody Map map) {
        UserEntity param = JSON.parseObject(JSON.toJSONString(map), UserEntity.class);
        return service.add(param);
    }

}
