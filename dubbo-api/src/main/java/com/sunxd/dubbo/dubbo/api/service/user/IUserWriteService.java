package com.sunxd.dubbo.dubbo.api.service.user;


import com.sunxd.base.common.entity.UserEntity;

/**
 * @author: 作者名称
 * @date: 2021-05-07 23:20
 **/
public interface IUserWriteService {

    int add(UserEntity userEntity);
}
