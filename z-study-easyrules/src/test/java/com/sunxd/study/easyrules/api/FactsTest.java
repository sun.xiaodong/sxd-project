package com.sunxd.study.easyrules.api;


import org.junit.Test;

/**
 * Facts Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>3�� 29, 2022</pre>
 */
public class FactsTest {


    private final Facts facts = new Facts();

    /**
     * Method: put(String name, T value)
     */
    @Test
    public void testPut() throws Exception {
        facts.add(new Fact<>("a",1));
        Fact<?> fact = facts.getFact("a");
        Object value = fact.getValue();
        System.out.println(value);

        facts.put("b",2);
        fact = facts.getFact("b");
        value = fact.getValue();
        System.out.println(value);

    }

    /**
     * Method: getFact(String name)
     */
    @Test
    public void testGetFact() throws Exception {

    }

    /**
     * Method: remove(Fact<?> fact)
     */
    @Test
    public void testRemove() throws Exception {

    }

    /**
     * Method: get(String factName)
     */
    @Test
    public void testGet() throws Exception {

    }

    /**
     * Method: asMap()
     */
    @Test
    public void testAsMap() throws Exception {

    }

    /**
     * Method: iterator()
     */
    @Test
    public void testIterator() throws Exception {

    }


} 
