package com.sunxd.study.easyrules.api;


import com.sunxd.study.easyrules.core.DefaultRulesEngine;
import com.sunxd.study.easyrules.rule.AgeRule;
import com.sunxd.study.easyrules.rule.MyStudent;
import org.junit.Test;

/**
 * Rules Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>3�� 29, 2022</pre>
 */
public class RulesTest {


    /**
     * Method: register(Objects... rules)
     */
    @Test
    public void testRegister() throws Exception {

        Rules rules = new Rules();
        rules.register(new AgeRule());
        rules.forEach(x -> {
            System.out.println(x.getName());
            System.out.println(x.getDescription());
            System.out.println(x.getPriority());
        });
        Facts facts = new Facts();
        facts.put("myStudent", MyStudent.builder().age(17).name("name").build());
        RulesEngine rulesEngine = new DefaultRulesEngine();
        rulesEngine.fire(rules,facts);
    }

    /**
     * Method: unregister(Object... rules)
     */
    @Test
    public void testUnregisterRules() throws Exception {

    }

    /**
     * Method: unregister(final String ruleName)
     */
    @Test
    public void testUnregisterRuleName() throws Exception {

    }

    /**
     * Method: isEmpty()
     */
    @Test
    public void testIsEmpty() throws Exception {

    }

    /**
     * Method: iterator()
     */
    @Test
    public void testIterator() throws Exception {

    }


    /**
     * Method: findRuleByName(String ruleName)
     */
    @Test
    public void testFindRuleByName() throws Exception {

    }



} 
