package com.sunxd.study.easyrules.rule;

import com.sunxd.study.easyrules.annotation.ActionAnnotation;
import com.sunxd.study.easyrules.annotation.ConditionAnnotation;
import com.sunxd.study.easyrules.annotation.FactAnnotation;
import com.sunxd.study.easyrules.annotation.RuleAnnotation;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/3/29 19:19
 */

@RuleAnnotation
public class AgeRule {

    @ConditionAnnotation
    public boolean when(@FactAnnotation("myStudent") MyStudent myStudent){
        return myStudent.getAge() > 15;
    }

    @ActionAnnotation
    public void then(@FactAnnotation("myStudent") MyStudent myStudent){
        System.out.println("myStudent :"+myStudent.getAge());
    }

}
