package com.sunxd.study.easyrules.rule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/3/29 19:39
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MyStudent {

    private String name;
    private Integer age;

}
