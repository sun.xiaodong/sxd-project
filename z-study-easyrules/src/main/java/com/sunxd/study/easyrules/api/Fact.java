package com.sunxd.study.easyrules.api;

import java.util.Objects;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/3/29 16:22
 */
public class Fact<T> {

    private final String name;
    private final T value;

    public Fact(String name, T value) {
        Objects.requireNonNull(name, "name must not be null");
        Objects.requireNonNull(value, "value must not be null");
        this.name = name;
        this.value = value;
    }

     /* Get the fact name.
     * @return fact name
	 */
    public String getName() {
        return name;
    }

    /**
     * Get the fact value.
     * @return fact value
     */
    public T getValue() {
        return value;
    }

}
