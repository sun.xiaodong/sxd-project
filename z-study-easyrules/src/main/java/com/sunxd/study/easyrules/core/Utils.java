package com.sunxd.study.easyrules.core;

import java.lang.annotation.Annotation;
import java.util.Objects;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/3/29 17:24
 */
public class Utils {

    public static <A extends Annotation> A findAnnotation(Class<A> targetAnnotation, Class<?> annotationType) {
        A foundAnnotation = annotationType.getAnnotation(targetAnnotation);
        if (Objects.isNull(foundAnnotation)) {
            for (Annotation annotation : annotationType.getAnnotations()){
                Class<? extends Annotation> type = annotation.annotationType();
                if(type.isAnnotationPresent(targetAnnotation)){
                    foundAnnotation = type.getAnnotation(targetAnnotation);
                    break;
                }
            }
        }
        return foundAnnotation;
    }

}
