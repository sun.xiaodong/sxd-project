package com.sunxd.study.easyrules.rule.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/3/29 11:13
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Person implements Serializable {
    private static final long serialVersionUID = 944252131183821204L;

    private String name;
    private boolean adult;
    private int age;

}
