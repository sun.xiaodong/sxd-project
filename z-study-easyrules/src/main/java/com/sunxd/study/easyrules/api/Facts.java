package com.sunxd.study.easyrules.api;

import java.util.*;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/3/29 16:24
 */
public class Facts implements Iterable<Fact<?>> {

    private final Set<Fact<?>> facts = new HashSet<>();

    public <T> void put(String name,T value){
        Objects.requireNonNull(name, "fact name must not be null");
        Objects.requireNonNull(value, "fact value must not be null");
        Fact<?> fact = getFact(name);
        if(Objects.nonNull(fact)){
            remove(fact);
        }
        facts.add(new Fact<>(name, value));

    }

    /**
     * Add a fact, replacing any fact with the same name.
     *
     * @param fact to add, must not be null
     */
    public void add(Fact<?> fact) {
        Objects.requireNonNull(fact, "fact must not be null");
        Fact<?> retrievedFact = getFact(fact.getName());
        if (retrievedFact != null) {
            remove(retrievedFact);
        }
        facts.add(fact);
    }

    public Fact<?> getFact(String name){
        Objects.requireNonNull(name, "fact name must not be null");
        return facts.stream().filter(fact -> fact.getName().equals(name)).findFirst().orElse(null);
    }

    public void remove(Fact<?> fact){
        Objects.requireNonNull(fact, "fact must not be null");
        facts.remove(fact);
    }

    /**
     * Get the value of a fact by its name. This is a convenience method provided
     * as a short version of {@code getFact(factName).getValue()}.
     *
     * @param factName name of the fact, must not be null
     * @param <T> type of the fact's value
     * @return the value of the fact having the given name, or null if there is
     * no fact with the given name
     */
    @SuppressWarnings("unchecked")
    public <T> T get(String factName) {
        Objects.requireNonNull(factName, "fact name must not be null");
        Fact<?> fact = getFact(factName);
        if (fact != null) {
            return (T) fact.getValue();
        }
        return null;
    }

    /**
     * Return a copy of the facts as a map. It is not intended to manipulate
     * facts outside of the rules engine (aka other than manipulating them through rules).
     *
     * @return a copy of the current facts as a {@link HashMap}
     */
    public Map<String, Object> asMap() {
        Map<String, Object> map = new HashMap<>();
        for (Fact<?> fact : facts) {
            map.put(fact.getName(), fact.getValue());
        }
        return map;
    }

    @Override
    public Iterator<Fact<?>> iterator() {
        return facts.iterator();
    }
}
