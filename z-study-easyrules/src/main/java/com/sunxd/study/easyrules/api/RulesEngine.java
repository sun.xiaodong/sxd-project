package com.sunxd.study.easyrules.api;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/3/29 19:41
 */
public interface RulesEngine {

    void fire(Rules rules, Facts facts);
}
