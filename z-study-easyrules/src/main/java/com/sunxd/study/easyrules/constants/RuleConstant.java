package com.sunxd.study.easyrules.constants;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/3/29 16:17
 */
public interface RuleConstant {

    /**
     * Default rule name.
     */
    String DEFAULT_NAME = "rule";

    /**
     * Default rule description.
     */
    String DEFAULT_DESCRIPTION = "description";

    /**
     * Default rule priority.
     */
    int DEFAULT_PRIORITY = Integer.MAX_VALUE - 1;

}
