package com.sunxd.study.easyrules;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZStudyEasyrulesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZStudyEasyrulesApplication.class, args);
    }

}
