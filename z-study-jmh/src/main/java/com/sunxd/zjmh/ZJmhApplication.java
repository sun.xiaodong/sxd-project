package com.sunxd.zjmh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZJmhApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZJmhApplication.class, args);
    }

}
