package com.sunxd.zjmh;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author: 作者名称
 * @date: 2021-09-23 15:22
 **/
public class JmhMethod {

    static List<Integer> nums = new ArrayList<>();
    static {
        Random r = new Random();
        for (int i = 0; i < 10000; i++) {
            nums.add(10000+r.nextInt(1000));
        }
    }

    static void forEach() {
        nums.forEach(JmhMethod::isPrime);
    }
    static void parallel(){
        nums.parallelStream().forEach(JmhMethod::isPrime);
    }

    static boolean isPrime(int num) {
        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) return false;
        }
        return true;
    }
}
