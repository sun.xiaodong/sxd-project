package com.sunxd.zjmh;

import com.sun.org.apache.xpath.internal.operations.Mod;
import org.openjdk.jmh.annotations.*;

/**
 * @author: 作者名称
 * @date: 2021-09-23 15:32
 **/
public class JmhTest {

    @Benchmark
    @Warmup(iterations = 1,time = 3) // 预热 iterations迭代次数  tim等待几秒
    @Fork(5) // 起多少线程执行
    @BenchmarkMode(Mode.Throughput) //
    @Measurement(iterations = 2,time = 3)  // 掉用多少次，每次迭代时常
    public void test(){
        JmhMethod.forEach();
    }
}
