package com.sunxd.base.mysql.base.mysql.dao;

import com.sunxd.base.common.entity.UserEntity;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author: 作者名称
 * @date: 2021-05-07 11:32
 **/

@Repository
public interface UserDao {

    int insert(UserEntity userEntity);

    void deleteById(Long id);

    UserEntity update(UserEntity userEntity);

    List<UserEntity> findList(UserEntity userEntity);

}
