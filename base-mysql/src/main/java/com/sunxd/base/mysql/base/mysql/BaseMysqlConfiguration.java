package com.sunxd.base.mysql.base.mysql;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author: 作者名称
 * @date: 2021-05-07 14:15
 **/

@Configuration
@MapperScan(value = {
        "com.sunxd.base.mysql.base.mysql.dao",
})
@EnableConfigurationProperties
public class BaseMysqlConfiguration {

}
