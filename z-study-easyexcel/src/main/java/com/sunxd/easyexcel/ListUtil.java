package com.sunxd.easyexcel;

import com.google.common.collect.Lists;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/6/10 17:38
 */
public class ListUtil {

    /**
     * 数据转换
     *
     * @param list list
     * @param function function
     * @param <T>  t
     * @param <R>  r
     * @return list
     */
    public static <T, R> List<R> toList(List<T> list, Function<T, R> function) {
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }
        return list.stream().map(function).collect(Collectors.toList());
    }

}
