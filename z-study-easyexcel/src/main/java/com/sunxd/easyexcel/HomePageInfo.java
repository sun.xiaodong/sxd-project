package com.sunxd.easyexcel;

import lombok.Data;

import java.io.Serializable;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/10/12 11:25
 */

/**
 * 首页图标返回数据
 */
@Data
public class HomePageInfo implements Serializable {
    private static final long serialVersionUID = 8846472998477075628L;

    /**
     * 统计日期
     */
    private String statDay;
    /**
     * 活动id
     */
    private String activityUuid;

    /**
     * 返佣规则
     */
    private String settlementType;

    /**
     * 媒体名称
     */
    private String mediaName;


    /**
     * sourceId
     */
    private String sourceId;


    /**
     * 实付金额
     */
    private String payAmount;


    /**
     * 预计收入
     */
    private String estimateIncome;

}
