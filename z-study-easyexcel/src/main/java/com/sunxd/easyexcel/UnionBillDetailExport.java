package com.sunxd.easyexcel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @description: 运营账单导出
 * @date 2022/3/1 9:08
 */
@Data
public class UnionBillDetailExport {

    @ExcelProperty(value = "统计日期")
    private String statDay;

    @ExcelProperty(value = "活动uuid")
    private String activityUuid;

    @ExcelProperty(value = "返佣规则")
    private String settlementType;

    @ExcelProperty(value = "媒体名称")
    private String mediaName;

    @ExcelProperty(value = "sourceId")
    private String sourceId;

    @ExcelProperty(value = "实付金额")
    private String payAmount;

    @ExcelProperty(value = "预计收入")
    private String estimateIncome;


}
