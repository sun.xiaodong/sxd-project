package com.sunxd.easyexcel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZStudyEasyexcelApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZStudyEasyexcelApplication.class, args);
    }

}
