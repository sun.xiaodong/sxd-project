package com.sunxd.easyexcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 * @author sun.xd
 * @description: description
 * @date 2022/10/10 15:27
 */
@Slf4j
@RestController
@RequestMapping("/api/marketing/union/member/home")
public class UnionHomeController {


    /**
     * 账单导出-明细
     *
     * @param response response
     * @return String
     * @throws IOException IOException
     */
    @GetMapping("/exportBillDetailTest")
    public void exportBillDetailTest(HttpServletResponse response) throws IOException {
//        第一种
//        EasyExcelUtil.writeExcel(response, ListUtil.toList(getDate(1), convertData()),
//                "测试", "sheetName", UnionBillDetailExport.class);


//        第二种
//        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
//        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//        response.setCharacterEncoding("utf-8");
//        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
//        String fileName = URLEncoder.encode("明细", "UTF-8").replaceAll("\\+", "%20");
//        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
//        EasyExcel.write(response.getOutputStream(), UnionBillDetailExport.class).sheet("模板")
//                .doWrite(ListUtil.toList(getDate(1), convertData()));


        // 方法3: 如果写到不同的sheet 同一个对象
        boolean flag = true;
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode("明细", "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream(), UnionBillDetailExport.class).build();
        int i = 0;
        while (flag) {
            List<HomePageInfo> infoList = getDate(i);
            if (!Objects.equals(infoList.size(), 10000)) {
                flag = false;
            }
            WriteSheet writeSheet = EasyExcel.writerSheet(i, "sheet" + i).build();
            i += 1;
            // 分页去数据库查询数据 这里可以去数据库查询每一页的数据
            excelWriter.write(ListUtil.toList(infoList, convertData()), writeSheet);

        }
        excelWriter.finish();
        return;
    }

    /**
     * 账单月 数据转换
     *
     * @return Function
     */
    private Function<HomePageInfo, UnionBillDetailExport> convertData() {
        return (unionBillExportMonthInfo -> {
            UnionBillDetailExport export = new UnionBillDetailExport();
            BeanUtils.copyProperties(unionBillExportMonthInfo, export);
            return export;
        });
    }

    /**
     * test
     *
     * @param i a
     * @return a
     */
    private List<HomePageInfo> getDate(int i) {
        List<HomePageInfo> list = Lists.newArrayList();
        if (i < 2) {
            for (int j = 0; j < 10000; j++) {
                HomePageInfo info = new HomePageInfo();
                info.setActivityUuid("ActivityUuid" + i + j);
                info.setEstimateIncome("EstimateIncome" + i + j);
                info.setMediaName("MediaName" + i + j);
                info.setPayAmount("PayAmount" + i + j);
                info.setSettlementType("SettlementType" + i + j);
                info.setSourceId("SourceId" + i + j);
                info.setStatDay("StatDay" + i + j);
                list.add(info);
            }
        } else {
            HomePageInfo info = new HomePageInfo();
            info.setActivityUuid("ActivityUuid" + i);
            info.setEstimateIncome("EstimateIncome" + i);
            info.setMediaName("MediaName" + i);
            info.setPayAmount("PayAmount" + i);
            info.setSettlementType("SettlementType" + i);
            info.setSourceId("SourceId" + i);
            info.setStatDay("StatDay" + i);
            list.add(info);
        }
        return list;
    }

}
