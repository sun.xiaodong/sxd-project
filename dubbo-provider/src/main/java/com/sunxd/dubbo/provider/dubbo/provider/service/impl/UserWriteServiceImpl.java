package com.sunxd.dubbo.provider.dubbo.provider.service.impl;
import com.sunxd.base.common.entity.UserEntity;
import com.sunxd.base.mysql.base.mysql.dao.UserDao;
import com.sunxd.dubbo.dubbo.api.service.user.IUserWriteService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * @author: 作者名称
 * @date: 2021-05-07 23:07
 **/
@DubboService
@RequiredArgsConstructor
public class UserWriteServiceImpl implements IUserWriteService {

    private final UserDao userDao;

    @Override
    public int add(UserEntity userEntity) {
        System.out.println("接收到请求");
        return 1;
    }
}
